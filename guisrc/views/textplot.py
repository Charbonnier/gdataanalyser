import os
from gi.repository import Gtk

from guisrc.gui import Gui
from src.views.textplot import PlotText
from structure.mutative import MutativeMemberSetterGui, FloatGui
from utils import glade_path
from views.colorfactory import ColorFactoryGui
from views.mplproperties import TextPropertiesGui
from views.plotstyle import FillStyleGui, MarkerStyleGui, LineStyleGui
from views.suplot import SubplotGui
from views.view import ViewSetting


class TextSubplotGui(SubplotGui):
    def __init__(self):
        super().__init__()
        builder = Gtk.Builder()
        builder.add_from_file(os.path.join(glade_path(), 'views', 'text_subplot.glade'))
        self._x_data_gui = MutativeMemberSetterGui(constant_gui=None)
        self._y_data_gui = MutativeMemberSetterGui(constant_gui=None)
        self._text_data_gui = MutativeMemberSetterGui(constant_gui=None)
        self._text_properties_gui = TextPropertiesGui()

        x_data_box = builder.get_object('x_data_box')
        ":type: Gtk.Box"
        x_data_box.pack_start(self._x_data_gui.gui(), True, True, 0)

        y_data_box = builder.get_object('y_data_box')
        ":type: Gtk.Box"
        y_data_box.pack_start(self._y_data_gui.gui(), True, True, 0)

        text_data_box = builder.get_object('text_data_box')
        ":type: Gtk.Box"
        text_data_box.pack_start(ViewSetting('Text Data', self._text_properties_gui), False, False, 0)
        text_data_box.pack_start(self._text_data_gui.gui(), True, True, 0)
        self._gui = builder.get_object('text_nbk')

    def set_context(self, context):
        super().set_context(context)
        self._x_data_gui.set_context(context)
        self._y_data_gui.set_context(context)
        self._text_data_gui.set_context(context)
        self._text_properties_gui.set_context(context)

    def set_object(self, subplot):
        """
        :param src.views.textplot.PlotText subplot:
        :return:
        """
        super().set_object(subplot)
        self.updating = True
        data_provider = subplot.get_data_provider()
        ":type: src.views.textplot.PlotTextDataProvider"
        self._x_data_gui.set_object((None, data_provider.mutable_members['x']))
        self._y_data_gui.set_object((None, data_provider.mutable_members['y']))
        self._text_data_gui.set_object((None, data_provider.mutable_members['text']))
        self._text_properties_gui.set_object(data_provider.text_properties)

    def gui(self):
        return self._gui


Gui.register_gui_for_class(PlotText, TextSubplotGui)
