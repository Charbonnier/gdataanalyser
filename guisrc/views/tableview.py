import os

from gi.repository import Gtk, GObject, GLib, Gdk

from guisrc.gui import Gui
from src.views.tableview import TableView, TableRenderer
from guisrc.structure.mutative import StringSetterGui
from utils import glade_path
from guisrc.views.view import ViewGui


class TableGuiRenderer(TableRenderer):
    def __init__(self, columns, treeview):
        """

        :param dict[str, (Gtk.TreeViewColumn, Gtk.CellRendererText, src.views.tableview.Column)] columns:
        :param Gtk.TreeView treeview:
        """
        super().__init__()
        self.columns = columns
        self.treeview = treeview
        self.end_rendering_cb = None

    def set_columns_data(self, body):
        try:
            super().set_columns_data(body)
            if len(body.columns_mms_ids) == 0:
                self.treeview.set_model(Gtk.ListStore(str))
            else:
                model = Gtk.ListStore(*([str]*len(body.columns_mms_ids)))

                for i, column_id in enumerate(body.columns_mms_ids):
                    column, renderer, _ = self.columns[column_id]
                    column.set_attributes(renderer, text=i)

                # Returns if body contains no data
                if len(body.columns_mms_values) == 0:
                    return

                for row in zip(*[body.columns_mms_values[c_id] for c_id in body.columns_mms_ids]):
                    row_str = [TableRenderer.apply_format(fmt, v) for fmt, v in zip(body.columns_formatters, row)]
                    model.append(row_str)

                self.treeview.set_model(model)
        finally:
            if self.end_rendering_cb is not None:
                self.end_rendering_cb(None, None)


class TableGui(ViewGui):
    def __init__(self):
        self.view = None
        ":type: src.views.tableview.TableView"
        self.view_widget = None
        ":type: Gtk.ScrolledWindow"

        self.columns = {}
        ":type: dict[str, (Gtk.TreeViewColumn, Gtk.CellRendererText, src.views.tableview.Column)]"

        builder = Gtk.Builder()
        builder.add_from_file(os.path.join(glade_path(), 'views', 'table.glade'))
        self.table_tview = builder.get_object('table_tview')
        ":type: Gtk.TreeView"
        self.column_edit_pop = builder.get_object('column_edit_pop')
        ":type: Gtk.Popover"
        self.name_entry = builder.get_object('name_entry')
        ":type: Gtk.Entry"
        self.formatter_entry = builder.get_object('formatter_entry')
        ":type: Gtk.Entry"

        super().__init__(builder.get_object('table_scrl'))

        self.field = StringSetterGui()
        self.column_edit_pop.get_child().pack_start(self.field.gui(), True, True, 0)
        # Initializes the TreeView with a dummy model, otherwise columns headers can be invisible in some conditions.
        self.table_tview.set_model(Gtk.ListStore(str))

        add_column_btn = builder.get_object('add_column_btn')
        self.view_tbx.add(Gtk.SeparatorToolItem(visible=True))
        self.view_tbx.add(add_column_btn)

        builder.connect_signals(self)

        self.current_edited_column = None
        ":type: Gtk.TreeViewColumn|None"

    def build_renderer(self):
        renderer = TableGuiRenderer(self.columns, self.table_tview)
        renderer.end_rendering_cb = self.on_end_rendering_figure
        return renderer

    def on_column_clicked(self, widget, data=None):
        """

        :param Gtk.TreeViewColumn widget:
        :param data:
        :return:
        """
        column_offset = widget.get_x_offset()
        rectangle = Gdk.Rectangle()
        rectangle.x = column_offset
        rectangle.y = 0
        rectangle.width = widget.get_width()
        rectangle.height = 20
        self.current_edited_column = None

        self.current_edited_column = widget
        column_id = self.current_edited_column.get_name()
        column_index = self.view.body.index_from_id(column_id)
        column = self.view.body.columns_mms[column_index]
        name = self.view.body.columns_names[column_index]
        formatter = self.view.body.columns_formatters[column_index]

        self.updating = True
        try:
            self.name_entry.set_text(name)
            self.formatter_entry.set_text(formatter)
            self.field.set_object((None, column))
        finally:
            self.updating = False
        self.column_edit_pop.set_pointing_to(rectangle)
        self.column_edit_pop.popup()

    def set_context(self, context):
        super().set_context(context)
        self.field.set_context(context)

    def set_object(self, table):
        self.view = table
        self.updating = True
        try:
            for c in self.table_tview.get_columns():
                self.table_tview.remove_column(c)
            self.columns.clear()
            for c_id, c in zip(self.view.body.columns_mms_ids, self.view.body.columns_mms):
                self.create_treeviewcolumn(c_id, c)
        finally:
            self.updating = False
        super().set_object(table)

    def create_treeviewcolumn(self, column_id, column_mm):
        """

        :param str column_id:
        :param src.views.tableview.Column column_mm:
        :return:
        """
        name = self.view.body.columns_names[self.view.body.index_from_id(column_id)]
        cell_renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn(name, cell_renderer)
        column.set_clickable(True)
        column.connect('clicked', self.on_column_clicked)
        column.set_name(column_id)
        column.set_resizable(True)
        column.set_reorderable(True)
        self.table_tview.append_column(column)
        self.columns[column_id] = column, cell_renderer, column_mm

    def on_name_entry_changed(self, widget):
        """

        :param Gtk.Entry widget:
        :return:
        """
        if self.current_edited_column is None:
            return

        name = widget.get_text()
        self.current_edited_column.set_title(name)

        if self.updating:
            return

        column_id = self.current_edited_column.get_name()
        self.view.body.rename_column(self.view.body.index_from_id(column_id), name)

    def on_formatter_entry_changed(self, widget):
        """

        :param Gtk.Entry widget:
        :return:
        """
        if self.current_edited_column is None:
            return
        if self.updating:
            return

        column_id = self.current_edited_column.get_name()
        self.view.body.columns_formatters[self.view.body.index_from_id(column_id)] = widget.get_text()

    def on_add_column_btn_clicked(self, widget):
        column_id, column = self.view.new_column('New Column', '')
        self.create_treeviewcolumn(column_id, column)

    def on_remove_column_btn_clicked(self, widget):
        if self.current_edited_column is not None:
            column_id = self.current_edited_column.get_name()
            self.table_tview.remove_column(self.current_edited_column)
            self.view.body.remove_column(self.view.body.index_from_id(column_id))
            self.current_edited_column = None
        self.column_edit_pop.popdown()

    def on_table_tview_columns_changed(self, widget, data=None):
        n_columns = self.table_tview.get_n_columns()
        # This function only handles columns reordering
        # Other events are filtered out by checking the number of columns
        if n_columns != self.view.body.get_n_columns():
            return
        ids = [c.get_name() for c in self.table_tview.get_columns()]
        self.view.body.reorder_columns(ids)


Gui.register_gui_for_class(TableView, TableGui)


if __name__ == "__main__":
    table = TableGui()
    table.view = TableView()
    window = Gtk.Window()
    window.add(table.gui())
    window.connect('delete-event', lambda a, w: Gtk.main_quit())
    window.show()
    Gtk.main()
