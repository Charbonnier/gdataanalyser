import os

from gi.repository import Gtk

from utils import glade_path
from views.plotstyle import LineStyleGui
from views.view import PropertiesGui, ConstantSettingPropertyGui, FloatPropertyGui, ColorPropertyGui, \
    ComboPropertyGui

builder = Gtk.Builder()
builder.add_objects_from_file(os.path.join(glade_path(), 'views', 'mplproperties.glade'),
                              ('fontstyle_lst', 'horizontalalignement_lst', 'verticalalignement_lst'))
FONT_STYLE_LST = builder.get_object('fontstyle_lst')
HORIZONTAL_ALIGNMENT_LST = builder.get_object('horizontalalignement_lst')
VERTICAL_ALIGNMENT_LST = builder.get_object('verticalalignement_lst')


class TextPropertiesGui(PropertiesGui):
    def build_properties(self):
        self.add_property(FloatPropertyGui('fontsize', 'Font Size'))
        self.add_property(ColorPropertyGui('color', 'Color'))
        self.add_property(FloatPropertyGui('rotation', 'Rotation'))
        self.add_property(ComboPropertyGui('fontstyle', FONT_STYLE_LST, 'Font Style'))
        self.add_property(FloatPropertyGui('fontstretch', 'Font Stretch'))
        self.add_property(ComboPropertyGui('verticalalignment', VERTICAL_ALIGNMENT_LST, 'Vertical Alignment'))
        self.add_property(ComboPropertyGui('horizontalalignment', HORIZONTAL_ALIGNMENT_LST, 'Horizontal Alignment'))


class GridPropertiesGui(PropertiesGui):
    def build_properties(self):
        self.add_property(ConstantSettingPropertyGui('linestyle', LineStyleGui(), 'Line Style'))
        self.add_property(ColorPropertyGui('color', 'Color'))
        self.add_property(FloatPropertyGui('linewidth', 'Width'))
