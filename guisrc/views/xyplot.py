import os
from gi.repository import Gtk

from guisrc.gui import Gui
from src.views.xyplot import PlotXY
from structure.mutative import MutativeMemberSetterGui, FloatGui
from utils import glade_path
from views.colorfactory import ColorFactoryGui
from views.plotstyle import FillStyleGui, MarkerStyleGui, LineStyleGui
from views.suplot import SubplotGui


class XYSubplotGui(SubplotGui):
    def __init__(self):
        super().__init__()
        builder = Gtk.Builder()
        builder.add_from_file(os.path.join(glade_path(), 'views', 'xy_subplot.glade'))
        self._x_data_gui = MutativeMemberSetterGui(constant_gui=None)
        self._y_data_gui = MutativeMemberSetterGui(constant_gui=None)
        self._linewidth_gui = MutativeMemberSetterGui(constant_gui=FloatGui())
        self._markersize_gui = MutativeMemberSetterGui(constant_gui=FloatGui())
        self._markerstyle_gui = MarkerStyleGui()
        self._linestyle_gui = LineStyleGui()
        self._line_color_gui = ColorFactoryGui()
        self._face_color_gui = ColorFactoryGui()
        self._edge_color_gui = ColorFactoryGui()
        self._fill_style_gui = FillStyleGui()
        self._marker_edge_width_gui = FloatGui()

        self._line_color_gui.on_object_class_changed = lambda g: self._on_class_change('color', g)
        self._face_color_gui.on_object_class_changed = lambda g: self._on_class_change('markerfacecolor', g)
        self._edge_color_gui.on_object_class_changed = lambda g: self._on_class_change('markeredgecolor', g)
        self._linestyle_gui.on_value_changed = lambda g: self._on_constant_change('linestyle', g)
        self._fill_style_gui.on_value_changed = lambda g: self._on_constant_change('fillstyle', g)
        self._markerstyle_gui.on_value_changed = lambda g: self._on_constant_change('marker', g)
        self._marker_edge_width_gui.on_value_changed = lambda g: self._on_constant_change('markeredgewidth', g)

        x_data_box = builder.get_object('x_data_box')
        ":type: Gtk.Box"
        x_data_box.pack_start(self._x_data_gui.gui(), True, True, 0)

        y_data_box = builder.get_object('y_data_box')
        ":type: Gtk.Box"
        y_data_box.pack_start(self._y_data_gui.gui(), True, True, 0)

        line_style_box = builder.get_object('line_style_box')
        ":type: Gtk.Box"
        line_style_box.pack_start(self._linestyle_gui.gui, True, True, 0)

        marker_size_box = builder.get_object('marker_size_box')
        ":type: Gtk.Box"
        marker_size_box.pack_start(self._markersize_gui.gui(), True, True, 0)

        marker_edge_width_box = builder.get_object('marker_edge_width_box')
        ":type: Gtk.Box"
        marker_edge_width_box.pack_start(self._marker_edge_width_gui.gui, True, True, 0)

        marker_style_box = builder.get_object('marker_style_box')
        ":type: Gtk.Box"
        marker_style_box.pack_start(self._markerstyle_gui.gui, True, True, 0)

        line_thickness_box = builder.get_object('line_thickness_box')
        ":type: Gtk.Box"
        line_thickness_box.pack_start(self._linewidth_gui.gui(), True, True, 0)

        color_box = builder.get_object('line_color_box')
        ":type: Gtk.Box"
        color_box.pack_start(self._line_color_gui.gui(), True, True, 0)

        color_box = builder.get_object('face_color_box')
        ":type: Gtk.Box"
        color_box.pack_start(self._face_color_gui.gui(), True, True, 0)

        color_box = builder.get_object('edge_color_box')
        ":type: Gtk.Box"
        color_box.pack_start(self._edge_color_gui.gui(), True, True, 0)

        fill_box = builder.get_object('fill_box')
        ":type: Gtk.Box"
        fill_box.pack_start(self._fill_style_gui.gui, True, True, 0)

        self._gui = builder.get_object('xy_nbk')

    def gui(self):
        return self._gui

    def set_context(self, context):
        super().set_context(context)
        self._x_data_gui.set_context(context)
        self._y_data_gui.set_context(context)
        self._linewidth_gui.set_context(context)
        self._markersize_gui.set_context(context)
        self._line_color_gui.set_context(context)
        self._face_color_gui.set_context(context)
        self._edge_color_gui.set_context(context)

    def _on_class_change(self, attribute, cls_gui):
        if self.updating:
            return
        if self._subplot is not None:
            setattr(self._subplot.get_data_provider(), attribute, cls_gui.displayed_object)
            cls_gui.set_context(self.context)

    def _on_constant_change(self, attribute, const_gui):
        if self.updating:
            return
        if self._subplot is not None:
            setattr(self._subplot.get_data_provider(), attribute, const_gui.get_value())

    def set_object(self, subplot):
        """
        :param src.views.xyplot.PlotXY subplot:
        :return:
        """
        super().set_object(subplot)
        self.updating = True
        data_provider = subplot.get_data_provider()
        ":type: src.views.xyplot.PlotXYDataProvider"
        self._linestyle_gui.set_value(data_provider.linestyle)
        self._x_data_gui.set_object((None, data_provider.mutable_members['x']))
        self._y_data_gui.set_object((None, data_provider.mutable_members['y']))
        self._linewidth_gui.set_object((None, data_provider.mutable_members['linewidth']))
        self._markersize_gui.set_object((None, data_provider.mutable_members['markersize']))
        self._line_color_gui.set_object(data_provider.color)
        self._face_color_gui.set_object(data_provider.markerfacecolor)
        self._edge_color_gui.set_object(data_provider.markeredgecolor)
        self._fill_style_gui.set_value(data_provider.fillstyle)
        self._markerstyle_gui.set_value(data_provider.marker)
        self._marker_edge_width_gui.set_value(data_provider.markeredgewidth)
        self.updating = False

    def set_parent_window(self, window=None):
        self._line_color_gui.set_parent_window(self.parent_window)
        self._face_color_gui.set_parent_window(self.parent_window)
        self._edge_color_gui.set_parent_window(self.parent_window)


Gui.register_gui_for_class(PlotXY, XYSubplotGui)
