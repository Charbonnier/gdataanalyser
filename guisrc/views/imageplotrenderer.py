import numpy as np
from gi.repository.GdkPixbuf import Pixbuf

from matplotlib.backends.backend_agg import FigureCanvasAgg


def draw_map(cmap, width, height, z_range=None):
    """
    :param matplotlib.colors.Colormap cmap:
    :param int width:
    :param int height:
    :param (float, float) z_range:
    :rtype: gi.repository.GdkPixbuf.Pixbuf, bytes
    :return: Returns the pixbuf and the buffer (keep it while pixbuf is in use)
    """
    if z_range is None:
        z_range = (-0.01, 1.01)
    z_map = np.linspace(z_range[0], z_range[1], width)
    rgb = cmap(np.ones((height, width)) * z_map) * 255
    rgb_bytes = np.int8(rgb)[:, :, 0:3].tobytes()
    pixbuf = Pixbuf.new_from_data(rgb_bytes, 0, False, 8, width, height, 3*width)
    return pixbuf, rgb_bytes


def render_plot_to_pixbuf(figure, width, height):
    """
    :param matplotlib.figure.Figure figure:
    :param int width:
    :param int height:
    :rtype: gi.repository.GdkPixbuf.Pixbuf, bytes
    :return: Returns the pixbuf and the buffer (keep it while pixbuf is in use)
    """
    canvas = FigureCanvasAgg(figure)
    dpi = figure.get_dpi()
    figure.set_size_inches(width / dpi, height / dpi)
    buffer = canvas.print_to_buffer()[0]
    buffer = np.frombuffer(buffer, np.uint8).reshape(width, height, 4)[:, :, 0:3].tobytes()
    return Pixbuf.new_from_data(buffer, 0, False, 8, width, height, 3 * width), buffer
