import os
from abc import abstractmethod
from typing import Optional, List

from gi.repository import GObject, Gtk, GLib

from guisrc.gui import Gui
from src.structure.source import SourceManager, Source
from src.structure.view import ViewRenderer, View, ViewManager
from src.views.mplproperties import Properties
from structure.mutative import FloatGui, BoolGui, ComboGui, build_reference_model_for_view, make_field_row, FIELD_OK, \
    remove_field, rename_field, ConstantGui
from utils import glade_path
from views.colorfactory import ColorGui


class ViewGui(Gui):
    def __init__(self, view_widget: Gtk.Widget):
        super().__init__()
        self.view = None
        self.renderer = self.build_renderer()
        self.rendering_in_progress = False
        self.rendering_pending = None
        self._edit_box_visible = False

        self.last_view_save_path = None

        self.view_widget = view_widget

        self._sm_observer_ids = set()

        builder = Gtk.Builder()
        builder.add_from_file(os.path.join(glade_path(), 'views', 'view.glade'))
        self.edit_btn = builder.get_object('edit_btn')
        self.close_btn = builder.get_object('close_btn')
        self.view_tbx: Gtk.Toolbar = builder.get_object('view_tbx')
        self.view_setup_nbk: Gtk.Notebook = builder.get_object('view_setup_nbk')
        self.main_view_box: Gtk.Box = builder.get_object('main_view_box')
        self.paned_container: Gtk.Paned = builder.get_object('paned_container')
        self.view_container_box: Gtk.Box = builder.get_object('view_box')
        self.view_container_box_paned: Gtk.Box = builder.get_object('view_box_paned')
        self.stack_container: Gtk.Box = builder.get_object('stack_container')

        # Categorizing gui.
        self.categorize_tbtn: Gtk.ToolButton = builder.get_object('categorize_tbtn')
        self.categorize_popover: Gtk.Popover = builder.get_object('categorize_popover')
        self.categorize_enabled_chkbtn: Gtk.CheckButton = builder.get_object('categorize_enabled_chkbtn')
        self.category_field_cmb: Gtk.ComboBox = builder.get_object('category_field_cmb')
        self.category_value_cmb: Gtk.ComboBox = builder.get_object('category_value_cmb')
        self.category_field_cmb.set_model(Gtk.ListStore(str, str, bool, str, str))
        """ field name, field with markup, sensitive, error (none, duplicate, missing)"""
        self.category_value_cmb.set_model(Gtk.ListStore(str))
        """ category value"""
        self.view_container_box.pack_start(self.view_widget, True, True, 0)

        self.stack = Gtk.Stack()
        self.stack.set_transition_type(Gtk.StackTransitionType.OVER_LEFT)
        self.stack.set_transition_duration(200)

        self.plot_edit_stack = Gtk.VBox()
        self.plot_edit_stack.pack_start(self.stack, True, True, 0)
        self.plot_edit_stack.show_all()
        self.plot_edit_stack.hide()

        self.stack.add_titled(self.view_setup_nbk, 'plots', 'Figure')
        self.stack_container.pack_start(self.plot_edit_stack, True, True, 0)

        self._gui = Gtk.HBox()
        self._gui.show()
        self._gui.pack_start(self.main_view_box, True, True, 0)

        builder.connect_signals(self)

    def gui(self):
        return self._gui

    @abstractmethod
    def build_renderer(self) -> ViewRenderer:
        raise NotImplementedError()

    def set_context(self, context):
        if self.context is not None:
            sm = self.context.get_source_manager()
            for obs_id in self._sm_observer_ids:
                sm.remove_event_observer(obs_id)
        super().set_context(context)
        sm = self.context.get_source_manager()
        self._sm_observer_ids.clear()
        self._sm_observer_ids.add(sm.add_event_observer(SourceManager.EVENT_FIELD_ADDED, self.on_sm_field_added))
        self._sm_observer_ids.add(sm.add_event_observer(SourceManager.EVENT_FIELD_REMOVED, self.on_sm_field_removed))
        self._sm_observer_ids.add(sm.add_event_observer(SourceManager.EVENT_FIELD_RENAMED, self.on_sm_field_renamed))

    def set_object(self, object_to_follow: View):
        self.view = object_to_follow
        self.updating = True
        try:
            self.categorize_enabled_chkbtn.set_active(object_to_follow.categories_filter.enabled)
            for i, row in enumerate(self.category_field_cmb.get_model()):
                if row[0] == self.view.categories_filter.field:
                    break
            else:
                i = -1
            self.category_field_cmb.set_active(i)
        finally:
            self.updating = False

    def evaluate(self, view_manager: ViewManager):
        view_manager.generate_view(self.view, self.renderer, self.view.category_index)

    @property
    def edit_box_visible(self):
        return self._edit_box_visible

    def get_file_filter(self):
        return None

    def select_save_path(self):
        save_dlg = Gtk.FileChooserDialog(title='Save view', action=Gtk.FileChooserAction.SAVE,
                                         buttons=(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                                                  Gtk.STOCK_SAVE, Gtk.ResponseType.OK))
        save_dlg.set_transient_for(self.main_window)
        file_filter = self.get_file_filter()
        if file_filter:
            save_dlg.set_filter(file_filter)
        if self.last_view_save_path:
            save_dlg.set_filename(self.last_view_save_path)
        response = save_dlg.run()
        path = save_dlg.get_filename()
        save_dlg.destroy()
        if response == Gtk.ResponseType.OK:
            return path
        else:
            return None

    def hide_edit_box(self):
        if not self.edit_box_visible:
            return
        self.do_hide_edit_box()

    def show_edit_box(self):
        if self.edit_box_visible:
            return
        self.do_show_edit_box()

    def do_hide_edit_box(self):
        self._edit_box_visible = False
        self.paned_container.hide()
        self.view_container_box.show()
        self.edit_btn.show()
        self.close_btn.hide()
        self.view_container_box_paned.remove(self.category_value_cmb)
        self.view_container_box.pack_start(self.category_value_cmb, False, False, 0)
        self.view_container_box_paned.remove(self.view_widget)
        self.view_container_box.pack_start(self.view_widget, True, True, 0)
        self.plot_edit_stack.hide()

    def do_show_edit_box(self):
        self._edit_box_visible = True
        self.paned_container.show()
        self.view_container_box.hide()
        self.edit_btn.hide()
        self.close_btn.show()
        self.view_container_box.remove(self.category_value_cmb)
        self.view_container_box_paned.pack_start(self.category_value_cmb, False, False, 0)
        self.view_container_box.remove(self.view_widget)
        self.view_container_box_paned.pack_start(self.view_widget, True, True, 0)
        self.plot_edit_stack.show()

    def reload_data(self):
        if self.rendering_in_progress:
            self.rendering_pending = (self.reload_data, ())
            return
        self.rendering_in_progress = True
        self.evaluate(self.context.get_view_manager())
        self.build_categories_value()

    def on_end_rendering_figure(self, canvas, data=None):
        if self.rendering_in_progress:
            self.rendering_in_progress = False
            GLib.idle_add(self.run_pending_rendering)

    # TODO: remove obselete method
    def on_fields_changed(self):
        if self.view is not None:
            new_model = Gtk.ListStore(str, str, bool, str, str)
            build_reference_model_for_view(self.context, new_model,
                                           self.view.categories_filter.field)
            self.category_field_cmb.set_model(new_model)

    def on_sm_field_added(self, sm: SourceManager, source: Source, field: str):
        self.category_value_cmb.get_model().append(make_field_row(field, source.name, FIELD_OK, True))

    def on_sm_field_removed(self, sm: SourceManager, source: Source, field: str, missing: bool):
        remove_field(self.category_value_cmb.get_model(), source, field, missing)

    def on_sm_field_renamed(self, sm: SourceManager, source: Source, old_field: str, new_field: str):
        rename_field(self.category_value_cmb.get_model(), old_field, new_field)

    def on_close_btn_clicked_cb(self, widget, data=None):
        self.hide_edit_box()

    def on_edit_btn_clicked_cb(self, widget, data=None):
        self.show_edit_box()

    def on_refresh_btn_clicked_cb(self, widget, data=None):
        self.reload_data()

    def on_categorize_tbtn_clicked(self, widget, data=None):
        self.categorize_popover.popup()

    def on_categories_chkbtn_toggled(self, widget: Gtk.CheckButton, data=None):
        """

        :param widget:
        :param data:
        :return:
        """
        if widget.get_active():
            enabled = True
            self.categorize_tbtn.set_icon_name('categorize')
            self.category_field_cmb.set_sensitive(True)
            self.category_value_cmb.show()
        else:
            enabled = False
            self.categorize_tbtn.set_icon_name('uncategorize')
            self.category_field_cmb.set_sensitive(False)
            self.category_value_cmb.hide()
        if self.updating:
            return
        self.view.categories_filter.enabled = enabled

    def on_save_btn_clicked(self, widget, data=None):
        path = self.select_save_path()

        if path is not None:
            self.last_view_save_path = path
            self.view.export(force=True, export_path=path)

    def on_save_all_categories_btn_clicked(self, widget, data=None):
        path = self.select_save_path()

        if path is not None:
            self.last_view_save_path = path
            self.context.get_view_manager().export_view_all_categories(self.view, export_path=path)

    def on_category_field_cmb_changed(self, widget: Gtk.ComboBox, data=None):
        """

        :param widget:
        :param data:
        :return:
        """
        if not self.updating:
            index = widget.get_active()
            if index < 0:
                self.view.categories_filter.field = None
                return
            model = widget.get_model()
            self.view.categories_filter.field = model[index][0]
        self.reload_category_filter()

    def on_category_value_cmb_changed(self, widget: Gtk.ComboBox, data=None):
        """

        :param widget:
        :param data:
        :return:
        """
        if self.updating:
            return
        self.view.category_index = widget.get_active()
        self.reload_categorized_items()

    def build_categories_value(self):
        if self.view.categories_filter.enabled:
            liststore = Gtk.ListStore(str)
            for category in self.view.categories_filter.get_categories():
                liststore.append([str(category)])
            self.updating = True
            self.category_value_cmb.set_model(liststore)
            if self.view.category_index is not None:
                if self.view.category_index < len(liststore):
                    index = self.view.category_index
                else:
                    index = 0
                self.category_value_cmb.set_active(index)
            self.updating = False

    def redraw_view(self):
        if self.view is not None:
            self.view.generate_view(self.renderer)

    def reload_category_filter(self):
        if self.rendering_in_progress:
            self.rendering_pending = (self.reload_category_filter, ())
            return
        self.rendering_in_progress = True
        try:
            self.view.evaluate_category_filter(self.context.get_view_manager().database)
        except KeyError:
            pass
        else:
            self.build_categories_value()
            self.view.evaluate_category(self.context.get_view_manager().database, category_index=self.view.category_index)
            self.redraw_view()
        finally:
            self.rendering_in_progress = False

    def reload_categorized_items(self):
        if self.rendering_in_progress:
            self.rendering_pending = (self.reload_categorized_items, ())
            return
        self.rendering_in_progress = True
        self.view.evaluate_category(self.context.get_view_manager().database, category_index=self.view.category_index)
        self.redraw_view()

    def run_pending_rendering(self):
        if self.rendering_pending is None:
            return
        fct, args = self.rendering_pending
        self.rendering_pending = None
        fct(*args)


class ViewSetting(Gtk.Box):
    def __init__(self, title: str, properties_gui: Optional['PropertiesGui'] = None):
        """

        :param title:
        :param properties_gui:
        """
        super().__init__()
        self.properties_gui = properties_gui
        self.setting_box: Optional[Gtk.Box] = None
        self.parameters_box: Optional[Gtk.Box] = None
        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(glade_path(), 'views', 'settings.glade'),
                                      ('setting_box', 'properties_pop'))
        self.setting_box: Gtk.Box = builder.get_object('setting_box')
        self.title_box: Gtk.Box = builder.get_object('title_box')
        self.parameters_box: Gtk.Box = builder.get_object('parameters_box')
        self.properties_pop: Gtk.Popover = builder.get_object('properties_pop')
        artist_title: Gtk.Label = builder.get_object('artist_title')

        artist_title.set_label(title)
        if properties_gui is not None:
            self.properties_pop.add(self.properties_gui.gui())
        else:
            show_properties_tbt = builder.get_object('show_properties_tbt')
            show_properties_tbt.set_sensitive(False)
        self.pack_start(self.setting_box, False, False, 0)
        builder.connect_signals(self)
        self.show()

    def on_show_properties_tbt_clicked(self, button):
        self.properties_pop.popup()


class ViewSettingConstant(ViewSetting):
    def __init__(self, title: str, constant_gui: ConstantGui, properties_gui: Optional['PropertiesGui'] = None):
        """

        :param title:
        :param constant_gui:
        :param properties_gui:
        """
        super().__init__(title, properties_gui)
        self.constant_gui = constant_gui
        self.constant_gui.on_value_changed = self.on_constant_changed
        self.parameters_box.pack_start(constant_gui.gui, False, False, 0)

    def on_constant_changed(self, constant_gui):
        self.emit('value_changed')

    def set_value(self, value):
        self.constant_gui.set_value(value)

    def get_value(self):
        return self.constant_gui.get_value()


GObject.type_register(ViewSetting)
GObject.signal_new("value_changed", ViewSetting, GObject.SIGNAL_RUN_LAST,
                   GObject.TYPE_BOOLEAN, ())


class SettingPropertyGui(Gui):
    def __init__(self, property: str, title: str = None):
        """
        :param property: The property to set
        :param title: Optional name for the property
        """
        super().__init__()
        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(glade_path(), 'views', 'settings.glade'),
                                      ('property_box',))
        self._gui: Gtk.Box = builder.get_object('property_box')
        self._user_defined: Gtk.CheckButton = builder.get_object('user_defined')
        self.property_container: Gtk.Box = builder.get_object('property_container')

        self.property_name = property
        if title is None:
            title = property
        self._user_defined.set_label(title)
        self.mplproperties: Optional[Properties] = None

        builder.connect_signals(self)

    def set_object(self, object_to_follow: Properties):
        """
        :param object_to_follow:
        :return:
        """
        self.mplproperties = object_to_follow
        self.updating = True
        try:
            if self.mplproperties is not None:
                self._user_defined = not self.mplproperties.use_default[self.property_name]
                self.show_property_value(self.mplproperties.properties[self.property_name])
        finally:
            self.updating = False

    @abstractmethod
    def show_property_value(self, value):
        """
        Displays the property of mplproperties
        :return:
        """
        raise NotImplementedError()

    def set_property_value(self):
        """
        Called when the property widget has changed.
        :return:
        """
        if self.mplproperties is not None:
            self.mplproperties.properties[self.property_name] = self.get_value_from_widget()

    @abstractmethod
    def get_value_from_widget(self):
        """
        Sets the property of mplproperties
        :return: the value to set
        """
        raise NotImplementedError

    def on_user_defined_toggled(self, widget: Gtk.CheckButton, data=None):
        """
        :param widget:
        :param data:
        :return:
        """
        use_default = not widget.get_active()
        self.property_container.set_sensitive(not use_default)
        if self.updating:
            return
        if self.mplproperties is not None:
            self.mplproperties.use_default[self.property_name] = use_default

    def gui(self):
        return self._gui


class PropertiesGui(Gui):
    def __init__(self):
        super().__init__()
        self._gui = Gtk.ListBox()
        self._gui.show()
        self._gui.set_selection_mode(Gtk.SelectionMode.NONE)
        self.property_gui_lst: List[PropertiesGui] = []
        self.mplproperties: Optional[Properties] = None
        self.build_properties()

    @abstractmethod
    def build_properties(self):
        raise NotImplementedError()

    def add_property(self, prop: SettingPropertyGui):
        """
        :param prop:
        :return:
        """
        self.property_gui_lst.append(prop)
        self._gui.add(prop.gui())

    def set_object(self, object_to_follow):
        self.mplproperties = object_to_follow

        self.updating = True
        try:
            for prop in self.property_gui_lst:
                prop.set_object(self.mplproperties)
        finally:
            self.updating = False

    def gui(self):
        return self._gui


class ConstantSettingPropertyGui(SettingPropertyGui):
    def __init__(self, property, constant_gui: ConstantGui, title: str = None):
        """
        :param property:
        :param constant_gui:
        :param title:
        """
        super().__init__(property, title)
        self.property_value = constant_gui
        self.property_container.add(self.property_value.gui)
        self.property_value.on_value_changed = self.on_value_changed

    def show_property_value(self, value):
        self.property_value.set_value(value)

    def get_value_from_widget(self):
        return self.property_value.value

    def on_value_changed(self, constantgui: ConstantGui):
        """
        Helper to connect to the on_value_change callback of ConstantGui.
        :param constantgui:
        :return:
        """
        self.set_property_value()


class FloatPropertyGui(ConstantSettingPropertyGui):
    def __init__(self, property, title=None):
        super().__init__(property, FloatGui(), title)


class BoolPropertyGui(ConstantSettingPropertyGui):
    def __init__(self, property, title=None):
        super().__init__(property, BoolGui(), title)


class ColorPropertyGui(ConstantSettingPropertyGui):
    def __init__(self, property, title=None):
        super().__init__(property, ColorGui(), title)


class ComboPropertyGui(ConstantSettingPropertyGui):
    def __init__(self, property: str, model: Gtk.ListStore, title: str=None):
        """
        :param property:
        :param model:
        :param title:
        """
        super().__init__(property, ComboGui(model), title)
