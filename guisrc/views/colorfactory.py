import os

from gi.repository import Gtk, Gdk, GObject, GdkPixbuf
from matplotlib import cm

from gui import Gui, ClassSelectorGui
from src.structure.database import DataBase
from src.views.colorfactory import ColorFactory, Color, ConstantColorFactory, GradientColorFactory, MapColorFactory
from structure.context import Context
from structure.elementsmanager import ElementsManager
from structure.mutative import ConstantGui, MutativeMemberSetterGui, VariantSetterGui
from utils import glade_path
from views.imageplotrenderer import draw_map
from views.scale import ScaleSelectorGui


class ColorGui(ConstantGui):

    def __init__(self):
        self._color_buffer = Gdk.RGBA(1., 1., 1., 0.)
        super().__init__()

    def build_gui(self):
        """
        :rtype: Gtk.ColorButton
        :return:
        """
        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(glade_path(), 'views', 'color_factory.glade'),
                                      ('constant_setup', ))
        gui = builder.get_object('constant_setup')
        ":type: Gtk.Box"
        self.color_btn = builder.get_object('constant_btn')
        ":type: Gtk.ColorButton"
        self.enable_sw = builder.get_object('enable_sw')
        ":type: Gtk.Switch"
        builder.connect_signals(self)

        return gui

    @staticmethod
    def color_to_rgba(color):
        """
        :param str|Color color:
        :rtype: Gdk.RGBA
        :return:
        """
        rgba = None
        if isinstance(color, str):
            rgba = Gdk.RGBA()
            rgba.parse(color)
            return rgba
        else:
            rgba = Gdk.RGBA(*color)
        return rgba

    @staticmethod
    def rgba_to_color(rgba):
        """
        :param Gdk.RGBA rgba:
        :rtype:  Color
        :return:
        """
        return Color((rgba.red, rgba.green, rgba.blue, rgba.alpha))

    def on_constant_btn_color_set(self, widget, data=None):
        if self.updating:
            return
        self.value = ColorGui.rgba_to_color(self.color_btn.get_rgba())
        self.value_changed()

    def on_enable_sw_active_notify(self, widget, data=None):
        """
        :param Gtk.Switch widget:
        :param data:
        :return:
        """
        color_active = widget.get_active()
        self.color_btn.set_sensitive(color_active)
        if self.updating:
            return
        if color_active:
            self.color_btn.set_rgba(self._color_buffer)
        else:
            self._color_buffer = self.color_btn.get_rgba()
            self.color_btn.set_rgba(Gdk.RGBA(1., 1., 1., 0.))

    def display_value(self, value):
        self.updating = True
        try:
            if value == (1., 1., 1., 0.) or value is None:
                self.enable_sw.set_active(False)
            else:
                self.enable_sw.set_active(True)
            if value is not None:
                self.color_btn.set_rgba(ColorGui.color_to_rgba(value))
        except IndexError:
            pass
        finally:
            self.updating = False


ConstantGui.register_gui(Color, ColorGui)


class ColorSetterGui(MutativeMemberSetterGui):
    def __init__(self):
        super().__init__(ColorGui())


class ColorFactoryGui(ClassSelectorGui):
    factory_model = None

    def __init__(self):
        if ColorFactoryGui.factory_model is None:
            ColorFactoryGui.factory_model = Gtk.ListStore(str, GObject.TYPE_PYOBJECT)
            for factory, name in ColorFactory.classes[ColorFactory.class_path()].items():
                ColorFactoryGui.factory_model.append([name, factory])
        super().__init__(ColorFactoryGui.factory_model)


class ConstColorFactoryGui(Gui):
    def __init__(self):
        super().__init__()
        self._color_setter_gui = ColorSetterGui()

    def gui(self):
        return self._color_setter_gui.gui()

    def set_object(self, color_factory):
        """
        :param src.views.colorfactory.ConstantColorFactory color_factory:
        :return:
        """
        self._color_setter_gui.set_object((None, color_factory.mutable_members['colors']))


class MapColorFactoryGui(Gui):
    maps_model = None
    ":type: Gtk.ListStore[GdkPixbuf.Pixbuf, str, bytes]"

    def __init__(self):
        super().__init__()
        if MapColorFactoryGui.maps_model is None:
            MapColorFactoryGui.maps_model = Gtk.ListStore(GdkPixbuf.Pixbuf, str, GObject.TYPE_PYOBJECT)
            for i, name in enumerate(sorted(m for m in cm.datad if not m.endswith("_r"))):
                pixbuf, buffer = draw_map(cm.get_cmap(name), 150, 15, (0, 1))
                MapColorFactoryGui.maps_model.append([pixbuf, name, buffer])

        self._gradient_thumbnail = None
        ":type: Gtk.Image"
        self._under_cbt = None
        ":type: Gtk.ColorButton"
        self._above_cbt = None
        ":type: Gtk.ColorButton"
        self._setup_window = None
        ":type: Gtk.Window"
        self._color_parameter_gui = VariantSetterGui()

        self.color_factory = self.make_color_factory()
        self.scale_gui = ScaleSelectorGui()
        self.build_gui()
        self._prv_under_color = None
        self._prv_above_color = None
        self._gradient_bytes = None
        self._gui.pack_start(self._color_parameter_gui.gui(), False, False, 0)

    def make_color_factory(self):
        return MapColorFactory()

    def build_gui(self):
        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(glade_path(), 'views', 'color_factory.glade'),
                                      ('map_factory', 'map_setup_window'))
        self._gui = builder.get_object('map_factory')
        ":type: Gtk.Box"
        self._gradient_thumbnail = builder.get_object('gradient_thumbnail')
        ":type: Gtk.Image"
        self._setup_window = builder.get_object('map_setup_window')
        ":type: Gtk.Window"
        map_setup_box = builder.get_object('map_setup_box')
        ":type: Gtk.Box"
        scale_exp = builder.get_object('scale_exp')
        ":type: Gtk.Expander"
        map_setup_box.pack_start(self.build_setup_window_content(), False, False, 0)
        scale_exp.add(self.scale_gui.gui())
        builder.connect_signals(self)

    def build_setup_window_content(self):
        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(glade_path(), 'views', 'color_factory.glade'),
                                      ('map_setup', ))
        self.map_cmb = builder.get_object('map_cmb')
        ":type: Gtk.ComboBox"
        self._under_cbt = builder.get_object('map_under_cbt')
        ":type: Gtk.ColorButton"
        self._above_cbt = builder.get_object('map_above_cbt')
        ":type: Gtk.ColorButton"
        self.map_cmb.set_model(MapColorFactoryGui.maps_model)
        builder.connect_signals(self)
        return builder.get_object('map_setup')

    def undo_setup_change(self):
        self.color_factory.under = self._prv_under_color
        self.color_factory.over = self._prv_above_color

    def save_current_setup(self):
        self._prv_under_color = self.color_factory.under
        self._prv_above_color = self.color_factory.over

    def set_parent_window(self, window=None):
        super().set_parent_window(window)
        self._setup_window.set_transient_for(window)
        self._color_parameter_gui.set_parent_window(window)

    def set_context(self, context):
        super().set_context(context)
        self._color_parameter_gui.set_context(context)

    def set_object(self, object_to_follow):
        """
        :param GradientColorFactory object_to_follow:
        :return:
        """
        self.updating = True
        try:
            self.color_factory = object_to_follow
            self.scale_gui.set_object(object_to_follow.color_scale)
            self._color_parameter_gui.set_object((None, object_to_follow.mutable_members['color_parameter']))
            self._update_gui()
        finally:
            self.updating = False

    def gui(self):
        return self._gui

    def _update_gui(self):
        cur_map_name = self.color_factory.map.name
        for i, (_, map_name, _) in enumerate(MapColorFactoryGui.maps_model):
            if cur_map_name == map_name:
                self.map_cmb.set_active(i)
                break

        self._set_color_buttons()

    def draw_gradient(self, width, height):
        return draw_map(self.color_factory.map, width, height)

    def _set_color_buttons(self):
        self._under_cbt.set_rgba(ColorGui.color_to_rgba(self.color_factory.under))
        self._above_cbt.set_rgba(ColorGui.color_to_rgba(self.color_factory.over))

    def _redraw_gradients(self):
        self._gradient_thumbnail.queue_draw()

    def on_gradient_setup_btn_clicked(self, widget, data=None):
        self.save_current_setup()
        response = self._setup_window.run()
        if response < 0:
            self.undo_setup_change()
            self._set_color_buttons()
        self._setup_window.hide()

    def on_map_cmb_changed(self, widget, data=None):
        """
        :param Gtk.ComboBox widget:
        :param data:
        :return:
        """
        cmap, cmap_name, _ = MapColorFactoryGui.maps_model[widget.get_active()]
        self.color_factory.set_map_from_name(cmap_name)
        self._redraw_gradients()

    def on_under_cbt_color_set(self, widget, data=None):
        """
        :param Gtk.ColorButton widget:
        :param data:
        :return:
        """
        self.color_factory.under = ColorGui.rgba_to_color(widget.get_rgba())
        self._redraw_gradients()

    def on_above_cbt_color_set(self, widget, data=None):
        """
        :param Gtk.ColorButton widget:
        :param data:
        :return:
        """
        self.color_factory.over = ColorGui.rgba_to_color(widget.get_rgba())
        self._redraw_gradients()

    def on_gradient_thumbnail_draw(self, widget, cr):
        """

        :param Gtk.Image widget:
        :param cairo.Context cr:
        :return:
        """
        width = widget.get_allocated_width()
        height = widget.get_allocated_height()
        pixbuf, buffer = self.draw_gradient(width, height)
        Gdk.cairo_set_source_pixbuf(cr, pixbuf, 0, 0)
        cr.paint()
        return True


class GradientColorFactoryGui(MapColorFactoryGui):
    def __init__(self):
        self.color_factory = None
        ":type: GradientColorFactory"
        self._gui = None
        ":type: Gtk.Box"
        self._gradient_image = None
        ":type: Gtk.Image"
        self._low_cbt = None
        ":type: Gtk.ColorButton"
        self._high_cbt = None
        ":type: Gtk.ColorButton"

        super().__init__()
        self._prv_low_color = None
        self._prv_high_color = None

    def make_color_factory(self):
        return GradientColorFactory()

    def build_setup_window_content(self):
        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(glade_path(), 'views', 'color_factory.glade'),
                                      ('gradient_setup', ))
        self._gradient_image = builder.get_object('gradient_image')
        ":type: Gtk.Image"
        self._under_cbt = builder.get_object('under_cbt')
        ":type: Gtk.ColorButton"
        self._low_cbt = builder.get_object('low_cbt')
        ":type: Gtk.ColorButton"
        self._high_cbt = builder.get_object('high_cbt')
        ":type: Gtk.ColorButton"
        self._above_cbt = builder.get_object('above_cbt')
        ":type: Gtk.ColorButton"
        builder.connect_signals(self)
        return builder.get_object('gradient_setup')

    def gui(self):
        return self._gui

    def _update_gui(self):
        self._set_color_buttons()

    def _set_color_buttons(self):
        super()._set_color_buttons()
        self._low_cbt.set_rgba(ColorGui.color_to_rgba(self.color_factory.start_color))
        self._high_cbt.set_rgba(ColorGui.color_to_rgba(self.color_factory.end_color))

    def undo_setup_change(self):
        self.color_factory.under = self._prv_under_color
        self.color_factory.start_color = self._prv_low_color
        self.color_factory.end_color = self._prv_high_color
        self.color_factory.over = self._prv_above_color

    def save_current_setup(self):
        self._prv_under_color = self.color_factory.under
        self._prv_low_color = self.color_factory.start_color
        self._prv_high_color = self.color_factory.end_color
        self._prv_above_color = self.color_factory.over

    def _redraw_gradients(self):
        super()._redraw_gradients()
        self._gradient_image.queue_draw()

    def on_low_cbt_color_set(self, widget, data=None):
        """
        :param Gtk.ColorButton widget:
        :param data:
        :return:
        """
        self.color_factory.start_color = ColorGui.rgba_to_color(widget.get_rgba())
        self._redraw_gradients()

    def on_high_cbt_color_set(self, widget, data=None):
        """
        :param Gtk.ColorButton widget:
        :param data:
        :return:
        """
        self.color_factory.end_color = ColorGui.rgba_to_color(widget.get_rgba())
        self._redraw_gradients()


Gui.register_gui_for_class(ConstantColorFactory, ConstColorFactoryGui)
Gui.register_gui_for_class(MapColorFactory, MapColorFactoryGui)
Gui.register_gui_for_class(GradientColorFactory, GradientColorFactoryGui)

if __name__ == "__main__":
    context = Context(ElementsManager(DataBase()))
    ccf = ColorFactoryGui()
    ccf.set_context(context)
    ccf.set_object(ConstantColorFactory())
    box = Gtk.Box()
    box.pack_start(ccf.gui(), True, True, 0)
    box.show()
    window = Gtk.Window()
    ccf.set_parent_window(window)
    window.connect('delete-event', lambda a, w: Gtk.main_quit())
    window.add(box)
    window.show()
    Gtk.main()
