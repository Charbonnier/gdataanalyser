from gui import Gui


class SubplotGui(Gui):
    def __init__(self):
        super().__init__()
        self._plot = None
        self._subplot = None

    def set_object(self, subplot):
        self._subplot = subplot
