import os
import time

from gi.repository import Gtk, GObject

from external.mpl_gtk3_backend.backend_gtk3 import FigureCanvasContainer, END_RENDERING_FIGURE_SIGNAL, NavigationToolBar2GTK3
from guisrc.gui import Gui
from src.structure.source import SourceManager
from src.views.plot import PlotRenderer, Plot, PlotView
from src.views.xyplot import PlotXY
from src.views.textplot import PlotText
from structure.mutative import StringSetterGui, BoolGui, FloatGui
from utils import glade_path
from views.mplproperties import TextPropertiesGui, GridPropertiesGui
from views.scale import ScaleSelectorGui
from views.view import ViewGui, ViewSettingConstant, ViewSetting
from widgets.listboxmanager import ManagedListBox

from guisrc.views.xyplot import XYSubplotGui
from guisrc.views.textplot import TextSubplotGui


class PlotGuiRenderer(PlotRenderer):
    def __init__(self) -> None:
        self.container = None
        ":type: FigureCanvasContainer"
        super().__init__()

    def build_canvas(self):
        spinner = Gtk.Spinner()
        spinner.connect('show', lambda w: w.start())
        spinner.connect('hide', lambda w: w.stop())
        self.container = FigureCanvasContainer(self.figure, spinner)
        canvas = self.container.canvas
        self.container.show()
        return canvas

    def set_need_redraw(self):
        super().set_need_redraw()
        self.canvas.user_request_redraw = True

    def clear_need_redraw(self):
        super().clear_need_redraw()
        self.canvas.user_request_redraw = False


# noinspection PyUnusedLocal
class SubplotElement(Gtk.Box):
    visible_pixbuf = None
    hidden_pixbuf = None

    def __init__(self, plot):
        """
        :param plot:
        """
        super().__init__()
        self.plot = plot
        builder = Gtk.Builder()
        builder.add_from_file(os.path.join(glade_path(), 'views', 'plot_subplot.glade'))
        self.subplot_element = builder.get_object('subplot_element')
        self.visible_ttb = builder.get_object('visible_ttb')
        ":type: Gtk.ToggleToolButton"
        self.category_ttb = builder.get_object('category_ttb')
        ":type: Gtk.ToggleToolButton"
        self.pack_start(self.subplot_element, True, True, 0)
        builder.connect_signals(self)
        self.subplot = None
        ":type src.views.plot.Plot"
        self.context = None
        ":type structure.context.Context"
        self.subplot_gui = None
        self.set_subplot(None)

    def on_subplot_edit_clicked(self, widget, data=None):
        self.emit('suplot_edit_clicked')

    def on_visible_ttb_toggled(self, widget, data=None):
        """

        :param Gtk.ToggleToolButton widget:
        :param data:
        :return:
        """
        if widget.get_active():
            widget.set_icon_name('visible')
        else:
            widget.set_icon_name('hidden')

        if self.subplot is not None:
            self.subplot.rendering_enabled = widget.get_active()
        self.emit('visibility_toggled')

    def on_category_ttb_toggled(self, widget, data=None):
        """

        :param Gtk.ToggleToolButton widget:
        :param data:
        :return:
        """
        if widget.get_active():
            widget.set_icon_name('categorize')
        else:
            widget.set_icon_name('uncategorize')

        if self.subplot is not None:
            self.subplot.categories_enabled = widget.get_active()
        self.emit('categorize_toggled')

    def set_context(self, context):
        """
        :param structure.context.Context context:
        """
        self.context = context

    def set_subplot(self, subplot):
        """
        :param src.views.plot.Plot subplot:
        :return:
        """
        start = time.time()
        self.subplot = subplot
        self.subplot_gui = Gui.get_gui_class_for_object(self.subplot)()
        print(time.time() - start, 'get_gui_class')
        self.subplot_gui.set_context(self.context)
        print(time.time() - start, 'set_context')
        self.subplot_gui.set_object(self.subplot)
        print(time.time() - start, 'set_subplot')
        if subplot is not None:
            self.visible_ttb.set_active(subplot.rendering_enabled)
            self.category_ttb.set_active(subplot.categories_enabled)


GObject.type_register(SubplotElement)
GObject.signal_new("suplot_edit_clicked", SubplotElement, GObject.SIGNAL_RUN_LAST,
                   GObject.TYPE_BOOLEAN, ())
GObject.signal_new("visibility_toggled", SubplotElement, GObject.SIGNAL_RUN_LAST,
                   GObject.TYPE_BOOLEAN, ())
GObject.signal_new("categorize_toggled", SubplotElement, GObject.SIGNAL_RUN_LAST,
                   GObject.TYPE_BOOLEAN, ())


class MplItemGui(Gui):
    def __init__(self, title, properties_gui):
        """
        :param str title:
        :param views.view.PropertiesGui|None properties_gui:
        """
        super().__init__()
        self.view_setting = ViewSetting(title, properties_gui)
        self.properties_gui = properties_gui

    def gui(self):
        return self.view_setting

    def set_object(self, object_to_follow):
        if self.properties_gui is not None:
            self.properties_gui.set_object(object_to_follow)


class MplTitleSetterGui(MplItemGui):
    def __init__(self, title):
        super().__init__(title, TextPropertiesGui())
        self.title_entry = StringSetterGui()
        self.view_setting.parameters_box.pack_start(self.title_entry.gui(), True, True, 0)

    def set_context(self, context):
        super().set_context(context)
        self.title_entry.set_context(context)

    def set_object(self, object_to_follow):
        """
        :param src.views.plot.PlotTitle object_to_follow:
        :return:
        """
        super().set_object(object_to_follow.title_properties)
        self.updating = True
        try:
            self.properties_gui.set_object(object_to_follow.title_properties)
            self.title_entry.set_object((None, object_to_follow.mutable_members['title']))
        finally:
            self.updating = False


# noinspection PyUnusedLocal
class GridGui(MplItemGui):
    def __init__(self, title):
        super().__init__(title, GridPropertiesGui())
        self.enabled = BoolGui()
        self.enabled.on_value_changed = self.on_enabled_toggled
        self.view_setting.title_box.pack_end(self.enabled.gui, False, False, 0)
        self.grid = None

    def on_enabled_toggled(self, bool_gui):
        self.grid.enabled = self.enabled.value

    def set_object(self, object_to_follow):
        """
        :param src.views.mplproperties.GridProperties object_to_follow:
        :return:
        """
        super().set_object(object_to_follow)
        self.grid = object_to_follow
        self.updating = True
        try:
            self.enabled.set_value(object_to_follow.enabled)
        finally:
            self.updating = False


class ScaleGui(MplItemGui):
    def __init__(self, title):
        super().__init__(title, None)
        self.scale = None
        self.scale_gui = ScaleSelectorGui()
        self.view_setting.parameters_box.pack_start(self.scale_gui.gui(), False, False, 0)

    def set_object(self, object_to_follow):
        super().set_object(object_to_follow)
        self.scale = object_to_follow
        self.updating = True
        try:
            self.scale_gui.set_object(self.scale)
        finally:
            self.updating = False


# noinspection PyUnusedLocal
class AxisGui(Gui):
    def __init__(self, nameget_size_request):
        super().__init__()
        self._gui = Gtk.VBox()
        self.axis = None
        self.title_gui = MplTitleSetterGui('Label')
        self.major_grid_gui = GridGui('Major Grid')
        self.minor_grid_gui = GridGui('Minor Grid')
        self.scale = ScaleGui('Scale')
        self.scale.scale_gui.on_object_class_changed = self.on_scale_class_changed
        self._gui.pack_start(self.title_gui.gui(), False, False, 5)
        self._gui.pack_start(self.major_grid_gui.gui(), False, False, 5)
        self._gui.pack_start(self.minor_grid_gui.gui(), False, False, 5)
        self._gui.pack_start(self.scale.gui(), False, False, 5)
        self._gui.show()

    def gui(self):
        return self._gui

    def set_context(self, context):
        super().set_context(context)
        self.title_gui.set_context(context)

    def set_object(self, axis):
        """
        :param src.views.plot.Axis axis:
        :return:
        """
        self.axis = axis
        self.title_gui.set_object(axis.title)
        self.major_grid_gui.set_object(axis.grid_major)
        self.minor_grid_gui.set_object(axis.grid_minor)
        self.scale.set_object(axis.scale)

    def on_scale_class_changed(self, scale_gui):
        self.axis.scale = self.scale.scale_gui.displayed_object


# noinspection PyUnusedLocal
class PlotGui(ViewGui):
    subplot_model = None

    def __init__(self):
        if PlotGui.subplot_model is None:
            PlotGui.subplot_model = Gtk.ListStore(str, GObject.TYPE_PYOBJECT)
            for subplot, name in Plot.classes[Plot.class_path()].items():
                PlotGui.subplot_model.append([name, subplot])
        self.view = None
        ":type: src.views.plot.PlotView"
        self.view_widget = None
        ":type: Gtk.ScrolledWindow"

        builder = Gtk.Builder()
        builder.add_from_file(os.path.join(glade_path(), 'views', 'plot.glade'))
        plot_container = builder.get_object('plot_container')
        ":type: Gtk.Box"
        super().__init__(plot_container)

        self.view_setup_nbk.append_page(builder.get_object('figure_page'), Gtk.Label('Figure'))
        self.view_setup_nbk.append_page(builder.get_object('axes_page'), Gtk.Label('Axes'))
        self.view_setup_nbk.append_page(builder.get_object('plot_page'), Gtk.Label('Plot'))

        separator = Gtk.SeparatorToolItem()
        separator.show()
        self.view_tbx.add(separator)
        self.view_tbx.add(builder.get_object('zoom_tgl'))

        self.fullscale_plot_scrl = builder.get_object('fullscale_plot_scrl')
        ":Type: Gtk.ScrolledWindow"
        self.figure_setup_box = builder.get_object('figure_setup_box')
        ":type: Gtk.Box"
        self.axes_setup_box = builder.get_object('axes_setup_box')
        ":type: Gtk.Box"
        self.plots_box = builder.get_object('plots_box')
        ":type: Gtk.Box"
        self.subplots_tv = builder.get_object('subplots_tv')
        ":type: Gtk.TreeView"
        self.axis_select_cmb = builder.get_object('axis_select_cmb')
        ":type: Gtk.ComboBox"
        self.subplot_container = builder.get_object('subplot_container')
        ":type: Gtk.Box"
        self.fullscale_plot_vprt = builder.get_object('fullscale_plot_vprt')
        ":type: Gtk.Viewport"
        self._subplots_dlg = builder.get_object('subplots_dlg')
        ":type: Gtk.Dialog"
        self.navigation = NavigationToolBar2GTK3(self.renderer.canvas)
        plot_container.pack_end(self.navigation, False, False, 0)

        self.subplots_tv.set_model(PlotGui.subplot_model)

        self.figure_title = MplTitleSetterGui('Title')
        self.figure_width = ViewSettingConstant('Width (inch)', FloatGui())
        self.figure_height = ViewSettingConstant('Height (inch)', FloatGui())
        self.figure_dpi = ViewSettingConstant('DPI', FloatGui())
        self.figure_width.connect('value_changed', self.on_figure_width_changed)
        self.figure_height.connect('value_changed', self.on_figure_height_changed)
        self.figure_dpi.connect('value_changed', self.on_figure_dpi_changed)
        self.figure_setup_box.pack_start(self.figure_title.gui(), False, False, 0)
        self.figure_setup_box.pack_start(self.figure_width, False, False, 0)
        self.figure_setup_box.pack_start(self.figure_height, False, False, 0)
        self.figure_setup_box.pack_start(self.figure_dpi, False, False, 0)

        self.axis_liststore = Gtk.ListStore(str, str, str)
        self.axis_select_cmb.set_model(self.axis_liststore)

        self.axis_gui = AxisGui('x')
        self.axis_gui.gui().hide()

        self.axes_setup_box.pack_start(self.axis_gui.gui(), False, False, 0)

        self.plot_view = None
        ":type: PlotView"

        self.plot_list = ManagedListBox()
        self.plot_list.manager.set_child_builder(self.build_subplot_element_cb)
        self.plots_box.pack_start(self.plot_list, True, True, 0)

        self.stack.add_titled(self.subplot_container, 'subplot', 'Edit')

        self.fullscale_plot_vprt.add(self.renderer.container)

        builder.connect_signals(self)
        self.plot_list.manager.connect('child_added', self.on_child_added)
        self.plot_list.manager.connect('child_removed', self.on_child_removed)
        self.plot_list.manager.connect('move_up', self.on_child_move_up)
        self.plot_list.manager.connect('move_down', self.on_child_move_down)
        self.renderer.canvas.connect(END_RENDERING_FIGURE_SIGNAL, self.on_end_rendering_figure)

    @property
    def subplot_edit_visible(self):
        return self.subplot_container is self.stack.get_visible_child()

    def set_context(self, context):
        super().set_context(context)
        self.figure_title.set_context(context)
        self.axis_gui.set_context(context)
        for row in self.plot_list.listbox.get_children():
            row.get_child().subplot_gui.set_context(context)

    def get_file_filter(self):
        file_filter = Gtk.FileFilter()
        file_filter.add_mime_type('image/png')
        file_filter.add_mime_type('image/svg+xml')
        return file_filter

    def on_axis_select_cmb_changed(self, widget, data=None):
        active = widget.get_active()
        if active < 0:
            self.axis_gui.gui().hide()
            return
        self.axis_gui.gui().show()
        _, axes_name, axis_name = self.axis_liststore[active]
        axis = self.plot_view.axis_dict[(axes_name, axis_name)]
        self.axis_gui.set_object(axis)

    def on_figure_width_changed(self, view_parameter):
        """

        :param ViewSettingConstant view_parameter:
        :return:
        """
        self.plot_view.figure_width = view_parameter.get_value()

    def on_figure_height_changed(self, view_parameter):
        """

        :param ViewSettingConstant view_parameter:
        :return:
        """
        self.plot_view.figure_height = view_parameter.get_value()

    def on_figure_dpi_changed(self, view_parameter):
        """

        :param ViewSettingConstant view_parameter:
        :return:
        """
        self.plot_view.figure_dpi = view_parameter.get_value()

    def build_subplot_element_cb(self, manager):
        self._subplots_dlg.show()
        response = self._subplots_dlg.run()
        self._subplots_dlg.hide()
        subplot_element = None
        if response == Gtk.ResponseType.OK:
            path, _ = self.subplots_tv.get_cursor()
            if path is not None:
                subplot_iter = PlotGui.subplot_model.get_iter(path)
                subplot = PlotGui.subplot_model[subplot_iter][1]()
                subplot_element = self.build_subplot_element()
                subplot_element.set_subplot(subplot)
        return subplot_element

    def build_subplot_element(self):
        subplot_element = SubplotElement(self)
        subplot_element.connect('suplot_edit_clicked', self.on_subplot_edit_clicked)
        subplot_element.subplot_gui.set_parent_window(self.parent_window)
        return subplot_element

    def set_transient(self, transient):
        super().set_transient(transient)
        self._subplots_dlg.set_transient_for(transient)
        self.navigation.set_window(window)

    def set_parent_window(self, window=None):
        super().set_parent_window(window)
        self._subplots_dlg.set_transient_for(self.parent_window)
        self.navigation.set_window(window)

    def build_renderer(self):
        return PlotGuiRenderer()

    def update_axis(self):
        active = self.axis_select_cmb.get_active()
        self.axis_liststore = Gtk.ListStore(str, str, str)
        for key, axis in self.plot_view.axis_dict.items():
            self.axis_liststore.append([axis.title.title, key[0], key[1]])
        self.axis_select_cmb.set_model(self.axis_liststore)
        self.axis_select_cmb.set_active(active)

    def set_object(self, plot_view):
        """
        :param src.views.plot.PlotView plot_view:
        :return:
        """
        start = time.time()
        super().set_object(plot_view)
        print(time.time()-start, 'super.set_object')
        self.updating = True
        edit_box_visible = self.edit_box_visible
        self.hide_edit_box()
        try:
            self.plot_view = plot_view
            self.update_axis()
            print(time.time()-start, 'update_axis')
            for child in self.plot_list.listbox.get_children():
                self.plot_list.listbox.remove(child)
                print(time.time()-start, 'remove child')
            for plot in plot_view.plots:
                subplot_element = self.build_subplot_element()
                print(time.time()-start, 'subplot_element')
                subplot_element.set_context(self.context)
                print(time.time()-start, 'set_context')
                subplot_element.set_subplot(plot)
                print(time.time()-start, 'set_subplot')
                subplot_element.show()
                print(time.time()-start, 'show')
                self.plot_list.manager.add_child(subplot_element)
                print(time.time()-start, 'add child')
            self.figure_title.set_object(self.plot_view.title)
            print(time.time()-start, 'figure_title')
            self.figure_width.set_value(self.plot_view.figure_width)
            print(time.time()-start, 'figure_width')
            self.figure_height.set_value(self.plot_view.figure_height)
            print(time.time()-start, 'figure_height')
            self.figure_dpi.set_value(self.plot_view.figure_dpi)
            print(time.time()-start, 'figure_dpi')
            if edit_box_visible:
                self.show_edit_box()
            self.navigation.update()
        finally:
            self.updating = False

    def _swap_subplots(self, origin_row, target_row):
        """
        :param Gtk.ListBoxRow origin_row:
        :param Gtk.ListBoxRow target_row:
        :return:
        """
        suplot = origin_row.get_child()
        origin_row.remove(suplot)

        target_subplot = target_row.get_child()
        target_row.remove(target_subplot)
        target_row.add(suplot)
        origin_row.add(target_subplot)

        target_view = self.plot_view.plots[target_row.get_index()]
        self.plot_view.plots[target_row.get_index()] = self.plot_view.plots[origin_row.get_index()]
        self.plot_view.plots[origin_row.get_index()] = target_view

    def reload_data_for_plot(self, subplot):
        if self.rendering_in_progress:
            self.rendering_pending = (self.reload_data_for_plot, (subplot, ))
            return
        self.view.query_data_for_plot(self.context.get_database(), subplot)
        self.redraw_view()

    def redraw_view(self):
        super().redraw_view()
        if self.view is not None:
            self.redraw_canvas()

    def reload_data(self):
        super().reload_data()
        self.redraw_canvas()

    def redraw_canvas(self):
        """ Redraws the canvas, but does not plot the data again.
        """
        self.renderer.set_need_redraw()
        self.renderer.canvas.draw()

    def on_back_to_plot_clicked(self, widget, data=None):
        self.hide_subplot_edit_box()

    def on_child_added(self, manager, child):
        """
        :param widgets.listboxmanager.ListBoxManager manager:
        :param SubplotElement child:
        :return:
        """
        child.connect('visibility_toggled', self.on_subplot_visibility_toggled)
        child.connect('categorize_toggled', self.on_subplot_categorize_toggled)
        if self.updating:
            return
        child.subplot_gui.set_context(self.context)
        self.plot_view.plots.append(child.subplot)

    def on_child_removed(self, manager, index):
        """
        :param widgets.listboxmanager.ListBoxManager manager:
        :param int index:
        :return:
        """
        if self.updating:
            return
        self.plot_view.plots.pop(index)

    def on_child_move_up(self, manager):
        """
        :param widgets.listboxmanager.ListBoxManager manager:
        :return:
        """
        origin_row = manager.get_selected()
        ":type: Gtk.ListBoxRow"
        origin_index = manager.get_selected().get_index()
        target_index = origin_index - 1
        target_row = manager.controlled_listbox.get_row_at_index(target_index)
        ":type: Gtk.ListBoxRow"
        self._swap_subplots(origin_row, target_row)
        return True

    def on_child_move_down(self, manager):
        """
        :param widgets.listboxmanager.ListBoxManager manager:
        :return:
        """
        origin_row = manager.get_selected()
        ":type: Gtk.ListBoxRow"
        origin_index = manager.get_selected().get_index()
        target_index = origin_index + 1
        target_row = manager.controlled_listbox.get_row_at_index(target_index)
        ":type: Gtk.ListBoxRow"
        self._swap_subplots(origin_row, target_row)
        return True

    def on_subplot_visibility_toggled(self, widget, data=None):
        """

        :param SubplotElement widget:
        :param data:
        :return:
        """
        if self.updating:
            return
        self.reload_data_for_plot(widget.subplot)

    def on_subplot_categorize_toggled(self, widget, data=None):
        """

        :param SubplotElement widget:
        :param data:
        :return:
        """
        if self.updating:
            return
        self.reload_data_for_plot(widget.subplot)

    def on_zoom_tgl_toggled(self, widget, data=None):
        """

        :param Gtk.ToggleToolButton widget:
        :param data:
        :return:
        """
        full_size = widget.get_active()
        self.plot_view.setup_figure(self.renderer)
        self.renderer.container.canvas.show_full_size(full_size)

        if full_size:
            widget.set_stock_id(Gtk.STOCK_ZOOM_100)
            self.fullscale_plot_scrl.set_policy(Gtk.PolicyType.ALWAYS, Gtk.PolicyType.ALWAYS)
        else:
            widget.set_stock_id(Gtk.STOCK_ZOOM_FIT)
            self.fullscale_plot_scrl.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.NEVER)

    def do_hide_edit_box(self):
        super().do_hide_edit_box()
        self.hide_subplot_edit_box()

    def hide_subplot_edit_box(self):
        if not self.subplot_edit_visible:
            return
        self.stack.set_visible_child(self.view_setup_nbk)

    def on_subplot_edit_clicked(self, widget, data=None):
        """
        :param SubplotElement widget:
        :param data:
        :return:
        """
        if self.subplot_edit_visible:
            return
        try:
            self.subplot_container.remove(self.subplot_container.get_children()[1])
        except IndexError:
            pass
        widget.subplot_gui.set_parent_window(self.parent_window)
        self.subplot_container.pack_start(widget.subplot_gui.gui(), True, True, 0)
        self.stack.set_visible_child(self.subplot_container)


Gui.register_gui_for_class(PlotView, PlotGui)

if __name__ == "__main__":
    first_plot = PlotXY()
    second_plot = PlotXY()

    plot_view = PlotView()

    plot_view.plots.append(first_plot)
    plot_view.plots.append(second_plot)

    plot_gui = PlotGui()
    window = Gtk.Window()
    window.set_size_request(800, 600)
    window.add(plot_gui.gui())
    plot_gui.set_parent_window(window)
    plot_gui.set_object(plot_view)
    plot_gui.set_object(plot_view)
    window.connect('delete-event', lambda a, w: Gtk.main_quit())
    window.show()
    Gtk.main()
