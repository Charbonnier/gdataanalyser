import os
from gi.repository import Gtk, GdkPixbuf
from matplotlib.markers import MarkerStyle

from src.utils import make_marker_preview
from structure.mutative import ConstantGui, ComboGui
from utils import glade_path, images_path


class FillStyleGui(ConstantGui):
    FillStyleModel = None

    def __init__(self):
        self.fill_style_cmb = None
        ":type: Gtk.ComboBox"
        super().__init__()

    def build_gui(self):
        """
        :rtype: Gtk.ComboBox
        :return:
        """
        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(glade_path(), 'views', 'plot_style.glade'),
                                      ('fill_style_selector',))
        self.fill_style_cmb = builder.get_object('fill_style_selector')
        ":type: Gtk.ComboBox"
        self.fill_style_cmb.set_model(FillStyleGui.get_fill_style_model())
        builder.connect_signals(self)
        self.fill_style_cmb.show()
        return self.fill_style_cmb

    @staticmethod
    def get_fillstyle_icon_path(fillstyle):
        fillstyle_file = fillstyle + os.path.extsep + '.png'
        fillstyle_folder = os.path.join(images_path(), 'fillstyle')
        return os.path.join(fillstyle_folder, fillstyle_file)

    @staticmethod
    def generate_fillstyle_icon(fillstyle):
        fillstyle_path = FillStyleGui.get_fillstyle_icon_path(fillstyle)
        size = 32
        try:
            make_marker_preview('o', fillstyle_path, facecolor='k', edgecolor='k', previewsize=size,
                                fillstyle=fillstyle)
        except FileNotFoundError:
            os.makedirs(os.path.dirname(fillstyle_path))
            make_marker_preview('o', fillstyle_path, facecolor='k', edgecolor='k', previewsize=size,
                                fillstyle=fillstyle)

    @staticmethod
    def get_fill_style_model():
        if FillStyleGui.FillStyleModel is not None:
            return FillStyleGui.FillStyleModel

        model = Gtk.ListStore(GdkPixbuf.Pixbuf, str, str)
        fs_list = ['none', 'full', 'left', 'right', 'top', 'bottom']

        icons = [(FillStyleGui.get_fillstyle_icon_path(fs), fs.title(), fs) for fs in fs_list]

        for icon_file, icon_name, icon_value in icons:
            if not os.path.exists(icon_file):
                FillStyleGui.generate_fillstyle_icon(icon_value)
            model.append([GdkPixbuf.Pixbuf.new_from_file(icon_file), icon_name, icon_value])

        FillStyleGui.FillStyleModel = model
        return model

    def on_fill_style_selector_changed(self, widget):
        """
        :param Gtk.ComboBox widget:
        :return:
        """
        if self.updating:
            return

        self.value = widget.get_model()[widget.get_active()][2]
        self.value_changed()

    def display_value(self, value):
        for i, row in enumerate(self.fill_style_cmb.get_model()):
            if value == row[2]:
                self.fill_style_cmb.set_active(i)
                break
        else:
            self.fill_style_cmb.set_active(0)


class MarkerStyleGui(ConstantGui):
    MarkerStyleModel = None
    ReversedMarkerDict = None

    def __init__(self):
        self.marker_style_cmb = None
        ":type: Gtk.ComboBox"
        super().__init__()

    @staticmethod
    def get_marker_icon_path(marker):
        description = MarkerStyle.markers[marker]
        marker_file = description + os.path.extsep + '.png'
        marker_folder = os.path.join(images_path(), 'markers')
        return os.path.join(marker_folder, marker_file)

    @staticmethod
    def generate_marker_icon(marker):
        marker_path = MarkerStyleGui.get_marker_icon_path(marker)
        size = 32
        try:
            make_marker_preview(marker, marker_path, facecolor='w', edgecolor='k', previewsize=size)
        except FileNotFoundError:
            os.makedirs(os.path.dirname(marker_path))
            make_marker_preview(marker, marker_path, facecolor='w', edgecolor='k', previewsize=size)

    def build_gui(self):
        """
        :rtype: Gtk.ComboBox
        :return:
        """
        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(glade_path(), 'views', 'plot_style.glade'),
                                      ('marker_style_selector',))
        self.marker_style_cmb = builder.get_object('marker_style_selector')
        ":type: Gtk.ComboBox"
        self.marker_style_cmb.set_model(MarkerStyleGui.get_marker_style_model())
        builder.connect_signals(self)
        self.marker_style_cmb.show()
        return self.marker_style_cmb

    @staticmethod
    def get_marker_style_model():
        if MarkerStyleGui.MarkerStyleModel is not None:
            return MarkerStyleGui.MarkerStyleModel

        MarkerStyleGui.ReversedMarkerDict = dict([(v, k) for k, v in MarkerStyle.markers.items()])

        model = Gtk.ListStore(GdkPixbuf.Pixbuf, str, str)

        descriptions_set = set(MarkerStyle.markers.values())
        nothing_description = MarkerStyle.markers[None]
        descriptions_set.remove(nothing_description)
        descriptions = [nothing_description]
        descriptions.extend(sorted(descriptions_set))

        for desc in descriptions:
            marker = MarkerStyleGui.ReversedMarkerDict[desc]
            icon_path = MarkerStyleGui.get_marker_icon_path(marker)
            if not os.path.exists(icon_path):
                MarkerStyleGui.generate_marker_icon(marker)
            model.append([GdkPixbuf.Pixbuf.new_from_file(icon_path), desc, '-'])

        MarkerStyleGui.MarkerStyleModel = model
        return model

    def on_marker_style_selector_changed(self, widget):
        """
        :param Gtk.ComboBox widget:
        :return:
        """
        if self.updating:
            return
        description = widget.get_model()[widget.get_active()][1]
        self.value = MarkerStyleGui.ReversedMarkerDict[description]
        self.value_changed()

    def display_value(self, value):
        description = MarkerStyle.markers[value]
        for i, row in enumerate(self.marker_style_cmb.get_model()):
            if description == row[1]:
                self.marker_style_cmb.set_active(i)
                break
        else:
            self.marker_style_cmb.set_active(0)


class LineStyleGui(ComboGui):
    line_style_dict = {'solid': '-',
                       'dashed': '--',
                       'dotted': ':',
                       'dashdot': '-.',
                       '': 'None',
                       ' ': 'None',
                       'none': 'None',
                       None: 'None'}

    def __init__(self):
        model = Gtk.ListStore(str, str)
        model.append(['Solid', '-'])
        model.append(['Dashed', '--'])
        model.append(['Dotted', ':'])
        model.append(['Dash-Dot', '-.'])
        model.append(['None', 'None'])
        super().__init__(model)

    def display_value(self, value):
        value = self.line_style_dict.get(value, value)

        super().display_value(value)
