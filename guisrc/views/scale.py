import os
from gi.repository import Gtk, GObject

from gui import ClassSelectorGui, Gui
from src.structure.database import DataBase
from src.views.scale import Scale, LimitFinder, LinearScale, ConstantLimit, MinMaxLimit, LogScale
from structure.context import Context
from structure.elementsmanager import ElementsManager
from structure.mutative import FloatGui
from utils import glade_path


class ScaleSelectorGui(ClassSelectorGui):
    scale_model = None

    def __init__(self):
        if ScaleSelectorGui.scale_model is None:
            ScaleSelectorGui.scale_model = Gtk.ListStore(str, GObject.TYPE_PYOBJECT)
            for scale, name in Scale.classes[Scale.class_path()].items():
                ScaleSelectorGui.scale_model.append([name, scale])
        super().__init__(ScaleSelectorGui.scale_model)


class BaseScaleGui(Gui):
    def __init__(self):
        super().__init__()
        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(glade_path(), 'views', 'scale.glade'),
                                      ('base_scale_box', 'base_scale_sgrp'))
        self._gui = builder.get_object('base_scale_box')
        self.min_box = builder.get_object('min_box')
        self.max_box = builder.get_object('max_box')
        self._min_gui = LimitSelectorGui(mode=LimitFinder.MIN)
        self._max_gui = LimitSelectorGui(mode=LimitFinder.MAX)
        self._min_gui.on_object_class_changed = self._on_min_class_changed
        self._max_gui.on_object_class_changed = self._on_max_class_changed
        self.min_box.pack_start(self._min_gui.gui(), True, True, 0)
        self.max_box.pack_start(self._max_gui.gui(), True, True, 0)
        self._gui.show()
        self.scale = LinearScale()
        self.set_object(self.scale)

    def gui(self):
        return self._gui

    def set_object(self, object_to_follow):
        """
        :param Scale object_to_follow:
        :return:
        """
        self.scale = object_to_follow
        self._min_gui.set_object(object_to_follow.min)
        self._max_gui.set_object(object_to_follow.max)

    def _on_min_class_changed(self, min_gui):
        if min_gui.updating:
            return
        self.scale.min = min_gui.displayed_object

    def _on_max_class_changed(self, max_gui):
        if max_gui.updating:
            return
        self.scale.max = max_gui.displayed_object


class LimitSelectorGui(ClassSelectorGui):
    limit_model = None

    def __init__(self, mode=LimitFinder.MIN):
        if LimitSelectorGui.limit_model is None:
            LimitSelectorGui.limit_model = Gtk.ListStore(str, GObject.TYPE_PYOBJECT)
            for limit, name in LimitFinder.classes[LimitFinder.class_path()].items():
                LimitSelectorGui.limit_model.append([name, limit])
        self.mode = mode
        super().__init__(LimitSelectorGui.limit_model)

    def make_object(self, cls):
        return cls(self.mode)

    def set_object(self, object_to_follow):
        super().set_object(object_to_follow)
        self.mode = object_to_follow.mode


class ConstantLimitGui(Gui):
    def __init__(self, mode=LimitFinder.MIN):
        super().__init__()
        self._limit_gui = FloatGui()
        self._limit = ConstantLimit(mode)
        self._limit_gui.on_value_changed = self._on_limit_changed

    def gui(self):
        return self._limit_gui.gui

    def set_object(self, object_to_follow):
        """
        :param src.views.scale.ConstantLimit object_to_follow:
        :return:
        """
        self._limit = object_to_follow
        self._limit_gui.set_value(object_to_follow.limit)

    def _on_limit_changed(self, float_gui):
        """
        :param FloatGui float_gui:
        :return:
        """
        self._limit.limit = float_gui.value


class MinMaxLimitGui(Gui):
    def __init__(self):
        super().__init__()
        self._gui = Gtk.Box()

    def gui(self):
        return self._gui

    def set_object(self, object_to_follow):
        pass


Gui.register_gui_for_class(LinearScale, BaseScaleGui)
Gui.register_gui_for_class(LogScale, BaseScaleGui)
Gui.register_gui_for_class(ConstantLimit, ConstantLimitGui)
Gui.register_gui_for_class(MinMaxLimit, MinMaxLimitGui)

if __name__ == "__main__":
    context = Context(ElementsManager(DataBase()))
    scl = ScaleSelectorGui()
    box = Gtk.Box()
    box.pack_start(scl.gui(), True, True, 0)
    box.show()
    window = Gtk.Window()
    scl.set_parent_window(window)
    window.connect('delete-event', lambda a, w: Gtk.main_quit())
    window.add(box)
    window.show()
    Gtk.main()
