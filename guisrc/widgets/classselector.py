from gi.repository import Gtk, GObject


class ClassSelectorWidget(Gtk.Box):
    def __init__(self, *args, class_model=None, **kwargs):
        """
        :param args: init arguments for box
        :param Gtk.ListStore[str, type] class_model:
        :param kwargs: init arguments for box
        """
        super().__init__(*args, orientation=Gtk.Orientation.VERTICAL, **kwargs)
        self._class_cmb = Gtk.ComboBox()
        ":type: Gtk.ComboBox"
        name_cell = Gtk.CellRendererText()
        self._class_cmb.pack_start(name_cell, True)
        self._class_cmb.add_attribute(name_cell, 'text', 0)
        if class_model is not None:
            self.set_model(class_model)

        self.pack_start(self._class_cmb, False, False, 0)
        self.show_all()
        self._class_cmb.connect('changed', self._on_class_changed)

    @staticmethod
    def build_model(class_dict):
        model = Gtk.ListStore(str, GObject.TYPE_PYOBJECT)
        for name, o_class in sorted(class_dict.items(), key=lambda t: t[0]):
            model.append([name, o_class])
        return model

    def set_model(self, class_model):
        """
        :param Gtk.ListStore[str, type] class_model:
        :return:
        """
        self._class_cmb.set_model(class_model)

    def get_model(self):
        return self._class_cmb.get_model()

    def set_class(self, in_object):
        for i, row in enumerate(self._class_cmb.get_model()):
            # Searches for an exact type match, ignoring inheritance
            if type(in_object) is row[1]:
                break
        else:
            self._class_cmb.set_active(-1)
            return
        self._class_cmb.set_active(i)

    def get_selected_row(self):
        class_model = self._class_cmb.get_model()
        if class_model is None:
            return None
        class_index = self._class_cmb.get_active()
        return class_model[class_index]

    def get_class(self):
        return self.get_selected_row()[1]

    def get_class_name(self):
        return self.get_class_name()[0]

    def _on_class_changed(self, widget):
        """
        :param Gtk.ComboBox widget:
        :return:
        """
        self.emit('class_changed')


GObject.type_register(ClassSelectorWidget)
GObject.signal_new('class_changed', ClassSelectorWidget, GObject.SIGNAL_RUN_LAST,
                   GObject.TYPE_NONE, ())
