from gi.repository import Gtk


class SectionRevealer(Gtk.VBox):
    def __init__(self, title, widget):
        super().__init__()
        self.show_toggle = Gtk.ToggleButton()
        self.show_toggle.set_label(title)
        self.revealer = Gtk.Revealer()
        self.revealer.add(widget)
        self.revealer.set_transition_type(Gtk.RevealerTransitionType.SLIDE_DOWN)
        self.pack_start(self.show_toggle, False, False, 0)
        self.show_toggle.show()
        self.show()
        self.show_toggle.connect('toggled', self.toggle_controlled_widget)

    def show_controlled_widget(self, show):
        self.show_toggle.set_active(show)

    def toggle_controlled_widget(self, widget, data=None):
        self.revealer.set_reveal_child(self.show_toggle.get_active())
        self.revealer.set_visible(self.show_toggle.get_active())
