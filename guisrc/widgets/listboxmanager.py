import os

from gi.repository import GObject
from gi.repository import Gtk

from guisrc.utils import glade_path


class ListBoxManager(Gtk.Box):
    def __init__(self, *args, can_reorder=True, can_add=True, can_remove=True, **kwargs):
        super().__init__(*args, **kwargs)
        self.show()
        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(glade_path(), 'widgets', 'listboxmanager.glade'),
                                      ('manager_tool_bar', ))
        self.toolbar = builder.get_object('manager_tool_bar')
        self.add(self.toolbar)
        self.move_up_button = builder.get_object('move_up')
        ":type: Gtk.ToolButton"
        self.move_down_button = builder.get_object('move_down')
        ":type: Gtk.ToolButton"
        self.add_element_button = builder.get_object('add_element')
        ":type: Gtk.ToolButton"
        self.remove_element_button = builder.get_object('remove_element')
        ":type: Gtk.ToolButton"
        builder.connect_signals(self)

        self._controlled_listbox = None
        ":type: Gtk.ListBox"
        self._row_selected_handler_id = None
        self._selected_row = None
        ":type: Gtk.ListBoxRow"
        self.can_reorder = can_reorder
        self.can_add = can_add
        self.can_remove = can_remove
        self.child_builder = None
        ":type: (ListBoxToolBar) -> Gtk.Widget"
        self.set_can_add(can_add)
        self.set_can_reorder(can_reorder)
        self.set_can_remove(can_remove)

    @property
    def controlled_listbox(self):
        """
        :rtype: Gtk.ListBox
        :return:
        """
        return self._controlled_listbox

    def set_can_add(self, can_add):
        self.can_add = can_add
        self.add_element_button.set_visible(self.can_add)
        self.update_add_sensitivity()

    def set_can_remove(self, can_remove):
        self.can_remove = can_remove
        self.remove_element_button.set_visible(self.can_remove)
        self.remove_element_button.set_sensitive(self.can_remove)

    def set_can_reorder(self, can_reorder):
        self.can_reorder = can_reorder
        self.move_down_button.set_visible(self.can_reorder)
        self.move_up_button.set_visible(self.can_reorder)
        self.update_reorder_sensitivity()

    def set_child_builder(self, builder):
        """
        :param (ListBoxManager) -> Gtk.Widget builder:
        """
        self.child_builder = builder
        self.update_add_sensitivity()

    def update_add_sensitivity(self):
        sensitive = self._controlled_listbox is not None and self.child_builder is not None and self.can_add
        self.add_element_button.set_sensitive(sensitive)

    def update_reorder_sensitivity(self):
        sensitive = self.can_add and self._selected_row is not None
        sensitive_up = sensitive and self._selected_row.get_index() > 0
        sensitive_down = sensitive and self._selected_row.get_index() < len(self._controlled_listbox) - 1
        self.move_down_button.set_sensitive(sensitive_down)
        self.move_up_button.set_sensitive(sensitive_up)

    def update_remove_sensitivity(self):
        sensitive = self.can_remove and self._selected_row is not None
        self.remove_element_button.set_sensitive(sensitive)

    # noinspection PyUnusedLocal
    def on_move_up_clicked(self, widget, data=None):
        if self.emit('move_up'):
            self._controlled_listbox.select_row(self._controlled_listbox.get_row_at_index(self._selected_row.get_index() - 1))

    # noinspection PyUnusedLocal
    def on_move_down_clicked(self, widget, data=None):
        if self.emit('move_down'):
            self._controlled_listbox.select_row(self._controlled_listbox.get_row_at_index(self._selected_row.get_index() + 1))

    def move_row(self, row, new_index):
        self._controlled_listbox.remove(row)
        self._controlled_listbox.insert(row, new_index)

    # noinspection PyUnusedLocal
    def on_add_element_clicked(self, widget, data=None):
        if self.child_builder is None or self._controlled_listbox is None:
            return
        child = self.child_builder(self)
        child.show()
        self.add_child(child)

    def add_child(self, child):
        self._controlled_listbox.add(child)
        self.emit('child_added', child)

    def insert_child(self, child, index):
        self._controlled_listbox.insert(child, index)
        self.emit('child_added', child)

    # noinspection PyUnusedLocal
    def on_remove_element_clicked(self, widget, data=None):
        index = self._selected_row.get_index()
        self.remove_child(index)

    def remove_child(self, index):
        self.emit('child_removed', index)
        self._controlled_listbox.remove(self._controlled_listbox.get_row_at_index(index))
        index = min(index, len(self._controlled_listbox)-1)
        self._controlled_listbox.select_row(self._controlled_listbox.get_row_at_index(index))

    def set_controlled_listbox(self, listbox):
        """
        :param Gtk.ListBox listbox:
        """
        if self._controlled_listbox is not None and self._row_selected_handler_id is not None:
            self._controlled_listbox.disconnect(self._row_selected_handler_id)
        self._controlled_listbox = listbox
        self.update_add_sensitivity()
        self._controlled_listbox.connect('row-selected', self.on_row_selected)

    # noinspection PyUnusedLocal
    def on_row_selected(self, listbox, row, data=None):
        """
        :param Gtk.ListBox listbox:
        :param Gtk.ListBoxRow row:
        :param data:
        :return:
        """
        self._selected_row = row
        self.emit('child_selected')
        self.update_remove_sensitivity()
        self.update_reorder_sensitivity()

    def get_selected(self):
        """
        :rtype: Gtk.ListBoxRow|None
        :return:
        """
        return self._selected_row

GObject.type_register(ListBoxManager)
GObject.signal_new("move_up", ListBoxManager, GObject.SIGNAL_RUN_LAST,
                   GObject.TYPE_BOOLEAN, ())
GObject.signal_new("move_down", ListBoxManager, GObject.SIGNAL_RUN_LAST,
                   GObject.TYPE_BOOLEAN, ())
GObject.signal_new("child_removed", ListBoxManager, GObject.SIGNAL_RUN_FIRST,
                   GObject.TYPE_NONE, (GObject.TYPE_INT, ))
GObject.signal_new("child_added", ListBoxManager, GObject.SIGNAL_RUN_FIRST,
                   GObject.TYPE_NONE, (GObject.TYPE_OBJECT, ))
GObject.signal_new("child_selected", ListBoxManager, GObject.SIGNAL_RUN_FIRST,
                   GObject.TYPE_NONE, ())


class ManagedListBox(Gtk.Box):
    def __init__(self, *args, can_reorder=True, can_add=True, can_remove=True, **kwargs):
        super().__init__(*args, **kwargs)
        scrolled = Gtk.ScrolledWindow()
        scrolled.show()
        self.listbox = Gtk.ListBox()
        scrolled.add(self.listbox)
        self.pack_start(scrolled, True, True, 0)
        self.manager = ListBoxManager(can_reorder=can_reorder, can_add=can_add, can_remove=can_remove)
        self.manager.toolbar.set_orientation(Gtk.Orientation.VERTICAL)
        self.pack_end(self.manager, False, False, 0)
        self.manager.set_controlled_listbox(self.listbox)
        self.listbox.show()
        self.show()


if __name__ == "__main__":
    window = Gtk.Window()
    window.set_size_request(200, 200)
    window.connect('delete-event', lambda a, w: Gtk.main_quit())
    managed_listbox = ManagedListBox()
    manager = managed_listbox.manager
    manager.set_child_builder(lambda s: Gtk.Label('child'))
    manager.connect('child_added', lambda m, w: w.set_text('child {}'.format(len(m._controlled_listbox))))
    window.add(managed_listbox)
    window.show()
    Gtk.main()
