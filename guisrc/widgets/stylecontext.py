from gi.repository import Gtk, Gdk


USER_INPUT_STYLE = "user_input"


def load_style(path):
    style_provider = Gtk.CssProvider()
    style_provider.load_from_path(path)
    Gtk.StyleContext.add_provider_for_screen(Gdk.Screen.get_default(),
                                             style_provider,
                                             Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)
