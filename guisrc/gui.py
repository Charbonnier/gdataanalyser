import os
import re
from abc import abstractmethod
from html import escape

from gi.repository import Gtk, GObject

from guisrc.utils import glade_path
# Cannot use annotation for Context due to a cyclic reference.
# :TODO Context is dependent of Gui, which is a design error
#from structure.context import Context
from widgets.classselector import ClassSelectorWidget


class Gui:
    guis = {}
    """:type: dict[(str, str), ()->Gui]"""
    main_window = None

    def __init__(self):
        super().__init__()
        self.updating = False
        self.context = None
        ":type: guisrc.structure.context.Context"
        self.transient = None
        """:type: Gtk.Window"""
        self._size_group = None
        ":type: Gtk.SizeGroup"
        self.parent_window = Gui.main_window

    @classmethod
    def get_icon(cls):
        return None

    def set_parent_window(self, window=None):
        if window is None:
            window = Gui.main_window
        elif Gui.main_window is None:
            Gui.main_window = window
        self.parent_window = window

    @abstractmethod
    def gui(self):
        """
        :return: The main gtk widget of the gui
        """
        raise NotImplementedError()

    @abstractmethod
    def set_object(self, object_to_follow):
        raise NotImplementedError()

    def set_context(self, context):
        """

        :param structure.context.Context context:
        :return:
        """
        self.context = context

    def set_size_group(self, size_group):
        """
        :param Gtk.SizeGroup|None size_group:
        :return:
        """
        if self._size_group is not None:
            for widget in self.sized_widgets():
                self._size_group.remove_widget(widget)
        self._size_group = size_group
        if self._size_group is not None:
            for widget in self.sized_widgets():
                self._size_group.add_widget(widget)

    def sized_widgets(self):
        return []

    def set_transient(self, transient):
        """
        :param Gtk.Window transient:
        :return:
        """
        self.transient = transient

    @staticmethod
    def get_gui_class_for_object(in_object):
        cls = in_object.__class__
        try:
            gui_class = Gui.guis[(cls.__module__, cls.__name__)]
        except KeyError:
            gui_class = NoGui
        return gui_class

    @staticmethod
    def build_gui_for_object(in_object):
        """
        :param object in_object:
        :return:
        """
        return Gui.get_gui_class_for_object(in_object)()

    @staticmethod
    def register_gui_for_class(cls, gui):
        """
        :param ()->Any cls: The class for which the Gui must be used
        :param ()->Gui gui: The gui class to use
        """
        Gui.guis[(cls.__module__, cls.__name__)] = gui

    def destroy(self):
        """Call it before removing the Gui."""
        self.on_change = []
        self.set_size_group(None)


class NoGui(Gui):
    def __init__(self):
        super().__init__()
        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(glade_path(), 'no_gui.glade'), ('no_gui_box',))
        self._gui = builder.get_object('no_gui_box')
        self.label = builder.get_object('no_gui_lbl')
        """:type: Gtk.Label"""
        self.base_message = self.label.get_text()

    def gui(self):
        return self._gui

    def set_object(self, object_to_follow):
        cls = object_to_follow.__class__
        self.label.set_text(self.base_message.format(cls.__module__, cls.__name__))


class SimpleGui(Gui):
    def __init__(self, gui):
        super().__init__()
        self._gui = gui

    def gui(self):
        return self._gui

    def set_object(self, in_object):
        pass


class RegexEntry:
    def __init__(self, regex, undo, warning_box, warning):
        """
        :param Gtk.Entry regex: Entry where user writes the regular expression
        :param Gtk.Button undo: Undo button that user can press to reset the last valid regex.
        :param Gtk.Widget warning_box: Container the error message to display to the user for invalid regex.
        :param Gtk.Label warning: Displays error message to the user
        """
        self.regex_entry = regex
        self.undo_btn = undo
        self.warning_box = warning_box
        self.warning_label = warning
        self.changed_cb = None

        self.updating = False
        self.regex_entry.connect('changed', self.on_regex_changed)
        self.undo_btn.connect('clicked', self.on_restore_regex)
        self.last_valid_regex = ''

    # noinspection PyUnusedLocal
    def on_regex_changed(self, widget, data=None):
        if not self.updating:
            regex = self.regex_entry.get_text()
            try:
                re.compile(regex)
            except re.error as ec:
                self.warning_box.show()
                before_error = regex[:ec.pos]
                error = regex[ec.pos]
                try:
                    after_error = regex[ec.pos + 1:]
                except IndexError:
                    after_error = ''
                self.warning_label.set_markup(f'{escape(ec.msg)}\n{escape(before_error)}<span underline="error"'
                                              f'underline-color="#FF0000">{escape(error)}</span>{escape(after_error)}')
            else:
                self.set_regex(regex)
                self.changed()

    def changed(self):
        self.last_valid_regex = self.regex_entry.get_text()
        if self.changed_cb is not None:
            self.changed_cb(self)

    # noinspection PyUnusedLocal
    def on_restore_regex(self, widget, data=None):
        self.updating = True
        self.regex_entry.set_text(self.last_valid_regex)
        self.updating = False
        self.warning_box.hide()

    def set_regex(self, regex):
        self.updating = True
        self.warning_box.hide()
        self.regex_entry.set_text(regex)
        self.last_valid_regex = regex
        self.updating = False

    def get_regex(self):
        return self.regex_entry.get_text()


class ClassSelectorGui(Gui):
    def __init__(self, class_model, expand=False, fill=False, build_args=None):
        super().__init__()
        self.expand = expand
        self.fill = fill
        if build_args is None:
            build_args = []
        self.build_args = build_args
        self.size_group = None
        self._gui = ClassSelectorWidget(class_model=class_model)
        self.displayed_object = None
        self.object_gui = None
        self.on_object_class_changed = None
        ":type: (ClassSelectorGui)->None"
        self._gui.connect('class_changed', self.on_class_cmb_changed)

    def make_object(self, cls):
        return cls()

    def on_class_cmb_changed(self, widget, data=None):
        """
        :param Gtk.ComboBox widget:
        :param data:
        :return:
        """
        if self.object_gui is not None:
            self._gui.remove(self.object_gui.gui())
        self.displayed_object = self.make_object(self._gui.get_class())
        self.object_gui = Gui.get_gui_class_for_object(self.displayed_object)(*self.build_args)
        self._gui.pack_start(self.object_gui.gui(), self.expand, self.fill, 0)
        self.object_gui.set_parent_window(self.parent_window)
        self.object_gui.set_context(self.context)
        self.object_gui.set_size_group(self.size_group)
        self.set_object_gui()
        if self.on_object_class_changed is not None:
            self.on_object_class_changed(self)

    def set_size_group(self, size_group):
        self.size_group = size_group
        if self.object_gui is not None:
            self.object_gui.set_size_group(size_group)

    def set_object_gui(self):
        self.object_gui.set_object(self.displayed_object)

    def gui(self):
        return self._gui

    def set_object(self, object_to_follow):
        self.updating = True
        try:
            self._gui.set_class(object_to_follow)
            self.displayed_object = object_to_follow
            if self.displayed_object is not None:
                self.set_object_gui()
        finally:
            self.updating = False


class ClassSelectorWithSourceGui(ClassSelectorGui):
    def __init__(self, class_model, expand=False, fill=False, build_args=None):
        super().__init__(class_model, expand, fill, build_args)
        self.source = None

    def set_object(self, object_to_follow):
        self.source, object_select = object_to_follow
        super().set_object(object_select)

    def set_object_gui(self):
        self.object_gui.set_object((self.source, self.displayed_object))
