import os
from typing import Union, Tuple, Any, Optional

from gi.repository import Gtk

from guisrc.gui import Gui, ClassSelectorWithSourceGui
from sources.source import GuiWithPreview, TVSourceItemInterface
from sources.textparser.textcaster import GTextCaster
from src.sources.textfile import TextFile
from src.sources.textparser.basetextparser import TextStructureParser
from src.sources.textparser.tableparser import Column, ColumnSubArray, ColumnIndex, ColumnParser
from structure.mutative import IntegerSetterGui
from utils import glade_path
from src.sources.textparser.tableparser import TableParser, NamedColumn, IndexColumn
from widgets.classselector import ClassSelectorWidget


class TableParserGui(GuiWithPreview, TVSourceItemInterface):
    COLUMN = 0
    COLUMN_PARSER = 1

    def __init__(self):
        super().__init__()
        self.can_create_child = True
        self.can_remove_child = True

        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(glade_path(), 'sources', 'tableparser.glade'),
                                      ('table_parser', 'columns_model', 'table_parser_structure'))
        self._gui = builder.get_object('table_parser')
        self.columns_model: Gtk.TreeStore = builder.get_object('columns_model')
        self.columns_tv: Gtk.TreeView = builder.get_object('columns_tv')
        self.data_in_row: Gtk.ComboBox = builder.get_object('data_in_row')
        self.delimiter_entry: Gtk.Entry = builder.get_object('delimiter_entry')
        self.has_header: Gtk.Switch = builder.get_object('has_header_switch')
        self.add_column_btn: Gtk.ToolButton = builder.get_object('add_column')
        self.add_data_btn: Gtk.ToolButton = builder.get_object('add_data')
        self.remove_item_btn: Gtk.ToolButton = builder.get_object('remove_item')

        self.table_parser: Union[TableParser, None] = None
        self.textfile: Union[TextFile, None] = None

        self.parser_container: Gtk.Box = builder.get_object('parser_container')

        self._sized_widgets = [builder.get_object('label2'),
                               builder.get_object('label3'),
                               builder.get_object('label4')]

        self._item_gui = None

        builder.connect_signals(self)

    def gui(self):
        return self._gui

    def sized_widgets(self):
        return self._sized_widgets

    def set_object(self, textfile_parser: Tuple[TextFile, TableParser]):
        self.updating = True
        self.textfile, self.table_parser = textfile_parser
        delimiter = self.table_parser.delimiter.encode('unicode_escape').decode('utf8')
        self.delimiter_entry.set_text(delimiter)
        self.has_header.set_active(self.table_parser.has_header)
        for i, row in enumerate(self.data_in_row.get_model()):
            if row[1] == self.table_parser.data_in_row:
                self.data_in_row.set_active(i)
                break
        else:
            self.data_in_row.set_active(-1)

        for i in range(len(self.table_parser.columns)):
            self._model_add_column(i)
        self.updating = False

    def add_child(self, index, treemodel_iter):
        new_index = self.table_parser.add_column(NamedColumn())
        column_gui = ColumnGui()
        value = (self.textfile, (self.table_parser, new_index, self.table_parser.columns[index]))
        column_gui.set_iter(treemodel_iter)
        self.setup_child_item(column_gui, value)
        return column_gui

    def rem_child(self, index, treemodel_iter):
        self.table_parser.remove_column(index)

    def _model_add_column(self, index):
        column_gui = ColumnGui()
        column = self.table_parser.columns[index]
        value = (self.textfile, (self.table_parser, index, column))
        c_iter = self.treemodel.append(self.treemodel_iter, [column.user_name, column_gui, column_gui.editable])
        column_gui.set_iter(c_iter)
        self.setup_child_item(column_gui, value)

    def setup_child_item(self, tree_item: 'ColumnGui', value: Any):
        super().setup_child_item(tree_item, value)
        tree_item.set_parent_window(self.parent_window)
        tree_item.set_context(self.context)
        tree_item.set_object(value)

    # noinspection PyUnusedLocal
    def on_delimiter_entry_changed(self, widget, data=None):
        if not self.updating:
            if self.table_parser is not None:
                delimiter = self.delimiter_entry.get_text().encode('utf8').decode('unicode_escape')
                self.table_parser.delimiter = delimiter

    # noinspection PyUnusedLocal
    def on_structure_combo_changed(self, widget, data=None):
        if not self.updating:
            if self.table_parser is not None:
                index = self.data_in_row.get_active()
                if index >= 0:
                    self.table_parser.data_in_row = self.data_in_row.get_model()[index][1]

    # noinspection PyUnusedLocal
    def on_has_header_switch_state_set(self, widget, data=None):
        if not self.updating:
            if self.table_parser is not None:
                self.table_parser.has_header = self.has_header.get_active()


class ColumnGui(GuiWithPreview, TVSourceItemInterface):
    column_model = None

    def __init__(self):
        if ColumnGui.column_model is None:
            ColumnGui.column_model = ClassSelectorWidget.build_model(
                Column.classes[Column.class_path()])

        super().__init__()
        self.can_create_child = True
        self.can_remove_child = True
        self.editable = True
        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(glade_path(), 'sources', 'tableparser.glade'),
                                      ('column_container', 'column_sg'))
        self._gui = builder.get_object('column_container')
        self._content_size_group = builder.get_object('column_sg')
        self.column_class_selector = ClassSelectorWithSourceGui(ColumnGui.column_model, True, True)
        self.column_class_selector.set_size_group(self._content_size_group)
        self.column_class_selector.on_object_class_changed = self.on_column_class_changed
        self._gui.pack_start(self.column_class_selector.gui(), True, True, 0)
        self._column: Optional[Column] = None
        self.textfile: Optional[TextFile] = None
        self._table_parser: Optional[TableParser] = None
        self._column_index: Optional[int] = None

        builder.connect_signals(self)

    def gui(self):
        return self._gui

    def set_object(self, column: Tuple[TextFile, Tuple[TableParser, int, Column]]):
        self.updating = True
        try:
            self.textfile, (self._table_parser, self._column_index, self._column) = column
            self.column_class_selector.set_object((self.textfile, self._column))
            for name, parser in self._table_parser.columns_parsers[self._column_index].items():
                parser_gui = ColumnParserContainerGui()
                value = self.textfile, (self._table_parser, name, parser)
                p_iter = self.treemodel.append(self.treemodel_iter, [name, parser_gui, parser_gui.editable])
                parser_gui.set_iter(p_iter)
                self.setup_child_item(parser_gui, value)
        finally:
            self.updating = False

    def add_child(self, index, treemodel_iter):
        base_name = 'data_{}'
        i = 0
        column_index = self.treemodel.get_path(self.treemodel_iter)[-1]
        table_parser: TableParser = self._table_parser
        while True:
            name = base_name.format(i)
            if not table_parser.check_name_exists(name):
                break
            i += 1
        parser = ColumnParser()
        c_parser = ColumnParserContainerGui()
        c_parser.set_iter(treemodel_iter)
        table_parser.add_parser(column_index, name, parser)
        value = self.textfile, (self._table_parser, name, parser)
        self.setup_child_item(c_parser, value)
        return c_parser

    def rem_child(self, index, treemodel_iter):
        name = self.treemodel[treemodel_iter][1].get_name()
        column_index = self.treemodel.get_path(self.treemodel_iter)[-1]
        self._table_parser.remove_data(column_index, name)

    def setup_child_item(self, tree_item: 'ColumnParserContainerGui', value):
        super().setup_child_item(tree_item, value)
        tree_item.set_parent_window(self.parent_window)
        tree_item.set_context(self.context)
        tree_item.set_object(value)

    def set_context(self, context):
        super().set_context(context)
        self.column_class_selector.set_context(context)

    def set_parent_window(self, window=None):
        super().set_parent_window(window)
        self.column_class_selector.set_parent_window(window)

    # noinspection PyUnusedLocal
    def on_column_class_changed(self, widget, data=None):
        if not self.updating:
            self._column = self.column_class_selector.displayed_object
            self._table_parser.remove_column(self._column_index)
            self._table_parser.insert_column(self._column_index, self._column)

    def on_title_cell_edited(self, path, new_title):
        self._column.user_name = new_title
        self.treemodel[self.treemodel_iter][0] = new_title


class NamedColumnGui(GuiWithPreview):
    def __init__(self):
        super().__init__()
        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(glade_path(), 'sources', 'tableparser.glade'),
                                      ('named_column', ))
        self._gui = builder.get_object('named_column')
        self.name_entry: Gtk.Entry = builder.get_object('name_entry')
        self.column: Optional[NamedColumn] = None
        self.textfile: Optional[TextFile] = None

        self._sized_widgets = [builder.get_object('label5')]
        builder.connect_signals(self)

    def gui(self):
        return self._gui

    def sized_widgets(self):
        return self._sized_widgets

    def set_object(self, textfile_column: Tuple[TextFile, NamedColumn]):
        self.updating = True
        self.textfile, self.column = textfile_column
        self.name_entry.set_text(self.column.header_name)
        self.updating = False

    # noinspection PyUnusedLocal
    def on_name_entry_changed(self, widget, data=None):
        if not self.updating:
            if self.column is not None:
                self.column.header_name = self.name_entry.get_text()


class IndexedColumnGui(GuiWithPreview):
    def __init__(self):
        super().__init__()
        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(glade_path(), 'sources', 'tableparser.glade'),
                                      ('indexed_column', ))
        self._gui = builder.get_object('indexed_column')
        self.column_index_container: Gtk.Box = builder.get_object('column_index_container')
        self.column_index = IntegerSetterGui()
        self.column_index_container.add(self.column_index.gui())
        self.column: Optional[IndexColumn] = None
        self.textfile: Optional[TextFile] = None

        self._sized_widgets = [builder.get_object('label6'),
                               builder.get_object('label7'),
                               builder.get_object('label8')]

        self.multiple_columns: Gtk.Switch = builder.get_object('multiple_columns')
        self.index_step: Gtk.SpinButton = builder.get_object('index_step')

        builder.connect_signals(self)

    def gui(self):
        return self._gui

    def sized_widgets(self):
        return self._sized_widgets

    def set_object(self, textfile_column: Tuple[TextFile, IndexColumn]):
        self.updating = True
        self.textfile, self.column = textfile_column
        self.column_index.set_object((self.textfile, self.column.mutable_members['index']))
        self.index_step.set_value(self.column.step)
        self.multiple_columns.set_active(self.column.enable_stepping)
        self.updating = False

    def set_context(self, context):
        super().set_context(context)
        self.column_index.set_context(context)

    def set_parent_window(self, window=None):
        super().set_parent_window(window)
        self.column_index.set_parent_window(window)

    # noinspection PyUnusedLocal
    def on_multiple_columns_state_set(self, widget, data=None):
        if not self.updating:
            if self.column is not None:
                self.column.enable_stepping = self.multiple_columns.get_active()
        self.index_step.set_sensitive(self.multiple_columns.get_active())

    # noinspection PyUnusedLocal
    def on_index_step_value_changed(self, widget, data=None):
        if not self.updating:
            if self.column is not None:
                self.column.step = self.index_step.get_value_as_int()


class ColumnParserContainerGui(GuiWithPreview, TVSourceItemInterface):
    column_parser_model = None

    def __init__(self):
        if ColumnParserContainerGui.column_parser_model is None:
            ColumnParserContainerGui.column_parser_model = ClassSelectorWidget.build_model(
                ColumnParser.classes[ColumnParser.class_path()])

        super().__init__()
        self._obs_field_renamed = None
        self.editable = True
        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(glade_path(), 'sources', 'tableparser.glade'),
                                      ('column_parser_container', 'column_parser_sg'))
        self._gui = builder.get_object('column_parser_container')
        self._content_size_group = builder.get_object('column_parser_sg')
        self.column_parser_class_selector = ClassSelectorWithSourceGui(ColumnParserContainerGui.column_parser_model,
                                                                       True, True)
        self.column_parser_class_selector.set_size_group(self._content_size_group)
        self.column_parser_class_selector.on_object_class_changed = self.on_column_parser_class_changed
        self._gui.pack_start(self.column_parser_class_selector.gui(), True, True, 0)
        self._column_parser: Optional[ColumnParser] = None
        self.textfile: Optional[TextFile] = None
        self._table_parser: Optional[TableParser] = None
        self._parser_name: Optional[str] = None

        builder.connect_signals(self)

    def gui(self):
        return self._gui

    def get_name(self):
        return self._parser_name

    def on_title_cell_edited(self, path, new_title):
        old_name = self.treemodel[self.treemodel_iter][0]
        self.textfile.manager.rename_field(old_name, new_title)

    def on_field_renamed(self, table_parser: TextStructureParser, old_name, new_name):
        if self.treemodel[self.treemodel_iter][0] == old_name:
            self.treemodel[self.treemodel_iter][0] = new_name

    def set_object(self, column_parser: Tuple[TextFile, Tuple[TableParser, str, ColumnParser]]):
        self.updating = True
        if self._table_parser is not None:
            self._table_parser.remove_event_observer(self._obs_field_renamed)
        self.textfile, (self._table_parser, self._parser_name, self._column_parser) = column_parser
        self._table_parser.add_event_observer(TextStructureParser.EVENT_FIELD_RENAMED, self.on_field_renamed)
        self.column_parser_class_selector.set_object((self.textfile, self._column_parser))
        self.updating = False

    def set_context(self, context):
        super().set_context(context)
        self.column_parser_class_selector.set_context(context)

    def set_parent_window(self, window=None):
        super().set_parent_window(window)
        self.column_parser_class_selector.set_parent_window(window)

    # noinspection PyUnusedLocal
    def on_column_parser_class_changed(self, widget, data=None):
        if not self.updating:
            self._column_parser = self.column_parser_class_selector.displayed_object
            self._table_parser.change_parser(self._parser_name, self._column_parser)


class ColumnParserGui(GuiWithPreview):
    def __init__(self):
        super().__init__()
        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(glade_path(), 'sources', 'tableparser.glade'),
                                      ('column_parser', ))
        text_parser_container: Gtk.Box = builder.get_object('textparser_container')
        self._gui = builder.get_object('column_parser')
        self.location_frame = builder.get_object('settings_frame')
        self.location_container = builder.get_object('location_container')
        self.text_parser_gui = GTextCaster()
        self.text_parser_gui.connect_on_change(self.on_text_caster_change)
        text_parser_container.pack_start(self.text_parser_gui.gui(), True, True, 0)
        self._column_parser: Optional[ColumnParser] = None
        self.textfile: Optional[TextFile] = None
        builder.connect_signals(self)

    def gui(self):
        return self._gui

    def on_text_caster_change(self, widget, data):
        if self.updating:
            return
        if self._column_parser is None:
            return
        if data == 'type':
            self._column_parser.text_caster = self.text_parser_gui.text_caster

    def set_size_group(self, size_group):
        super().set_size_group(size_group)
        self.text_parser_gui.set_size_group(size_group)

    def set_object(self, textfile_parser: Tuple[TextFile, ColumnParser]):
        self.updating = True
        self.textfile, self._column_parser = textfile_parser
        self.text_parser_gui.set_object((self.textfile, self._column_parser.text_caster))
        self.updating = False

    def set_context(self, context):
        super().set_context(context)
        self.text_parser_gui.set_context(context)

    def set_parent_window(self, window=None):
        super().set_parent_window(window)
        self.text_parser_gui.set_parent_window(window)


class ColumnIndexGui(ColumnParserGui):
    def __init__(self):
        self._column_parser: Optional[ColumnIndex] = None

        super().__init__()
        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(glade_path(), 'sources', 'tableparser.glade'),
                                      ('indexed_data', ))
        self.location_container.add(builder.get_object('indexed_data'))
        self.location_frame.set_visible(True)
        self.data_index_container: Gtk.Box = builder.get_object('data_index_container')
        self.data_index = IntegerSetterGui()
        self.data_index_container.pack_start(self.data_index.gui(), True, True, 0)
        self.data_stepping: Gtk.Switch = builder.get_object('data_stepping')
        self.data_step: Gtk.SpinButton = builder.get_object('data_step')
        self.data_step.set_adjustment(Gtk.Adjustment(1, 1, 4e9, 1))

        self._sized_widgets = [builder.get_object('label11'),
                               builder.get_object('label12'),
                               builder.get_object('label13')]

        builder.connect_signals(self)

    def gui(self):
        return self._gui

    def sized_widgets(self):
        widgets = super().sized_widgets()
        widgets.extend(self._sized_widgets)
        return widgets

    # noinspection PyUnusedLocal
    def on_data_step_value_changed(self, widget, data=None):
        if not self.updating:
            if self._column_parser is not None:
                self._column_parser.step = self.data_step.get_value_as_int()

    # noinspection PyUnusedLocal
    def on_data_stepping_state_set(self, widget, data=None):
        if not self.updating:
            if self._column_parser is not None:
                self._column_parser.enable_stepping = self.data_stepping.get_active()
                self.data_step.set_sensitive(self._column_parser.enable_stepping)

    def set_object(self, textfile_parser: Tuple[TextFile, ColumnIndex]):
        super().set_object(textfile_parser)
        self.updating = True
        self.data_index.set_object((self.textfile, self._column_parser.mutable_members['index']))
        self.data_stepping.set_active(self._column_parser.enable_stepping)
        self.data_step.set_value(self._column_parser.step)
        self.data_step.set_sensitive(self._column_parser.enable_stepping)
        self.updating = False

    def set_context(self, context):
        super().set_context(context)
        self.data_index.set_context(context)

    def set_parent_window(self, window=None):
        super().set_parent_window(window)
        self.data_index.set_parent_window(window)


class ColumnSubArrayGui(ColumnParserGui):
    def __init__(self):
        self._column_parser: Optional[ColumnSubArray] = None
        super().__init__()
        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(glade_path(), 'sources', 'tableparser.glade'),
                                      ('indexed_array_data', ))
        self.location_container.add(builder.get_object('indexed_array_data'))
        self.location_frame.set_visible(True)
        self.start_index: Gtk.SpinButton = builder.get_object('start_index')
        self.start_index.set_adjustment(Gtk.Adjustment(0, 0, 4e9, 1))

        self.length: Gtk.SpinButton = builder.get_object('array_length')
        self.length.set_adjustment(Gtk.Adjustment(1, 1, 4e9, 1))

        self.enable_stepping : Gtk.Switch= builder.get_object('data_stepping1')

        self.step: Gtk.SpinButton = builder.get_object('array_step')
        self.step.set_adjustment(Gtk.Adjustment(1, 1, 4e9, 1))

        self._sized_widgets = [builder.get_object('label9'),
                               builder.get_object('label18'),
                               builder.get_object('label10'),
                               builder.get_object('label17')]

        builder.connect_signals(self)

    def gui(self):
        return self._gui

    def sized_widgets(self):
        widgets = super().sized_widgets()
        widgets.extend(self._sized_widgets)
        return widgets

    def set_object(self, textfile_parser: Tuple[TextFile, ColumnSubArray]):
        super().set_object(textfile_parser)
        self.updating = True
        self.start_index.set_value(self._column_parser.start_index)
        self.length.set_value(self._column_parser.length)
        self.enable_stepping.set_active(self._column_parser.enable_stepping)
        self.step.set_value(self._column_parser.step)
        self.updating = False

    # noinspection PyUnusedLocal
    def on_array_step_value_changed(self, widget, data=None):
        if not self.updating:
            if self._column_parser is not None:
                self._column_parser.step = self.step.get_value_as_int()

    # noinspection PyUnusedLocal
    def on_array_length_value_changed(self, widget, data=None):
        if not self.updating:
            if self._column_parser is not None:
                self._column_parser.length = self.length.get_value_as_int()

    # noinspection PyUnusedLocal
    def on_start_index_value_changed(self, widget, data=None):
        if not self.updating:
            if self._column_parser is not None:
                self._column_parser.start_index = self.start_index.get_value_as_int()

    # noinspection PyUnusedLocal
    def on_data_stepping1_state_set(self, widget, data=None):
        if not self.updating:
            if self._column_parser is not None:
                self._column_parser.enable_stepping = self.enable_stepping.get_active()


Gui.register_gui_for_class(TableParser, TableParserGui)
Gui.register_gui_for_class(NamedColumn, NamedColumnGui)
Gui.register_gui_for_class(IndexColumn, IndexedColumnGui)
Gui.register_gui_for_class(ColumnParser, ColumnParserGui)
Gui.register_gui_for_class(ColumnIndex, ColumnIndexGui)
Gui.register_gui_for_class(ColumnSubArray, ColumnSubArrayGui)
