from sources.source import PreviewContext


class TextFilePreviewContext(PreviewContext):
    def __init__(self):
        super().__init__()
        self._folder_path = ""
        self._file_path = ""

    @property
    def folder_path(self):
        return self._folder_path

    @folder_path.setter
    def folder_path(self, folder_path):
        self._folder_path = folder_path
        self.signal_changed()

    @property
    def file_path(self):
        return self._file_path

    @file_path.setter
    def file_path(self, file_path):
        self._file_path = file_path
        self.signal_changed()