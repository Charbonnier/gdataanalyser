import os
from typing import Tuple

from gi.repository import Gtk

from src.sources.textparser.textcaster import TextToInt, TextToFloat, TextToBool, TextCaster
from guisrc.gui import Gui
from src.structure.source import Source
from structure.mutative import StringSetterGui
from utils import glade_path
from widgets.classselector import ClassSelectorWidget


class GTextCaster(Gui):
    caster_model = None

    def __init__(self):
        if GTextCaster.caster_model is None:
            GTextCaster.caster_model = ClassSelectorWidget.build_model(
                TextCaster.classes[TextCaster.class_path()])

        super().__init__()
        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(glade_path(), 'sources', 'textcaster.glade'),
                                      ('text_caster_container', ))
        self._gui = builder.get_object('text_caster_container')
        ":type: Gtk.Box"
        self.export_type = ClassSelectorWidget(class_model=GTextCaster.caster_model)
        self.export_type.connect('class_changed', self.on_export_type_changed)
        export_selector_box = builder.get_object('export_selector_box')
        ":type: Gtk.Box"
        export_selector_box.pack_start(self.export_type, True, True, 0)
        self.stop_on_error = builder.get_object('stop_on_error')
        ":type: Gtk.Switch"
        self.default_value = StringSetterGui()
        self.default_box = builder.get_object('default_box')
        ":type: Gtk.Box"
        self.default_box.pack_start(self.default_value.gui(), True, True, 0)
        self._sized_widgets = [builder.get_object('label11'),
                               builder.get_object('label1'),
                               builder.get_object('label9')]
        self.text_caster = None
        ":type: TextCaster"
        self.source = None
        ":type: src.structure.source.Source"
        builder.connect_signals(self)

    def gui(self):
        return self._gui

    def sized_widgets(self):
        return self._sized_widgets

    # noinspection PyUnusedLocal
    def on_export_type_changed(self, widget, data=None):
        """
        :param Gtk.ComboBox widget:
        :param data:
        :return:
        """
        if not self.updating:
            self.text_caster = self.export_type.get_class()(self.text_caster)
            self.default_value.set_object((self.source, self.text_caster.mutable_members['value_on_exception']))

    def set_object(self, source_caster: Tuple[Source, TextCaster]):
        self.updating = True
        self.source, self.text_caster = source_caster
        self.export_type.set_class(self.text_caster)
        self.default_value.set_object((self.source, self.text_caster.mutable_members['value_on_exception']))
        self.stop_on_error.set_state(self.text_caster.raise_on_exception)
        self.updating = False

    def set_context(self, context):
        super().set_context(context)
        self.default_value.set_context(context)

    def set_parent_window(self, window=None):
        super().set_parent_window(window)
        self.default_value.set_parent_window(window)

    # noinspection PyUnusedLocal
    def on_stop_on_error_state_set(self, widget, state, data=None):
        self.default_box.set_sensitive(not state)
        if not self.updating:
            if self.text_caster is not None:
                self.text_caster.raise_on_exception = state


class GTextToBool(GTextCaster):
    pass

Gui.register_gui_for_class(TextCaster, GTextCaster)
Gui.register_gui_for_class(TextToInt, GTextCaster)
Gui.register_gui_for_class(TextToFloat, GTextCaster)
Gui.register_gui_for_class(TextToBool, GTextToBool)
