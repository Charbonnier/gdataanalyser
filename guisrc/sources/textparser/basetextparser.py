import logging
import os
from typing import Union, Tuple, Optional

from gi.repository import Gtk

from guisrc.gui import Gui, ClassSelectorWithSourceGui
from sources.source import GuiWithPreview, TVSourceItemInterface
from sources.textparser.textcaster import GTextCaster
from src.sources.textfile import TextFile, TextFileStructure
from src.sources.textparser.basetextparser import TextStructureParser
from src.sources.textparser.iniparser import IniParser, IniOption
from src.utils import main_log_handler
from utils import glade_path
from widgets.classselector import ClassSelectorWidget


logger = logging.Logger(__name__, level=logging.WARNING)
logger.addHandler(main_log_handler)


class TextParserContainerGui(GuiWithPreview):
    parser_model = None

    def __init__(self):
        if TextParserContainerGui.parser_model is None:
            TextParserContainerGui.parser_model = ClassSelectorWidget.build_model(
                TextStructureParser.classes[TextStructureParser.class_path()])

        super().__init__()

        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(glade_path(), 'sources', 'textparser.glade'),
                                      ('parser_container_box', 'parser_content_sg'))
        self._gui = builder.get_object('parser_container_box')
        ":type: Gtk.Box"
        self._content_sg = builder.get_object('parser_content_sg')
        ":type: Gtk.SizeGroup"
        self.parser_selector = ClassSelectorWithSourceGui(TextParserContainerGui.parser_model, True, True)
        self.parser_selector.set_size_group(self._content_sg)
        self.parser_selector.on_object_class_changed = self.on_parser_class_changed
        self._gui.pack_start(self.parser_selector.gui(), True, True, 0)
        self.textfile: Optional[TextFile] = None
        self.file_structure: Optional[TextFileStructure] = None
        builder.connect_signals(self)
        self.treemodel = None
        self.treemodel_iter = None

    def set_treemodel(self, treemodel):
        self.treemodel = treemodel
        self._set_parser_gui_treemodel()

    def set_iter(self, treemodel_iter):
        self.treemodel_iter = treemodel_iter
        self._set_parser_gui_treemodel_iter()

    def _set_parser_gui_treemodel(self):
        if self.parser_gui is not None:
            try:
                self.parser_gui.set_treemodel(self.treemodel)
            except AttributeError:
                logger.warning("Parser gui '%s' does not support treemodel.",
                            self.parser_gui.__class__.__name__)

    def _set_parser_gui_treemodel_iter(self):
        if self.parser_gui is not None:
            try:
                self.parser_gui.set_iter(self.treemodel_iter)
            except AttributeError:
                logger.warning("Parser gui '%s' does not support treemodel.",
                            self.parser_gui.__class__.__name__)

    def gui(self):
        return self._gui

    def get_preview(self):
        try:
            return self.parser_gui.get_preview()
        except AttributeError:
            return self.preview()

    @property
    def parser_gui(self):
        return self.parser_selector.object_gui

    @property
    def parser(self):
        return self.file_structure.structure_parser

    def set_object(self, textfile_parser: Tuple[TextFile, TextFileStructure]):
        self.updating = True
        self.textfile, self.file_structure = textfile_parser
        self.parser_selector.set_object((self.textfile, self.parser))
        self.updating = False

    def set_context(self, context):
        super().set_context(context)
        self.parser_selector.set_context(context)

    def set_parent_window(self, window=None):
        super().set_parent_window(window)
        self.parser_selector.set_parent_window(window)

    def on_parser_class_changed(self, widget, data=None):
        self._set_parser_gui_treemodel()
        self._set_parser_gui_treemodel_iter()
        if not self.updating:
            self.file_structure.set_structure_parser(self.parser_selector.displayed_object)


class TextParserGui(GuiWithPreview, TVSourceItemInterface):
    pass


class IniParserGui(TextParserGui):
    def __init__(self):

        super().__init__()
        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(glade_path(), 'sources', 'textparser.glade'),
                                      ('ini_parser', 'ini_parser_preview', 'ini_items'))
        self._gui = builder.get_object('ini_parser')
        ":type: Gtk.Paned"
        self.ini_items = builder.get_object('ini_items')
        ":type: Gtk.ListStore"
        self.ini_items_tv = builder.get_object('ini_items_tv')
        ":type: Gtk.TreeView"
        self.remove_item_btn = builder.get_object('remove_item')
        ":type: Gtk.Button"
        self.text_caster = GTextCaster()
        self.text_caster.gui().set_sensitive(False)
        self._gui.add2(self.text_caster.gui())
        self._preview = builder.get_object('ini_parser_preview')
        self.parser: Union[IniParser, None] = None
        self.textfile: Union[TextFile, None] = None
        builder.connect_signals(self)

    def gui(self):
        return self._gui

    def set_object(self, textfile_parser):
        """
        :param (external.dataanalyser.src.sources.textfile.TextFile, IniParser) textfile_parser:
        :return:
        """
        self.updating = True
        self.textfile, self.parser = textfile_parser
        self.ini_items.clear()
        for item_name, options in self.parser.data_items.items():
            self.ini_items.append([item_name, options.section, options.option])

        self.updating = False

    def set_context(self, context):
        super().set_context(context)
        self.text_caster.set_context(context)

    def set_parent_window(self, window=None):
        super().set_parent_window(window)
        self.text_caster.set_parent_window(window)

    def on_export_name_textrenderer_edited(self, renderer, path, new_text, data=None):
        item_iter = self.ini_items.get_iter(path)
        if new_text == self.ini_items[item_iter][0]:
            return
        elif new_text in self.parser.data_items.keys():
            return
        old_name = self.ini_items[item_iter][0]
        self.ini_items[item_iter][0] = new_text
        item = self.parser.data_items.pop(old_name)
        self.parser.data_items[new_text] = item

    def on_section_textrenderer_edited(self, renderer, path, new_text, data=None):
        item_iter = self.ini_items.get_iter(path)
        item_name = self.ini_items[item_iter][0]
        self.parser.data_items[item_name].section = new_text
        self.ini_items[item_iter][1] = new_text

    def on_option_textrenderer_edited(self, renderer, path, new_text, data=None):
        item_iter = self.ini_items.get_iter(path)
        item_name = self.ini_items[item_iter][0]
        self.parser.data_items[item_name].option = new_text
        self.ini_items[item_iter][2] = new_text

    def on_ini_item_cursor_changed(self, treeview, data=None):
        """
        :param Gtk.TreeStore treeview:
        :param data:
        :return:
        """
        item = None
        item_iter = self.ini_items.get_iter(treeview.get_cursor()[0])
        if item_iter is not None:
            item_name = self.ini_items[item_iter][0]
            if self.parser is not None:
                item = self.parser.data_items.get(item_name, None)
        if item is None:
            self.text_caster.gui().set_sensitive(False)
            self.remove_item_btn.set_sensitive(False)
        else:
            self.text_caster.set_object((self.textfile, item.caster))
            self.text_caster.gui().set_sensitive(True)
            self.remove_item_btn.set_sensitive(True)

    def on_add_item_clicked(self, button, data=None):
        i = 0
        new_name = ''
        while True:
            new_name = 'new_{0}'.format(i)
            if new_name not in self.parser.data_items.keys():
                break
            i += 1
        self.ini_items.append([new_name, '', ''])
        self.parser.data_items[new_name] = IniOption('', '')

    def on_remove_item_clicked(self, button, data=None):
        item_iter = self.ini_items.get_iter(self.ini_items_tv.get_cursor()[0])
        if item_iter is not None:
            item_name = self.ini_items[item_iter][0]
            if self.parser is not None:
                self.parser.data_items.pop(item_name)
            self.ini_items.remove(item_iter)


Gui.register_gui_for_class(IniParser, IniParserGui)
