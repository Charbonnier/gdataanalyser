import os
from typing import Dict, Callable, Union, Tuple, Optional

from gi.repository import Gtk

from guisrc.gui import ClassSelectorGui, Gui, RegexEntry
from sources.source import SourceGui, GuiWithPreview, Preview
from sources.textparser.textcaster import GTextCaster
from sources.textparser.textfilepreviewcontext import TextFilePreviewContext
from src.sources.filefinder import FileFinder, FindByRegex, FindByName, FileFinderSource, TRegexGroupId
from guisrc.structure.fieldname import FieldNameEntry
from guisrc.structure.path import PathGui
from structure.context import Context
from utils import glade_path
from widgets.classselector import ClassSelectorWidget


class FileFinderSourceGui(SourceGui):
    def __init__(self):
        super().__init__()
        self._filefindercontainer = FileFinderContainer()
        self.set_active_gui(self._filefindercontainer)

    def set_object(self, file_finder):
        """

        :param src.sources.filefinder.FileFinderSource file_finder:
        :return:
        """
        self._filefindercontainer.set_object(file_finder)


class FileFinderContainer(GuiWithPreview):
    file_finder_model = None

    def __init__(self):
        if FileFinderContainer.file_finder_model is None:
            FileFinderContainer.file_finder_model = ClassSelectorWidget.build_model(
                FileFinder.classes[FileFinder.class_path()])

        self.preview: Union[FileFinderPreview, None] = None
        super().__init__()
        self._field_renamed_obs_id = None
        self._field_renamed_callbacks: Dict[str, Callable[[str, ], None]] = {}

        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(glade_path(), 'sources', 'filefinder.glade'),
                                      ('file_finders_model', 'file_finder_container_box', 'file_finder_size_group',
                                       'file_finder_container_scrl'))
        self.size_group = builder.get_object('file_finder_size_group')
        self.main_gui = builder.get_object('file_finder_container_scrl')
        """:type: Gtk.ScrolledWindow"""
        self.file_finder_selector = FileFinderSelector(class_model=FileFinderContainer.file_finder_model)
        self.file_finder_selector.set_size_group(self.size_group)
        file_finder_selector_container = builder.get_object('file_finder_selector_container')
        ":type: Gtk.Box"
        file_finder_selector_container.pack_start(self.file_finder_selector.gui(), True, True, 0)
        self.path_export_name = FieldNameEntry()
        path_export_name_box = builder.get_object('path_export_name_box')
        ":type: Gtk.Box"
        path_export_name_box.pack_start(self.path_export_name.gui(), True, True, 0)

        self.base_path_gui = PathGui()

        # TODO: Remove all connect_on_change, now using observers
        # self.base_path_gui.connect_on_change(self.on_base_path_changed)
        file_finder_base_path_container = builder.get_object('file_finder_path_attribute_container')
        file_finder_base_path_container.pack_start(self.base_path_gui.gui(), True, True, 0)
        builder.connect_signals(self)
        self.file_finder_source: Union[FileFinderSource, None] = None
        ":type: FileFinderSource"

    def gui(self):
        return self.main_gui

    def build_preview(self):
        return FileFinderPreview(self)

    @property
    def file_finder(self):
        if self.file_finder_source is None:
            return None
        return self.file_finder_source.file_finder

    def set_object(self, file_finder_source: FileFinderSource):
        if self.file_finder_source is not None:
            self.file_finder_source.remove_event_observer(self._field_renamed_obs_id)
        self.file_finder_source = file_finder_source
        self._field_renamed_obs_id = self.file_finder_source.add_event_observer(FileFinderSource.EVENT_FIELD_RENAMED,
                                                                                self.on_field_renamed)
        self._field_renamed_callbacks = {self.file_finder_source.path_export_name: self.on_path_export_name_changed}

        self.file_finder_selector.set_object(self.file_finder_source)
        self.path_export_name.set_object((self.file_finder_source, self.file_finder_source.path_export_name))
        self.base_path_gui.set_object((self.file_finder_source, self.file_finder.search_path))

    def set_context(self, context: Context):
        super().set_context(context)
        self.base_path_gui.set_context(context)
        self.file_finder_selector.set_context(context)
        self.path_export_name.set_context(context)

    def on_field_renamed(self, source: FileFinderSource, old_name: str, new_name: str):
        try:
            cb = self._field_renamed_callbacks.pop(old_name)
        except KeyError:
            pass
        else:
            self._field_renamed_callbacks[new_name] = cb
            cb(new_name)

    def on_path_export_name_changed(self):
        self.path_export_name.set_object((self.file_finder_source, self.file_finder_source.path_export_name))

    # noinspection PyUnusedLocal
    def on_base_path_changed(self, gui, data=None):
        self.preview.preview_restart_update()

    # noinspection PyUnusedLocal
    def on_finder_class_changed(self, widget, data=None):
        if self.updating:
            return


# noinspection PyAbstractClass
class FileFinderPreview(Preview):
    def __init__(self, owner):
        """
        :param FileFinderContainer owner:
        """
        self.owner = None
        ":type: FileFinderContainer"
        super().__init__(owner)
        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(glade_path(), 'sources', 'filefinder.glade'),
                                      ('file_finder_preview_setup', 'file_finder_preview',
                                       'file_finder_preview_updating'))
        self.preview_setup = builder.get_object('file_finder_preview_setup')
        ":type: Gtk.Window"
        self.preview = builder.get_object('file_finder_preview')
        self.preview_files_tv = builder.get_object('file_finder_preview_tv')
        ":type: Gtk.TreeView"
        self.file_finder_preview_folder_btn = builder.get_object('file_finder_preview_folder_btn')
        ":type: Gtk.FileChooserButton"
        self.preview_updating = builder.get_object('file_finder_preview_updating')
        self.use_preview_base_path = False
        self.export_names_columns = {}
        self.export_names_indexes = {}

        self._files_generator = None
        self._preview_model = None
        ":type: Gtk.ListStore"
        self._preview_n_fields = None
        self._n_files = 0

        self.context: TextFilePreviewContext = None
        self.set_context(TextFilePreviewContext())
        builder.connect_signals(self)

    def gui(self):
        return self.preview

    def setup_gui(self):
        return self.preview_setup

    def set_context(self, context):
        super().set_context(context)
        self.updating = True
        if self.context is not None:
            self.file_finder_preview_folder_btn.set_filename(self.context.folder_path)
        self.updating = False

    # noinspection PyUnusedLocal
    def on_file_finder_preview_folder_chk_toggled(self, widget, data=None):
        """
        :param Gtk.CheckButton widget:
        :param data:
        :return:
        """
        self.use_preview_base_path = widget.get_active()

    # noinspection PyUnusedLocal
    def on_file_finder_preview_folder_btn_file_set(self, widget, data=None):
        """
        :param Gtk.FileChooserButton widget:
        :param data:
        :return:
        """
        if not self.updating:
            self.context.folder_path = widget.get_filename()

    def preview_update_started(self):
        if self.use_preview_base_path:
            path = self.context.folder_path
        else:
            path = self.owner.file_finder.search_path.path

        self.build_preview_model()
        self.preview_updating.show()
        self._files_generator = self.owner.file_finder.enumerate_files(path)
        self._preview_model = self.preview_files_tv.get_model()
        self._preview_n_fields = self._preview_model.get_n_columns()
        self._n_files = 0

    def preview_update_function(self):
        try:
            f = next(self._files_generator)
            if f is None:
                return False
            attributes = self.owner.file_finder.get_file_attributes(f)
        except StopIteration:
            return True
        else:
            row = ['']*self._preview_n_fields
            row[0] = str(self._n_files)
            self._n_files += 1
            row[1] = f
            for attribute_name, value in attributes.items():
                row[self.export_names_indexes[attribute_name]] = str(value)
            self._preview_model.append(row)
            return False

    def preview_update_ended(self):
        self.preview_updating.hide()

    def make_export_column(self, attribute, index):
        try:
            column, cell_renderer = self.export_names_columns[attribute]
        except KeyError:
            cell_renderer = Gtk.CellRendererText()
            column = Gtk.TreeViewColumn(cell_renderer=cell_renderer)
            self.export_names_columns[attribute] = column, cell_renderer
            self.preview_files_tv.append_column(column)
        export_name = self.owner.file_finder.file_attributes_export_names.get(attribute, '')
        self.export_names_indexes[attribute] = index
        column.clear_attributes(cell_renderer)
        column.add_attribute(cell_renderer, 'text', index)
        column.set_title(export_name)

    def set_attribute_export_name(self, attribute, name):
        self.export_names_columns[attribute][0].set_title(name)

    def build_preview_model(self):
        try:
            attributes_name = self.owner.file_finder.get_active_attributes_export_names()
        except StopIteration:
            attributes_name = set()
        to_remove = self.export_names_columns.keys() - attributes_name
        for attribute in to_remove:
            column, _ = self.export_names_columns.pop(attribute)
            self.export_names_indexes.pop(attribute)
            self.preview_files_tv.remove_column(column)
        for index, attribute_name in enumerate(attributes_name):
            self.make_export_column(attribute_name, index + 2)
        self.preview_files_tv.set_model(Gtk.ListStore(*[str] * (2 + len(attributes_name))))


class FileFinderSelector(ClassSelectorGui):
    def __init__(self, class_model):
        super().__init__(class_model)
        self.file_finder_source: Union[FileFinderSource, None] = None
        self._file_finder_setted_id = None

    def set_object(self, object_to_follow: FileFinderSource):
        if self.file_finder_source is not None:
            self.file_finder_source.remove_event_observer(self._file_finder_setted_id)
        self.file_finder_source = object_to_follow
        self._file_finder_setted_id = self.file_finder_source.add_event_observer(
            FileFinderSource.EVENT_FILE_FINDER_SET,
            self.on_file_finder_setted)

        super().set_object(object_to_follow.file_finder)

    def on_file_finder_setted(self, source: FileFinderSource):
        super().set_object(source.file_finder)

    def set_object_gui(self):
        if not self.updating:
            self.file_finder_source.set_file_finder(self.displayed_object)
        self.object_gui.set_object(self.file_finder_source)


class GRegexCaster(GTextCaster):
    def __init__(self):
        super().__init__()
        self.group_caster_index: Optional[TRegexGroupId] = None
        self.finder: Optional[FindByRegex] = None

    def set_object(self, source_caster_idx: Tuple[FileFinderSource, TRegexGroupId]):
        source, self.group_caster_index = source_caster_idx
        # noinspection PyTypeChecker
        self.finder: FindByRegex = source.file_finder
        source_caster = source, self.finder.group_caster[self.group_caster_index]
        super().set_object(source_caster)

    def on_export_type_changed(self, widget, data=None):
        super().on_export_type_changed(widget, data)
        if not self.updating:
            self.finder.group_caster[self.group_caster_index] = self.text_caster


class FindByRegexGui(Gui):
    def __init__(self):
        super().__init__()
        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(glade_path(), 'sources', 'filefinder.glade'),
                                      ('find_by_regex', 'regex_groups_lst', 'file_finder_by_regex_size_group'))
        self.regex_groups_lst: Gtk.ListStore = builder.get_object('regex_groups_lst')
        self.regex_groups_tv: Gtk.TreeView = builder.get_object('regex_groups_tv')
        self._gui = builder.get_object('find_by_regex')
        regex_entry = builder.get_object('regex_entry')
        restore_regex = builder.get_object('restore_regex')
        invalid_regex_info = builder.get_object('invalid_regex_info')
        invalid_regex_message = builder.get_object('invalid_regex_message')
        self.regex_entry = RegexEntry(regex_entry, restore_regex, invalid_regex_info, invalid_regex_message)
        self.regex_entry.changed_cb = self.on_regex_entry_changed
        self.match_subdir: Gtk.CheckButton = builder.get_object('match_subdir')
        self.regex_group_export_box: Gtk.Box = builder.get_object('regex_group_export_box')
        self.regex_group_export: Gtk.Entry = builder.get_object('regex_group_export')

        self.text_caster: GRegexCaster = GRegexCaster()
        self.regex_group_export_box.pack_start(self.text_caster.gui(), True, True, 0)

        self.textfile: Union[FileFinderSource, None] = None
        self.file_finder: Union[FindByRegex, None] = None
        export_name_box: Gtk.Box = builder.get_object('export_name_box')

        self.field_name = FieldNameEntry()
        self._export_field_renamed_id = None
        self._regex_changed_id = None
        self._selected_group = None

        export_name_box.pack_start(self.field_name.gui(), True, True, 0)
        self._sized_widgets = [builder.get_object('label1'),
                               builder.get_object('label8'),
                               builder.get_object('label9'),
                               builder.get_object('label10')
                               ]
        builder.connect_signals(self)
        self.updating = False

    def set_size_group(self, size_group):
        super().set_size_group(size_group)
        self.text_caster.set_size_group(size_group)

    def sized_widgets(self):
        return self._sized_widgets

    def gui(self):
        return self._gui

    def set_object(self, source: FileFinderSource):
        if self.textfile is not None:
            self.file_finder.remove_event_observer(self._export_field_renamed_id)
            self.file_finder.remove_event_observer(self._regex_changed_id)
        self.textfile = source
        # noinspection PyTypeChecker
        self.file_finder: FindByRegex = source.file_finder
        self._export_field_renamed_id = self.file_finder.add_event_observer(FileFinder.EVENT_EXPORT_FIELD_RENAMED,
                                                                            self.on_export_field_renamed)
        self._regex_changed_id = self.file_finder.add_event_observer(FindByRegex.EVENT_REGEX_CHANGED,
                                                                     self.on_regex_changed)
        self.updating = True
        self.regex_entry.set_regex(self.file_finder.regex)
        self.match_subdir.set_active(self.file_finder.match_subdirs)
        self.set_groups()
        self.updating = False

    def set_context(self, context):
        super().set_context(context)
        self.field_name.set_context(self.context)
        self.text_caster.set_context(self.context)

    def set_parent_window(self, window=None):
        super().set_parent_window(window)
        self.field_name.set_parent_window(self.parent_window)
        self.text_caster.set_parent_window(self.parent_window)

    def set_groups(self):
        self.regex_groups_lst.clear()
        for group, _ in sorted(self.file_finder.group_index.items(), key=lambda t: t[1]):
            self.regex_groups_lst.append([group[-1], group])

    def on_export_field_renamed(self, _: FindByRegex, attribute, new_name: str):
        if attribute == self._selected_group:
            self.field_name.set_object((self.textfile, new_name))

    def on_regex_changed(self, file_finder: FindByRegex):
        self.updating = True
        try:
            self.regex_entry.set_regex(file_finder.regex)
            self.set_groups()
        finally:
            self.updating = False

    def on_regex_group_export_state_set(self, widget: Gtk.CheckButton, _=None):
        if not self.updating:
            path, column = self.regex_groups_tv.get_cursor()
            if path is None:
                return
            group_iter = self.regex_groups_lst.get_iter(path)
            row = self.regex_groups_lst[group_iter]
            self.file_finder.enable_attribute(row[1], widget.get_active())
            self.field_name.gui().set_sensitive(widget.get_active())
            self.field_name.update_field_name()

    # noinspection PyUnusedLocal
    def on_match_subdir_state_set(self, widget, data=None):
        if not self.updating:
            self.file_finder.match_subdirs = self.match_subdir.get_active()

    # noinspection PyUnusedLocal
    def on_regex_entry_changed(self, regex_entry: RegexEntry):
        if not self.updating:
            self.file_finder.regex = self.regex_entry.get_regex()

    # noinspection PyUnusedLocal
    def on_regex_group_cursor_changed(self, widget, data=None):
        path, column = self.regex_groups_tv.get_cursor()
        if path is None:
            self.regex_group_export_box.set_sensitive(False)
        else:
            self.regex_group_export_box.set_sensitive(True)
        self.update_regex_group_export(path)

    def update_regex_group_export(self, path: str):
        if path is None:
            return
        self.updating = True
        try:
            group_iter = self.regex_groups_lst.get_iter(path)
            self._selected_group = self.regex_groups_lst[group_iter][1]
            self.text_caster.set_context(self.context)
            self.text_caster.set_object((self.textfile, self._selected_group))
            self.text_caster.set_parent_window(self.parent_window)
            group_enabled = self._selected_group in self.file_finder.enabled_file_attributes
            self.regex_group_export.set_active(group_enabled)
            self.field_name.gui().set_sensitive(group_enabled)
            name = self.file_finder.get_all_attributes_export_names().get(self._selected_group, 're_group')
            self.field_name.set_object((self.textfile, name))
        finally:
            self.updating = False

    def destroy(self):
        super().destroy()
        self.regex_entry.changed_cb = None
        self.text_caster.on_change = None
        self.text_caster.destroy()


class FindByNameGui(Gui):
    def __init__(self):
        super().__init__()
        self._on_file_finder_name_changed_id = None
        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(glade_path(), 'sources', 'filefinder.glade'),
                                      ('find_by_name',))
        self._gui = builder.get_object('find_by_name')
        self.name_entry: Gtk.Entry = builder.get_object('name_entry')
        self.textfile: Optional[FileFinderSource] = None
        self.file_finder: Optional[FindByName] = None
        builder.connect_signals(self)
        self.updating = False

    def gui(self):
        return self._gui

    def set_object(self, ff_source: FileFinderSource):
        if self.textfile is not None:
            self.file_finder.remove_event_observer(self._on_file_finder_name_changed_id)
        self.textfile = ff_source
        # noinspection PyTypeChecker
        self.file_finder: FindByName = ff_source.file_finder
        self.file_finder.add_event_observer(FindByName.EVENT_FILE_NAME_CHANGED, self.on_file_name_changed)
        self.updating = True
        self.name_entry.set_text(self.file_finder.file_name)
        self.updating = False

    # noinspection PyUnusedLocal
    def on_name_entry_changed(self, widget, data=None):
        if not self.updating:
            self.file_finder.file_name = self.name_entry.get_text()

    # noinspection PyUnusedLocal
    def on_file_name_changed(self, find_by_name: FindByName):
        self.updating = True
        try:
            self.name_entry.set_text(self.file_finder.file_name)
        finally:
            self.updating = False


Gui.register_gui_for_class(FindByRegex, FindByRegexGui)
Gui.register_gui_for_class(FindByName, FindByNameGui)
Gui.register_gui_for_class(FileFinderSource, FileFinderSourceGui)
