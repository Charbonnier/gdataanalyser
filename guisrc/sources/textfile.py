import os
from typing import Union, Tuple, Optional

from gi.repository import Gtk, Gdk

from guisrc.gui import Gui, RegexEntry, ClassSelectorGui
from guisrc.utils import glade_path
from sources.source import GuiWithPreview, Preview, TreeViewSource, TVSourceItemInterface
from sources.textparser.basetextparser import TextParserContainerGui
from sources.textparser.tableparser import TableParserGui  # Makes TableParser available in the Gui
from sources.textparser.textfilepreviewcontext import TextFilePreviewContext
from src.sources.textfile import MatchLine, MatchFirstLine, MatchLastLine, \
    MatchLineNumber, MatchLineContent, MatchLineRegex, TextFile, TextFileStructure
from guisrc.structure.path import PathGui
from src.sources.textparser.basetextparser import TextStructureParser
from structure.mutative import IntegerGui
from widgets.classselector import ClassSelectorWidget

TAG_FILE = 'Files'
TAG_STRUCTURE = 'Structure'
TAG_LIMIT = 'Limit'
TAG_PARSER = 'Parser'


class GFileRoot(PathGui, TVSourceItemInterface):
    def __init__(self):
        super().__init__()
        self.can_create_child = True
        self.can_remove_child = True
        self.preview_context: Optional[TextFilePreviewContext] = None
        self.source: TextFile = None

    @property
    def textfile(self) -> TextFile:
        return self.source

    def add_child(self, index, treemodel_iter):
        structure = TextFileStructure()
        self.textfile.insert_file_structure(structure, index)
        tree_item = GTextFileStructure()
        value = self.textfile, index
        tree_item.set_iter(treemodel_iter)
        self.setup_child_item(tree_item, value)
        return tree_item

    def rem_child(self, index, treemodel_iter):
        return self.textfile.remove_file_structure(index)

    def setup_child_item(self, tree_item, value):
        super().setup_child_item(tree_item, value)
        tree_item.set_context(self.context)
        tree_item.set_parent_window(self.parent_window)
        tree_item.set_object(value)
        tree_item.preview.set_context(self.preview_context)

    def set_object(self, textfile: TextFile):
        self.source = textfile
        super().set_object((self.textfile, self.textfile.file_path))
        for i, fs in enumerate(self.textfile.file_structure):
            structure_gui = GTextFileStructure()
            s_iter = self.treemodel.append(self.treemodel_iter,
                                           [fs.user_name, structure_gui, structure_gui.editable])
            structure_gui.set_iter(s_iter)
            self.setup_child_item(structure_gui, (self.textfile, i))


class GTextFile(TreeViewSource):
    def __init__(self):
        super().__init__()
        self.file_root = GFileRoot()
        self.file_root.set_treemodel(self._attributes_model)
        self.file_root.treemodel_iter = self._attributes_model.append(None, ['Files', self.file_root, False])

    def set_context(self, context):
        super().set_context(context)
        self.file_root.set_context(context)

    def set_object(self, textfile: TextFile):
        self._clear()

        self.textfile = textfile
        self._attributes_model.clear()
        self.file_root.treemodel_iter = self._attributes_model.append(None, ['Files', self.file_root, False])
        self.file_root.set_object(self.textfile)


# noinspection PyAbstractClass
class FileStructurePreview(Preview):
    def __init__(self, owner):
        self.owner: Optional[GTextFileStructure] = None
        super().__init__(owner)
        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(glade_path(), 'sources', 'filestructure.glade'),
                                      ('structure_preview_setup', 'structure_preview',
                                       'structure_file_content_buffer', 'structure_file_content_tag_table'))

        self._preview: Gtk.Box = builder.get_object('structure_preview')
        self._preview_setup_gui = builder.get_object('structure_preview_setup')
        self.limit_size_group = builder.get_object('structure_setup_size')
        self.limit_preview = builder.get_object('structure_limit_preview')
        self.limit_preview_buffer: Gtk.TextBuffer = builder.get_object('structure_file_content_buffer')
        self.structure_preview_file_btn: Gtk.FileChooserButton = builder.get_object('structure_preview_file_btn')
        self.limit_preview_buffer.create_tag('structure_highlight', paragraph_background_rgba=Gdk.RGBA(.59, 1., .59))
        self.preview_no_file_selected: Gtk.InfoBar = builder.get_object('structure_preview_no_file_selected')
        self.context: TextFilePreviewContext = None
        self._file_parser = None
        builder.connect_signals(self)

    def gui(self):
        return self._preview

    def setup_gui(self):
        return self._preview_setup_gui

    def on_context_changed(self):
        self.update_preview_setup()
        super().on_context_changed()

    def set_context(self, context: TextFilePreviewContext):
        super().set_context(context)
        self.update_preview_setup()

    def update_preview_setup(self):
        self.updating = True
        if self.context is not None and self.context.file_path:
            self.structure_preview_file_btn.set_filename(self.context.file_path)
            self.update_file_selection_hint()
        self.updating = False

    # noinspection PyUnusedLocal
    def on_structure_preview_file_btn_file_set(self, widget: Gtk.FileChooserButton, data=None):
        if not self.updating:
            self.context.file_path = widget.get_filename()
            self.preview_start_update()
        self.update_file_selection_hint()

    def update_file_selection_hint(self):
        if self.context.file_path:
            self.preview_no_file_selected.hide()
        else:
            self.preview_no_file_selected.show()

    def preview_update_started(self):
        self._file_parser = self._preview_parse_file()
        self.owner.fileStructure.clear()
        self.limit_preview_buffer.delete(self.limit_preview_buffer.get_start_iter(),
                                         self.limit_preview_buffer.get_end_iter())

    def _preview_parse_file(self):
        with open(self.context.file_path, 'r') as f:
            for line in f:
                yield line

    def preview_update_function(self):
        try:
            line = next(self._file_parser)
        except StopIteration:
            return True
        except:
            return True
        else:
            part = self.owner.fileStructure.add_line(line)
            if part:
                self.limit_preview_buffer.insert_with_tags_by_name(self.limit_preview_buffer.get_end_iter(), line,
                                                                   'structure_highlight')
            else:
                self.limit_preview_buffer.insert(self.limit_preview_buffer.get_end_iter(), line)

            return False

    def preview_update_ended(self):
        pass

    # noinspection PyUnusedLocal
    def on_goto_select_file(self, widget, data=None):
        self.setup_gui().get_parent().show()


class GTextFileStructure(GuiWithPreview, TVSourceItemInterface):
    match_line_model = None

    def __init__(self):
        if GTextFileStructure.match_line_model is None:
            GTextFileStructure.match_line_model = ClassSelectorWidget.build_model(
                MatchLine.classes[MatchLine.class_path()])

        self.preview: Optional[FileStructurePreview] = None
        super().__init__()
        self.editable = True
        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(glade_path(), 'sources', 'filestructure.glade'),
                                      ('structure_gui', 'structure_setup_size',
                                       'file_structure_model'))
        self._gui: Gtk.Box = builder.get_object('structure_gui')
        self.limit_gui = builder.get_object('structure_limit_box')
        self.label_size_group = builder.get_object('structure_setup_label_size')
        _start_box: Gtk.Box = builder.get_object('start_line_class_box')
        _end_box: Gtk.Box = builder.get_object('end_line_class_box')
        self.start_line_include: Gtk.CheckBox = builder.get_object('start_line_include')
        self.end_line_include: Gtk.CheckBox = builder.get_object('end_line_include')

        self.start_line_selector = ClassSelectorGui(GTextFileStructure.match_line_model)
        self.end_line_selector = ClassSelectorGui(GTextFileStructure.match_line_model)
        _start_box.pack_start(self.start_line_selector.gui(), True, True, 0)
        _end_box.pack_start(self.end_line_selector.gui(), True, True, 0)
        self.start_line_selector.on_object_class_changed = self.on_start_line_class_changed
        self.end_line_selector.on_object_class_changed = self.on_end_line_class_changed

        builder.connect_signals(self)
        self.text_parser_container = TextParserContainerGui()
        self._obs_set_parser_id = None
        self._gui.pack_start(self.text_parser_container.gui(), True, True, False)
        self.textFile: Union[TextFile, None] = None
        self.fileStructure: Union[TextFileStructure, None] = None

    def set_treemodel(self, treemodel):
        super().set_treemodel(treemodel)
        self.text_parser_container.set_treemodel(treemodel)

    def set_iter(self, treemodel_iter):
        super().set_iter(treemodel_iter)
        self.text_parser_container.set_iter(treemodel_iter)

    def get_can_create_child(self):
        parser_gui = self.text_parser_container.parser_gui
        if parser_gui is not None:
            ans = parser_gui.get_can_create_child()
        else:
            ans = False
        return ans

    def get_can_remove_child(self):
        return self.text_parser_container.parser_gui.get_can_remove_child()

    def add_child(self, index, treemodel_iter):
        return self.text_parser_container.parser_gui.add_child(index, treemodel_iter)

    def rem_child(self, index, treemodel_iter):
        return self.text_parser_container.parser_gui.rem_child(index, treemodel_iter)

    def build_preview(self):
        return FileStructurePreview(self)

    def gui(self):
        return self._gui

    def hide(self):
        for widget in self._gui.get_children():
            widget.hide()
        self.preview.limit_preview.hide()

    def show_limit(self):
        self.hide()
        self.limit_gui.show()
        self.preview.limit_preview.show()

    def show_parser(self):
        self.hide()
        self.text_parser_container.gui().show()

    def set_object(self, structure: Tuple[TextFile, int]):
        self.updating = True
        if self.fileStructure is not None:
            self.fileStructure.remove_event_observer(self._obs_set_parser_id)
        self.textFile = structure[0]
        self.fileStructure = self.textFile.get_file_structure(structure[1])
        self._obs_set_parser_id = self.fileStructure.add_event_observer(TextFileStructure.EVENT_SET_PARSER,
                                                                        self.on_parser_class_changed)
        self.start_line_selector.set_object(self.fileStructure.start)
        self.end_line_selector.set_object(self.fileStructure.end)
        self.start_line_include.set_active(self.fileStructure.include_start)
        self.end_line_include.set_active(self.fileStructure.include_end)
        self.text_parser_container.set_object((self.textFile, self.fileStructure))
        self.updating = False

    def set_context(self, context):
        super().set_context(context)
        self.start_line_selector.set_context(context)
        self.end_line_selector.set_context(context)
        self.text_parser_container.set_context(context)

    def set_parent_window(self, window=None):
        super().set_parent_window(window)
        self.start_line_selector.set_parent_window(window)
        self.end_line_selector.set_parent_window(window)
        self.text_parser_container.set_parent_window(window)

    def on_start_line_class_changed(self, class_selector_gui: ClassSelectorGui):
        if self.updating:
            return
        self.fileStructure.start = class_selector_gui.displayed_object

    def on_end_line_class_changed(self, class_selector_gui: ClassSelectorGui):
        if self.updating:
            return
        self.fileStructure.end = class_selector_gui.displayed_object

    # noinspection PyUnusedLocal
    def on_start_line_include_toggled(self, widget, data=None):
        if not self.updating:
            self.fileStructure.include_start = widget.get_active()
            self.preview.preview_restart_update()

    # noinspection PyUnusedLocal
    def on_end_line_include_toggled(self, widget, data=None):
        if not self.updating:
            self.fileStructure.include_end = widget.get_active()
            self.preview.preview_restart_update()

    def on_parser_class_changed(self, file_structure: TextFileStructure, old_parser: TextStructureParser):
        while True:
            child_iter = self.treemodel.iter_children(self.treemodel_iter)
            if child_iter is None:
                break
            self.treemodel.remove(child_iter)

    def on_title_cell_edited(self, path, new_title):
        self.fileStructure.user_name = new_title
        self.treemodel[self.treemodel_iter][0] = new_title

    # noinspection PyUnusedLocal
    def match_line_changed(self, matchline, data=None):
        self.preview.preview_restart_update()

    def destroy(self):
        super().destroy()
        self.start_line_selector.destroy()
        self.end_line_selector.destroy()


class MatchLineGui(Gui):
    def __init__(self):
        super().__init__()
        self._gui = Gtk.Box()

    def set_object(self, object_to_follow):
        pass

    def gui(self):
        return self._gui


class MatchLineNumberGui(Gui):
    def __init__(self):
        super().__init__()
        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(glade_path(), 'sources', 'filestructure.glade'),
                                      ('match_line_number_gui', ))
        self._gui = builder.get_object('match_line_number_gui')
        self._line_number_entry = IntegerGui()
        self._gui.pack_start(self._line_number_entry.gui, True, True, 0)
        self._line_number_entry.on_value_changed = self.on_line_number_entry_changed
        self.updating = False
        self.match_line: Union[MatchLineNumber, None] = None

    # noinspection PyUnusedLocal
    def on_line_number_entry_changed(self, integer_gui):
        if not self.updating:
            if self.match_line is not None:
                self.match_line.line_to_match = integer_gui.value

    def set_object(self, object_to_follow: MatchLineNumber):
        self.match_line = object_to_follow
        self._line_number_entry.set_value(self.match_line.line_to_match)

    def destroy(self):
        super().destroy()
        self._line_number_entry = None

    def gui(self):
        return self._gui


class MatchLineRegexGui(Gui):
    def __init__(self):
        super().__init__()
        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(glade_path(), 'sources', 'filestructure.glade'),
                                      ('match_line_regex_gui', ))
        self._gui = builder.get_object('match_line_regex_gui')
        regex_entry = builder.get_object('regex_entry')
        restore_regex = builder.get_object('restore_regex')
        invalid_regex_info = builder.get_object('invalid_regex_info')
        invalid_regex_message = builder.get_object('invalid_regex_message')
        self.regex_entry = RegexEntry(regex_entry, restore_regex, invalid_regex_info, invalid_regex_message)
        self.regex_entry.changed_cb = self.on_regex_entry_changed
        self.updating = False
        self.match_line: Union[MatchLineRegex, None] = None
        builder.connect_signals(self)

    def set_regex(self, regex):
        if self.match_line is not None:
            self.match_line.regex = regex

    def set_object(self, object_to_follow):
        self.match_line = object_to_follow
        self.regex_entry.set_regex(self.match_line.regex)

    # noinspection PyUnusedLocal
    def on_regex_entry_changed(self, regex_entry):
        if not self.updating:
            self.set_regex(self.regex_entry.get_regex())

    def destroy(self):
        super().destroy()
        self.regex_entry.changed_cb = None

    def gui(self):
        return self._gui


class MatchLineContentGui(Gui):
    def __init__(self):
        super().__init__()
        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(glade_path(), 'sources', 'filestructure.glade'),
                                      ('match_line_content_gui', ))
        self._gui = builder.get_object('match_line_content_gui')
        self.line_content: Gtk.Entry = builder.get_object('line_content_entry')
        self.updating = False
        self.match_line: Union[MatchLineContent, None] = None
        builder.connect_signals(self)

    def set_object(self, object_to_follow: MatchLineContent):
        self.match_line = object_to_follow
        self.updating = True
        self.line_content.set_text(self.match_line.content)
        self.updating = False

    # noinspection PyUnusedLocal
    def on_line_content_entry_changed(self, widget, data=None):
        if self.updating:
            return
        else:
            self.match_line.content = self.line_content.get_text()

    def gui(self):
        return self._gui


Gui.register_gui_for_class(MatchFirstLine, MatchLineGui)
Gui.register_gui_for_class(MatchLastLine, MatchLineGui)
Gui.register_gui_for_class(MatchLineNumber, MatchLineNumberGui)
Gui.register_gui_for_class(MatchLineContent, MatchLineContentGui)
Gui.register_gui_for_class(MatchLineRegex, MatchLineRegexGui)
Gui.register_gui_for_class(TextFile, GTextFile)
