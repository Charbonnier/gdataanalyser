import os
from abc import abstractmethod
from typing import List, Dict, Tuple

from gi.repository import Gtk

from guisrc.gui import Gui
from sources.source import SourceGui
from src.structure.mutative import MutableMemberSetter
from src.sources.function import FunctionSource
from structure.fieldname import FieldNameEntry
from structure.mutative import VariantSetterGui
from utils import glade_path
from widgets.listboxmanager import ManagedListBox, ListBoxManager

POSITIONAL_ARGUMENT = 0
KEYWORD_ARGUMENT = 1
VARIABLE_ARGUMENT = 2
KEYWORD_VARARG = 3
RETURNED_ELEMENT = 4
UNUSED_ELEMENT = 5


class FunctionGui(SourceGui):
    functions_model = Gtk.ListStore(str, str)

    def __init__(self):
        super().__init__()
        self.function_source: FunctionSource = None
        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(glade_path(), 'sources', 'function.glade'),
                                      ('function_main', 'function_selection_dialog', 'sections_size_group'))
        self.function_selector = builder.get_object('function_selection_dialog')
        ":type: Gtk.Dialog"
        self.function_treeview = builder.get_object('function_treeview')
        ":type: Gtk.TreeView"
        self.function_select_button = builder.get_object('function_select')
        ":type: Gtk.ToggleButton"
        self.function_treeview.set_model(FunctionGui.functions_model)
        self.function_gui = builder.get_object('function_main')
        ":type: Gtk.Box"
        self.sections_container = builder.get_object('sections_container')
        ":type: Gtk.Box"
        self.id_to_gui = {}
        ":type: dict[id, Gui]"
        self.ui_ids = []
        ":type: list[id]"
        self.positionals = []
        ":type: list[SignatureItem]"
        self.keywords = {}
        ":type: dict[str, SignatureItem]"

        self.sections_size_group = builder.get_object('sections_size_group')
        ":type: Gtk.SizeGroup"

        self.sections_manager = ManagedListBox()
        self.current_section = None
        ":type: SignatureSection"
        self.sections_container.pack_start(self.sections_manager, True, True, 0)

        self._positional_section = PositionalSection(self.sections_manager, size_group=self.sections_size_group)
        self._keyword_section = KeywordSection(self.sections_manager, size_group=self.sections_size_group)
        self._var_section = VarPositionalSection(self.sections_manager, size_group=self.sections_size_group)
        self._var_keyword_section = VarKeywordSection(self.sections_manager, size_group=self.sections_size_group)
        self._output_section = OutputSection(self.sections_manager, size_group=self.sections_size_group)

        self.clear_parameters()
        self._parameters_container.pack_start(self.function_gui, True, True, 0)
        builder.connect_signals(self)

        self.sections_manager.manager.connect('child_removed', self.on_item_removed)
        self.sections_manager.manager.connect('child_added', self.on_child_added)
        self.sections_manager.manager.connect('child_selected', self.on_child_selected)
        self.sections_manager.manager.connect('move_up', self.on_item_moved_up)
        self.sections_manager.manager.connect('move_down', self.on_item_moved_down)

        self.sections = [self._positional_section, self._keyword_section,
                         self._var_section, self._var_keyword_section,
                         self._output_section]

        FunctionGui.update_functions_list()

    def set_transient(self, transient):
        super().set_transient(transient)
        self.function_selector.set_transient_for(transient)

    @staticmethod
    def update_functions_list():
        for function_id, function in FunctionSource.functions.items():
            FunctionGui.functions_model.append([function.friendly_name, function_id])

    def set_object(self, function_source: FunctionSource):
        self.updating = True
        try:
            self.function_source = function_source

            self.function_select_button.set_label(self.function_source.function.friendly_name)
            for i, row in self.functions_model:
                if row[1] == self.function_source.function_id:
                    break
            else:
                i = 0
            self.function_treeview.set_cursor(i)
            self._set_sections_context()
            self._positional_section.set_object(function_source)
            self._keyword_section.set_object(function_source)
            self._var_section.set_object(function_source)
            self._var_keyword_section.set_object(function_source)
            self._output_section.set_object(function_source)
        finally:
            self.updating = False

        self.activate_section(self._positional_section)
        self.activate_section(self._keyword_section)
        self.activate_section(self._var_section)
        self.activate_section(self._var_keyword_section)

    def set_context(self, context):
        super().set_context(context)
        self._set_sections_context()

    def set_parent_window(self, window=None):
        super().set_parent_window(window)
        self.function_selector.set_transient_for(window)
        self._set_sections_parent_window()

    def _set_sections_context(self):
        self._positional_section.set_context(self.context)
        self._keyword_section.set_context(self.context)
        self._var_section.set_context(self.context)
        self._var_keyword_section.set_context(self.context)
        self._output_section.set_context(self.context)

    def _set_sections_parent_window(self):
        self._positional_section.set_parent_window(self.parent_window)
        self._keyword_section.set_parent_window(self.parent_window)
        self._var_section.set_parent_window(self.parent_window)
        self._var_keyword_section.set_parent_window(self.parent_window)
        self._output_section.set_parent_window(self.parent_window)

    def add_vararg(self):
        pass

    def add_kwvararg(self):
        pass

    def set_function(self, function_id):
        self.function_source.function_id = function_id
        self.function_select_button.set_label(self.function_source.function.friendly_name)
        self.set_object(self.function_source)

    def activate_section(self, section):
        section.header.set_visible(section.get_is_sensitive())

    # noinspection PyUnusedLocal
    def on_function_select_clicked(self, widget, data=None):
        """
        :param Gtk.ToggleButton widget:
        :param data:
        :return:
        """
        self.function_selector.show()
        response = self.function_selector.run()
        self.function_selector.hide()
        if response == Gtk.ResponseType.OK:
            path, _ = self.function_treeview.get_cursor()
            if path is None:
                return
            function_iter = FunctionGui.functions_model.get_iter(path)
            function_id = FunctionGui.functions_model[function_iter][1]
            self.set_function(function_id)

    # noinspection PyUnusedLocal
    def on_function_selection_dialog_delete_event(self, widget, data=None):
        self.function_selector.hide()

    # noinspection PyUnusedLocal
    def on_child_selected(self, manager: ListBoxManager):
        can_add = False
        can_remove = False
        can_reorder = False
        child_builder = None
        self.current_section = None

        row = manager.get_selected()
        if row is None:
            self.sections_manager.manager.set_child_builder(child_builder)
            self.sections_manager.manager.set_can_add(can_add)
            self.sections_manager.manager.set_can_remove(can_remove)
            self.sections_manager.manager.set_can_reorder(can_reorder)
            return

        index = manager.get_selected().get_index()

        sections_end = self.sections[1:]
        sections_end.append(None)

        is_header = False
        for section, section_next in zip(self.sections, sections_end):
            s_index = section.header.get_index()

            if section_next is None:
                s_next_index = index + 1
            else:
                s_next_index = section_next.header.get_index()

            if s_index <= index < s_next_index:
                is_header = index == s_index
                self.current_section = section
                can_add = section.can_add
                child_builder = section.child_builder
                can_remove = section.can_remove and not is_header
                can_reorder = section.can_reorder and not is_header
                break

        self.sections_manager.manager.set_child_builder(child_builder)
        self.sections_manager.manager.set_can_add(can_add)
        self.sections_manager.manager.set_can_remove(can_remove)
        self.sections_manager.manager.set_can_reorder(can_reorder)

        if self.current_section is not None and not is_header:
            self.current_section.on_child_selected(manager)

    # noinspection PyUnusedLocal
    def on_child_added(self, manager: ListBoxManager, signature_widget):
        if self.updating:
            return
        if self.current_section is None:
            raise ValueError('Child added but no section selected')
        row = signature_widget.get_parent()
        new_index = len(self.current_section.signature_guis) + self.current_section.header.get_index()
        manager.move_row(row, new_index)
        self.current_section.on_child_added(manager, signature_widget)

    # noinspection PyUnusedLocal
    def on_item_removed(self, manager: ListBoxManager, index: int):
        """
        :param widgets.listboxmanager.ListBoxManager manager:
        :param int index: index of the item
        :return:
        """
        if self.updating:
            return
        if self.current_section is None:
            raise ValueError('Child added but no section selected')
        self.current_section.on_item_removed(manager, index - self.current_section.header.get_index() - 1)

    # noinspection PyUnusedLocal
    def on_item_moved_up(self, manager: ListBoxManager):
        if self.current_section is None:
            raise ValueError('Child added but no section selected')
        self.current_section.on_item_moved_up(manager)

    # noinspection PyUnusedLocal
    def on_item_moved_down(self, manager: ListBoxManager):
        if self.current_section is None:
            raise ValueError('Child added but no section selected')
        self.current_section.on_item_moved_down(manager)


class SignatureSection(Gui):
    def __init__(self, managed_list_box, can_reorder=True, can_add=True, can_remove=True, size_group=None, title=''):
        super().__init__()
        self.signatures_mlb = managed_list_box

        self._can_reorder = can_reorder
        self._can_add = can_add
        self._can_remove = can_remove

        self.function_source: FunctionSource = None
        self.signature_guis = []
        header_box = Gtk.Box()
        header_box.set_orientation(Gtk.Orientation.VERTICAL)
        header_box.pack_start(Gtk.Label(title), False, False, 0)
        self.header = Gtk.ListBoxRow()
        self.header.add(header_box)
        self.header.show_all()
        self.signatures_mlb.listbox.add(self.header)

        if size_group is not None:
            self.set_size_group(size_group)

    @property
    def can_reorder(self):
        return self._can_reorder

    @property
    def can_add(self):
        return self._can_add

    @property
    def can_remove(self):
        return self._can_remove

    def get_selection_index(self, manager: ListBoxManager):
        return manager.get_selected().get_index() - self.header.get_index() - 1

    def set_parent_window(self, window=None):
        super().set_parent_window(window)
        for signature_gui in self.signature_guis:
            signature_gui.set_parent_window(window)

    def set_context(self, context):
        super().set_context(context)
        for signature_gui in self.signature_guis:
            signature_gui.set_context(context)

    def get_is_sensitive(self):
        return True

    def set_object(self, function: FunctionSource):
        self.function_source = function
        self.updating = True
        signature_items = self.get_signature_items()
        for i, signature_item in enumerate(signature_items):
            try:
                signature_gui = self.signature_guis[i]
            except IndexError:
                signature_gui = self.append_signature_gui()
                self.signatures_mlb.manager.insert_child(signature_gui.gui(),
                                                         self.header.get_index() + len(self.signature_guis))
            signature_gui.set_context(self.context)
            signature_gui.set_parent_window(self.parent_window)
            signature_gui.set_object((self.function_source, signature_item))

        n_items = len(signature_items)
        header_index = self.header.get_index()
        for i in range(n_items, len(self.signature_guis)):
            self.remove_signature_item(n_items)
            self.signatures_mlb.manager.remove_child(n_items + header_index + 1)
        self.updating = False

    def append_signature_gui(self):
        signature_gui = self.build_signature_item(self.signatures_mlb.manager)
        signature_gui.set_parent_window(self.parent_window)
        signature_gui.set_context(self.context)
        self.signature_guis.append(signature_gui)
        if self._size_group is not None:
            self.add_to_size_group(self.signature_guis[-1])
        return signature_gui

    def remove_signature_item(self, index):
        signature_gui = self.signature_guis.pop(index)
        if self._size_group is not None:
            self.remove_from_size_group(signature_gui)
        for i in range(index, len(self.signature_guis)):
            self.signature_guis[i].index = i
        return signature_gui

    @abstractmethod
    def add_to_size_group(self, signature_gui):
        raise NotImplementedError()

    @abstractmethod
    def remove_from_size_group(self, signature_gui):
        raise NotImplementedError()

    def get_signature_items(self):
        return []

    def change_signature(self):
        pass

    @abstractmethod
    def build_signature_item(self, manager: ListBoxManager):
        raise NotImplementedError()

    def child_builder(self, manager: ListBoxManager):
        return self.append_signature_gui().gui()

    def on_item_removed(self, manager: ListBoxManager, index):
        if self.updating:
            return
        self.remove_signature_item(index)

    def on_child_added(self, manager: ListBoxManager, signature_widget):
        pass

    def on_child_selected(self, manager: ListBoxManager):
        pass

    def on_item_moved_up(self, manager: ListBoxManager):
        pass

    # noinspection PyUnusedLocal
    def on_item_moved_down(self, manager: ListBoxManager):
        pass


class SignatureItem(Gui):
    #TODO: this method seems unused, remove it.
    @classmethod
    def get_type(cls):
        return UNUSED_ELEMENT

    def __init__(self, name, can_disable):
        super().__init__()
        self.function_source = None
        ":type: src.sources.function.FunctionSource"
        self.name = name
        self.unused = False
        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(glade_path(), 'sources', 'function.glade'),
                                      ('signature_element',))
        self._gui = builder.get_object('signature_element')
        ":type: Gtk.Box"
        self.enable_switch = builder.get_object('enable')
        ":type: Gtk.Switch"
        self.set_can_disable(can_disable)
        self._sub_gui = self.make_sub_gui()
        self.set_name(name)
        self._gui.pack_start(self._sub_gui, True, True, 0)
        builder.connect_signals(self)

    def gui(self):
        return self._gui

    def set_can_disable(self, can_disable):
        self.enable_switch.set_sensitive(can_disable)

    def set_object(self, function_mm):
        pass

    def get_object(self):
        pass

    def set_unused(self, unused):
        self.unused = True
        self._gui.set_sensitive(False)

    def get_unused(self):
        return self.unused

    def make_sub_gui(self):
        return Gtk.Label()

    def set_name(self, name):
        self.updating = True
        self.name = name
        self.show_name()
        self.updating = False

    def show_name(self):
        self._sub_gui.set_text(self.name)

    def on_enable_state_set(self, widget, state, data=None):
        self._sub_gui.set_sensitive(state)


class Argument(SignatureItem):
    def __init__(self, name, can_disable):
        super().__init__(name, can_disable)
        self.mutative_gui = self.build_mutative_gui()
        self.mutable_member_id = None
        self.mutable_member = MutableMemberSetter('unnamed', 0)
        self.mutative_gui.mutable_member = self.mutable_member
        self.updating = False

    def build_mutative_gui(self):
        return VariantSetterGui()

    def set_context(self, context):
        super().set_context(context)
        self.mutative_gui.set_context(context)

    def set_parent_window(self, window=None):
        super().set_parent_window(window)
        self.mutative_gui.set_parent_window(window)

    def set_object(self, function_mm):
        """
        :param (src.sources.function.FunctionSource, Any) function_mm: The function source to handle and the identifier
         of the parameter.
        :return:
        """
        self.updating = True
        self.function_source, self.mutable_member_id = function_mm
        mm = self._pick_mutable_member()
        if mm is not None:
            self.mutable_member = mm
        self.mutative_gui.set_object((self.function_source, self.mutable_member))
        self.updating = False

    def _pick_mutable_member(self):
        return self.function_source.mutable_members[self.mutable_member_id]

    def get_object(self):
        return self.mutable_member

class PositionalArgument(Argument):
    @classmethod
    def get_type(cls):
        return POSITIONAL_ARGUMENT

    def __init__(self, name):
        self.name_label = None
        ":type: Gtk.Label"
        super().__init__(name, False)
        self.enable_switch.set_visible(False)
        self._gui.pack_start(self.mutative_gui.gui(), True, True, 0)

    def _pick_mutable_member(self):
        try:
            return self.function_source.get_positional(self.mutable_member_id)
        except IndexError:
            return None

    def set_object(self, function_mm):
        super().set_object(function_mm)
        self.name_label.set_text(self.mutable_member.friendly_name)

    def make_sub_gui(self):
        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(glade_path(), 'sources', 'function.glade'),
                                      ('positional_arg_name',))
        self.name_label = builder.get_object('positional_arg_name')
        return self.name_label

    def show_name(self):
        self.name_label.set_text(self.name)


class PositionalSection(SignatureSection):
    def __init__(self, managed_list_box: ManagedListBox, size_group=None):
        self.signature_guis: List[PositionalArgument] = []
        super().__init__(managed_list_box, can_reorder=False, can_add=False, can_remove=False,
                         size_group=size_group, title='Positionals')

    def sized_widgets(self):
        return [p.name_label for p in self.signature_guis]

    def get_signature_items(self):
        return [i for i, _ in enumerate(self.function_source.get_positionals())]

    def get_is_sensitive(self):
        return self.function_source.function.has_positionals

    def set_object(self, function_source: FunctionSource):
        super().set_object(function_source)
        self.check_can_remove()

    def add_to_size_group(self, signature_gui):
        self._size_group.add_widget(signature_gui.gui())

    def remove_from_size_group(self, signature_gui):
        self._size_group.remove_widget(signature_gui.gui())

    @staticmethod
    def default_name(arg_index):
        return 'Unused {}'.format(arg_index)

    def check_can_remove(self):
        self.signatures_mlb.manager.set_can_remove(self.function_source.function.n_positional < len(self.signature_guis))

    def on_item_removed(self, manager: ListBoxManager, index: int):
        super().on_item_removed(manager, index)
        if self.updating:
            return

        for i in range(index, self.function_source.function.n_positional):
            self.signature_guis[i].set_name(self.function_source.positional_argument_name(index)[0])
            self.signature_guis[i].set_object((self.function_source, self.function_source.get_positional(i)))
        self.check_can_remove()

    def build_signature_item(self, manager: ListBoxManager):
        return PositionalArgument(self.default_name(len(manager.controlled_listbox)))

class KeywordArgument(Argument):
    @classmethod
    def get_type(cls):
        return KEYWORD_ARGUMENT

    def __init__(self, name):
        self.name_label = None
        ":type: Gtk.Label"
        super().__init__(name, True)
        self._gui.pack_start(self.mutative_gui.gui(), True, True, 0)

    def set_object(self, function_mm):
        super().set_object(function_mm)
        self.set_name_label()

    def set_name_label(self):
        self.name_label.set_text(self.mutable_member_id[0])

    def _pick_mutable_member(self):
        try:
            return self.function_source.get_keyword(self.mutable_member_id)
        except KeyError:
            return None

    def disable_argument(self):
        self.function_source.remove_keyword_argument(self.mutable_member_id)

    def make_sub_gui(self):
        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(glade_path(), 'sources', 'function.glade'),
                                      ('positional_arg_name',))
        self.name_label = builder.get_object('positional_arg_name')
        return self.name_label

    def show_name(self):
        if self.unused:
            self.name_label.set_markup("<span foreground='#FF0000' strikethrough='true'>{}</span>".format(self.name))
        else:
            self.name_label.set_markup(self.name)

    def set_unused(self, unused):
        self.unused = unused
        self.show_name()

    def on_enable_state_set(self, widget, state, data=None):
        super().on_enable_state_set(widget, state, data)
        if not self.unused:
            if state:
                self.function_source.add_keyword_argument(self.name, mutable=self.get_object())
            else:
                self.function_source.remove_keyword_argument(self.name)


class KeywordSection(SignatureSection):
    def __init__(self, managed_list_box: ManagedListBox, can_reorder=False, can_add=False, can_remove=False,
                 size_group=None, title='Keywords'):
        self.keywords: Dict[str, KeywordArgument] = {}
        self.ord_keywords: List[str] = []
        self.default_prefix = 'Unused'

        super().__init__(managed_list_box, can_reorder, can_add, can_remove,
                         size_group=size_group, title=title)

    def sized_widgets(self):
        return [p.name_label for p in self.keywords.values()]

    def add_to_size_group(self, signature_gui):
        self._size_group.add_widget(signature_gui.name_label)

    def remove_from_size_group(self, signature_gui):
        self._size_group.remove_widget(signature_gui.name_label)

    def get_is_sensitive(self):
        return self.function_source.function.has_kwords

    def append_signature_gui(self):
        signature_gui = super().append_signature_gui()
        ":type: KeywordArgument"
        name = signature_gui.name
        self.keywords[name] = signature_gui
        self.ord_keywords.append(name)
        return signature_gui

    def remove_signature_item(self, index: int):
        signature_gui = super().remove_signature_item(index)
        name = self.ord_keywords.pop(index)
        self.keywords.pop(name)
        return signature_gui

    def build_signature_item(self, manager: ListBoxManager):
        name = self.default_name()
        return KeywordArgument(name)

    def default_name(self):
        i = 0
        while True:
            name = '{} {}'.format(self.default_prefix, i)
            i += 1
            if name not in self.keywords.keys():
                break
        return name

    def get_signature_items(self):
        return [name for name, _ in self.function_source.get_keywords()]

    def on_child_selected(self, manager: ListBoxManager):
        selected_row = manager.get_selected()
        if selected_row is not None:
            index = self.get_selection_index(manager)
            name = self.ord_keywords[index]
            manager.set_can_remove(name not in self.function_source.function.keywords)
        else:
            manager.set_can_remove(False)

    def on_item_removed(self, manager: ListBoxManager, index: int):
        # must be done before ord_keywords is changed.
        if not self.updating:
            name = self.ord_keywords[index]
            self.function_source.remove_keyword_argument(name)
        super().on_item_removed(manager, index)


class VarPositionalArgument(PositionalArgument):
    @classmethod
    def get_type(cls):
        return VARIABLE_ARGUMENT


class VarPositionalSection(SignatureSection):
    def __init__(self, managed_list_box: ManagedListBox, size_group=None):
        self.signature_guis: List[VarPositionalArgument] = []
        super().__init__(managed_list_box, can_reorder=True, can_add=True, can_remove=True,
                         size_group=size_group, title='Additional Positional')

    def sized_widgets(self):
        return [p.name_label for p in self.signature_guis]

    def add_to_size_group(self, signature_gui):
        self._size_group.add_widget(signature_gui.name_label)

    def remove_from_size_group(self, signature_gui):
        self._size_group.add_widget(signature_gui.name_label)

    def get_is_sensitive(self):
        return self.function_source.function.has_var_positional

    def get_signature_items(self):
        return [i for i, _ in enumerate(self.function_source.get_var_positionals())]

    def build_signature_item(self, manager: ListBoxManager):
        return VarPositionalArgument(str(len(manager.controlled_listbox)))

    def get_var_positional_index(self, i: int):
        return i + self.function_source.function.n_positional

    def remove_signature_item(self, index: int):
        signature_gui = super().remove_signature_item(index)
        for i, (signature_item, arg) in enumerate(zip(self.signature_guis, self.function_source.get_var_positionals())):
            signature_item.mutable_member_id = i
            signature_item.set_name(arg.friendly_name)

        return signature_gui

    def on_child_added(self, manager: ListBoxManager, signature_widget):
        super().on_child_added(manager, signature_widget)
        if self.updating:
            return
        self.function_source.append_positional_argument(0)
        self.signature_guis[-1].set_object((self.function_source, self.get_var_positional_index(len(self.signature_guis) - 1)))

    def on_item_removed(self, manager: ListBoxManager, index):
        """
        :param widgets.listboxmanager.ListBoxManager manager:
        :param int index: index of the item
        :return:
        """
        super().on_item_removed(manager, index)
        if self.updating:
            return
        self.function_source.remove_positional_argument(index)

    def on_item_moved_up(self, manager: ListBoxManager):
        old_index = self.get_selection_index(manager)
        new_index = old_index - 1
        new_arg_index = self.get_var_positional_index(new_index)
        old_arg_index = self.get_var_positional_index(old_index)
        self.function_source.move_positional_argument(old_arg_index, new_arg_index)
        for i in range(new_index, len(self.signature_guis)):
            self.signature_guis[i].set_object((self.function_source, self.get_var_positional_index(i)))
        return True

    def on_item_moved_down(self, manager: ListBoxManager):
        old_index = self.get_selection_index(manager)
        new_index = old_index + 1
        new_arg_index = self.get_var_positional_index(new_index)
        old_arg_index = self.get_var_positional_index(old_index)
        self.function_source.move_positional_argument(old_arg_index, new_arg_index)
        for i in range(old_index, len(self.signature_guis)):
            self.signature_guis[i].set_object((self.function_source, self.get_var_positional_index(i)))
        return True


class VarKeyWordArgument(KeywordArgument):
    @classmethod
    def get_type(cls):
        return VARIABLE_ARGUMENT

    def __init__(self, name):
        self.name_label = None
        ":type: Gtk.Entry"
        self.error_icon = None
        ":type: Gtk.Image"
        self.name_box = None
        ":type: Gtk.Box"
        super().__init__(name)
        self.enable_switch.set_visible(False)
        self.updating = False

    def set_name_label(self):
        self.name_label.set_text(self.mutable_member_id)

    def show_name(self):
        self.name_label.set_text(self.name)
        self.error_icon.set_visible(self.unused)

    def set_unused(self, unused):
        if not self.unused:
            self.function_source.remove_keyword_argument(self.mutable_member_id)
        self.unused = unused
        self.error_icon.set_visible(unused)

    def make_sub_gui(self):
        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(glade_path(), 'sources', 'function.glade'),
                                      ('keyword_vararg',))
        self.name_label = builder.get_object('keyword_vararg_name')
        self.error_icon = builder.get_object('keyword_vararg_error')
        self.name_box = builder.get_object('keyword_vararg')
        builder.connect_signals(self)
        return self.name_box

    def on_keyword_vararg_name_changed(self, widget, data=None):
        if self.updating:
            return
        new_name = self.name_label.get_text()

        if new_name == self.mutable_member_id:
            return

        if new_name in self.function_source.kwargs_mm.keys():
            self.set_unused(True)
            return

        if new_name in self.function_source.function.function_parameters_names:
            self.set_unused(True)
            return

        if not self.unused:
            self.function_source.remove_keyword_argument(self.mutable_member_id)
        else:
            self.set_unused(False)
        self.mutable_member_id = new_name
        self.function_source.add_keyword_argument(new_name, mutable=self.get_object())


class VarKeywordSection(KeywordSection):
    def __init__(self, managed_list_box, can_reorder=False, can_add=True, can_remove=True, size_group=None):
        self.keywords: Dict[str, VarKeyWordArgument] = {}
        super().__init__(managed_list_box, can_reorder, can_add, can_remove,
                         size_group=size_group, title='Additional Keywords')
        self.default_prefix = 'New'

    def sized_widgets(self):
        return [p.name_label for p in self.keywords.values()]

    def build_signature_item(self, manager):
        name = self.default_name()
        return VarKeyWordArgument(name)

    def get_signature_items(self):
        return [name for name, _ in self.function_source.get_var_keywords()]

    def get_is_sensitive(self):
        return self.function_source.function.has_var_keyword

    def on_child_added(self, manager: ListBoxManager, signature_widget):
        super().on_child_added(manager, signature_widget)
        if self.updating:
            return
        name = self.ord_keywords[-1]
        self.function_source.add_keyword_argument(name, 0)
        new_keyword = self.keywords[name]
        new_keyword.set_object((self.function_source, name))


class OutputItem(SignatureItem):
    def __init__(self, name):
        self.name_gui: Gtk.Widget = None
        self.name_field: FieldNameEntry = None
        super().__init__(name, True)
        self.index: int = None

    def make_sub_gui(self):
        self.name_field = FieldNameEntry()
        self.name_field.field_name_changed_cb = self.on_output_name_changed
        self.name_gui = self.name_field.gui()
        return self.name_gui

    def set_object(self, function_index: Tuple[FunctionSource, int]):
        super().set_object(function_index)
        self.function_source, self.index = function_index
        self.name_field.set_object((self.function_source, self.function_source.returned_fields[self.index]))
        self.set_ignored(self.index in self.function_source.ignored_fields_indexes)

    def set_context(self, context):
        super().set_context(context)
        self.name_field.set_context(self.context)

    def set_parent_window(self, window=None):
        super().set_parent_window(window)
        self.name_field.set_parent_window(self.parent_window)

    def set_name(self, name):
        pass

    def set_ignored(self, ignored):
        self.updating = True
        self.enable_switch.set_active(not ignored)
        self.updating = False

    def get_ignored(self):
        return self.enable_switch.get_active()

    def on_output_name_changed(self, name):
        self.function_source.returned_fields[self.index] = name

    def on_enable_state_set(self, widget, state, data=None):
        super().on_enable_state_set(widget, state, data)
        if not state:
            self.function_source.ignored_fields_indexes.add(self.index)
        else:
            try:
                self.function_source.ignored_fields_indexes.remove(self.index)
            except KeyError:
                pass
        self.name_field.update_field_name()


class OutputSection(SignatureSection):
    def __init__(self, managed_list_box, can_reorder=True, can_add=True, can_remove=True, size_group=None):
        super().__init__(managed_list_box, can_reorder, can_add, can_remove, size_group=size_group, title='Outputs')
        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(glade_path(), 'sources', 'function.glade'),
                                      ('output_box',))
        self.dispatch_switch = builder.get_object('dispatch_switch')
        ":type: Gtk.Switch"
        container = self.header.get_child()
        container.pack_start(builder.get_object('output_box'), True, True, 0)
        builder.connect_signals(self)

    def build_signature_item(self, manager: ListBoxManager):
        name = self.default_name()
        new_output = OutputItem(name)
        new_output.set_context(self.context)
        new_output.set_parent_window(self.parent_window)
        return new_output

    def default_name(self):
        i = 0
        while True:
            name = 'Output {}'.format(i)
            i += 1
            if name not in self.function_source.returned_fields:
                break
        return name

    def get_signature_items(self):
        return [i for i, _ in enumerate(self.function_source.returned_fields)]

    def set_object(self, function):
        super().set_object(function)
        self.updating = True
        try:
            self.dispatch_switch.set_active(function.dispatch_result)
        finally:
            self.updating = False

    def add_to_size_group(self, signature_gui):
        """
        :param OutputItem signature_gui: 
        :return: 
        """
        self._size_group.add_widget(signature_gui.name_field.entry)

    def remove_from_size_group(self, signature_gui):
        """
        :param OutputItem signature_gui: 
        :return: 
        """
        self._size_group.remove_widget(signature_gui.name_field.entry)

    def on_dispatch_switch_state_set(self, widget, data=None):
        """

        :param Gtk.Switch widget:
        :param data:
        :return:
        """
        if self.updating:
            return
        self.function_source.dispatch_result = widget.get_active()

    def on_child_added(self, manager: ListBoxManager, signature_widget):
        super().on_child_added(manager, signature_widget)
        if self.updating:
            return
        new_output = self.signature_guis[-1]
        self.function_source.returned_fields.append(new_output.name)
        new_output.set_object((self.function_source, len(self.signature_guis) - 1))
        new_output.name_field.update_field_name()

    def on_item_removed(self, manager: ListBoxManager, index):
        """
        :param widgets.listboxmanager.ListBoxManager manager:
        :param int index:
        """
        super().on_item_removed(manager, index)
        for i, item in enumerate(self.signature_guis):
            item.index = i
        if self.updating:
            return
        self.function_source.remove_returned_field(index)

    def on_item_moved_up(self, manager: ListBoxManager):
        """
        :param widgets.listboxmanager.ListBoxManager manager:
        :return:
        """
        old_index = self.get_selection_index(manager)
        new_index = old_index - 1
        self.function_source.move_returned_field(old_index, new_index)
        for i in range(new_index, len(self.signature_guis)):
            self.signature_guis[i].set_object((self.function_source, i))
        return True

    def on_item_moved_down(self, manager: ListBoxManager):
        """
        :param widgets.listboxmanager.ListBoxManager manager:
        :return:
        """
        old_index = self.get_selection_index(manager)
        new_index = old_index + 1
        self.function_source.move_returned_field(old_index, new_index)
        for i in range(old_index, len(self.signature_guis)):
            self.signature_guis[i].set_object((self.function_source, i))
        return True



if __name__ == "__main__":
    from numpy import interp, linspace
    from src.sources.function import Function, FunctionSource
    FunctionSource.register_function('interp', Function(interp, 'interp'))
    FunctionSource.register_function('linspace', Function(linspace, 'linspace'))
    fct_src = FunctionSource()
    fct_src.function_id = 'interp'
    window = Gtk.Window()
    window.set_size_request(800, 600)
    window.connect('delete-event', lambda a, w: Gtk.main_quit())
    function_gui = FunctionGui()
    function_gui.set_object(fct_src)
    fct_src.function_id = 'identity'
    function_gui.set_object(fct_src)
    window.add(function_gui.gui())
    window.show()
    Gtk.main()

Gui.register_gui_for_class(FunctionSource, FunctionGui)
