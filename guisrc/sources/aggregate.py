from sources.function import FunctionGui, SignatureSection, Argument
from guisrc.gui import Gui
from src.sources.aggregate import AggregateSource
from src.sources.function import FunctionSource
from structure.mutative import MutativeMemberSetterGui
from widgets.listboxmanager import ListBoxManager


class AggregateGui(FunctionGui):
    def __init__(self):
        super().__init__()
        self._group_section = GroupSection(self.sections_manager, size_group=self.sections_size_group)
        self._sort_section = SortSection(self.sections_manager, size_group=self.sections_size_group)
        self.sections.extend([self._group_section, self._sort_section])

    def set_object(self, function_source: AggregateSource):
        super().set_object(function_source)
        self.updating = True
        try:
            self._group_section.set_object(function_source)
            self._sort_section.set_object(function_source)
        finally:
            self.updating = False

        self.activate_section(self._group_section)
        self.activate_section(self._sort_section)

    def _set_sections_context(self):
        super()._set_sections_context()
        self._group_section.set_context(self.context)
        self._sort_section.set_context(self.context)

    def _set_sections_parent_window(self):
        super()._set_sections_parent_window()
        self._group_section.set_parent_window(self.parent_window)
        self._sort_section.set_parent_window(self.parent_window)


class GroupSection(SignatureSection):
    def __init__(self, managed_list_box, size_group=None):
        self.function_source: AggregateSource = None
        super().__init__(managed_list_box, can_reorder=False, can_add=True, can_remove=True,
                         size_group=size_group, title='Group Fields')

    def build_signature_item(self, manager):
        return GroupItem('group')

    def get_signature_items(self):
        return self.function_source.groups.setters

    def add_to_size_group(self, signature_gui):
        pass

    def remove_from_size_group(self, signature_gui):
        pass

    def set_object(self, function: FunctionSource):
        super().set_object(function)

    def on_item_removed(self, manager, index):
        super().on_item_removed(manager, index)
        if self.updating:
            return
        self.function_source.remove_group(index)

    def on_child_added(self, manager, signature_widget):
        super().on_child_added(manager, signature_widget)
        if self.updating:
            return
        setter = self.function_source.append_group()
        self.signature_guis[-1].set_object((self.function_source, setter))


class SortSection(SignatureSection):
    def __init__(self, managed_list_box, size_group=None):
        self.function_source: AggregateSource = None
        super().__init__(managed_list_box, can_reorder=True, can_add=True, can_remove=True,
                         size_group=size_group, title='Sort Fields')

    def build_signature_item(self, manager):
        return SortItem('Order')

    def get_signature_items(self):
        return self.function_source.order.setters

    def add_to_size_group(self, signature_gui):
        pass

    def remove_from_size_group(self, signature_gui):
        pass

    def on_item_removed(self, manager, index):
        super().on_item_removed(manager, index)
        if self.updating:
            return
        self.function_source.remove_order(index)

    def on_child_added(self, manager, signature_widget):
        super().on_child_added(manager, signature_widget)
        if self.updating:
            return
        setter = self.function_source.append_order()
        self.signature_guis[-1].set_object((self.function_source, setter))

    def on_item_moved_up(self, manager: ListBoxManager):
        old_index = self.get_selection_index(manager)
        new_index = old_index - 1
        self.function_source.move_order(old_index, new_index)
        setters = self.function_source.order.setters
        for i in range(new_index, len(self.signature_guis)):
            self.signature_guis[i].set_object((self.function_source, setters[i]))

    def on_item_moved_down(self, manager: ListBoxManager):
        old_index = self.get_selection_index(manager)
        new_index = old_index + 1
        self.function_source.move_order(old_index, new_index)
        setters = self.function_source.order.setters
        for i in range(old_index, len(self.signature_guis)):
            self.signature_guis[i].set_object((self.function_source, setters[i]))


class GroupItem(Argument):
    def __init__(self, name):
        super().__init__(name, False)
        self.enable_switch.set_visible(False)
        self._gui.pack_start(self.mutative_gui.gui(), True, True, 0)

    def build_mutative_gui(self):
        return MutativeMemberSetterGui(None)

    def _pick_mutable_member(self):
        return self.mutable_member_id


class SortItem(Argument):
    def __init__(self, name):
        super().__init__(name, False)
        self.enable_switch.set_visible(False)
        self._gui.pack_start(self.mutative_gui.gui(), True, True, 0)

    def build_mutative_gui(self):
        return MutativeMemberSetterGui(None)

    def _pick_mutable_member(self):
        return self.mutable_member_id


Gui.register_gui_for_class(AggregateSource, AggregateGui)
