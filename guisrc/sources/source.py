import os
from abc import abstractmethod

from gi.repository import Gtk, GObject

from guisrc.gui import Gui
from utils import glade_path


# noinspection PyUnusedLocal
class SourceGui(Gui):
    def __init__(self):
        super().__init__()
        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(glade_path(), 'sources', 'source.glade'),
                                      ('source_box', 'attributes', 'preview_window'))
        self.parent_window = None
        """:type: Gtk.Window"""
        self._gui = builder.get_object('source_box')
        ":type: Gtk.Box"
        self._source_box = self._gui
        self._paned = builder.get_object('source_main')
        ":type: Gtk.Paned"
        self._hide_preview_tb = builder.get_object('hide_preview')
        ":type: Gtk.ToolButton"
        self._show_preview_tb = builder.get_object('show_preview')
        ":type: Gtk.ToolButton"
        self._parameter_control_box = builder.get_object('parameter_control_box')
        ":type: Gtk.Box"
        self._parameters_container = builder.get_object('parameters_container_box')
        """:type: Gtk.Box"""
        self._empty_parameters = builder.get_object('empty_parameters')
        self._preview_container = builder.get_object('preview_container_box')
        """:type: Gtk.Box"""
        self._preview_box = builder.get_object('preview_box')
        """:type: Gtk.Box"""
        self._preview_setup_window = builder.get_object('preview_window')
        """:type: Gtk.Window"""
        self._parameters_paned = self._gui
        """:type: Gtk.Paned"""
        self._active_gui = None
        """:type: GuiWithPreview"""
        self._preview_height = None
        builder.connect_signals(self)

    def gui(self):
        return self._gui

    def set_active_gui(self, gui):
        """
        :param GuiWithPreview gui:
        :return:
        """
        if self._active_gui is not None:
            self._parameters_container.remove(self._active_gui.gui())
            preview = self._active_gui.get_preview()
            if preview is not None:
                self._preview_container.remove(preview.gui())
                setup_gui = preview.setup_gui()
                if setup_gui is not None:
                    self._preview_setup_window.remove(setup_gui)
        else:
            self._parameters_container.remove(self._empty_parameters)

        if gui is None:
            self._parameters_container.add(self._empty_parameters)
        else:
            self._parameters_container.pack_start(gui.gui(), True, True, 0)
            preview = gui.get_preview()
            if preview is not None:
                self._preview_container.pack_start(preview.gui(), True, True, 0)
                setup_gui = preview.setup_gui()
                if setup_gui is not None:
                    self._preview_setup_window.add(setup_gui)
        self._active_gui = gui

    def set_parent_window(self, window=None):
        super().set_parent_window(window)
        if self._active_gui is not None:
            self._active_gui.set_parent_window(window)
        self._preview_setup_window.set_transient_for(window)

    def set_context(self, context):
        super().set_context(context)
        if self._active_gui is not None:
            self._active_gui.set_context(context)

    def set_size_group(self, size_group):
        super().set_size_group(size_group)
        if self._active_gui is not None:
            self._active_gui.set_size_group(size_group)

    def show_preview(self):
        self._paned.add2(self._preview_box)
        self._show_preview_tb.hide()
        self._hide_preview_tb.show()

    def hide_preview(self):
        self._paned.remove(self._preview_box)
        self._show_preview_tb.show()
        self._hide_preview_tb.hide()

    def on_preview_properties_clicked(self, widget, data=None):
        if self._active_gui is not None:
            self._preview_setup_window.show()

    def on_hide_preview_clicked(self, widget, data=None):
        self.hide_preview()

    def on_show_preview_clicked(self, widget, data=None):
        self.show_preview()

    def on_preview_update_clicked(self, widget, data=None):
        if self._active_gui is not None:
            self._active_gui.get_preview().preview_restart_update()

    def on_preview_window_delete_event(self, widget, data=None):
        self._preview_setup_window.hide()
        return True

    def clear_parameters(self):
        parameters = self._parameters_container.get_children()
        for parameter in parameters:
            self._parameters_container.remove(parameter)


TVSITEM = 1
EDITABLE = 2


class TVSourceItemInterface:
    def __init__(self) -> None:
        super().__init__()
        self.treemodel_iter = None
        ":type: Gtk.TreeIter"
        self._treemodel = None
        ":type: Gtk.TreeModel"
        self.can_create_child = False
        self.can_remove_child = False
        self.editable = False

    def get_can_create_child(self):
        return self.can_create_child

    def get_can_remove_child(self):
        return self.can_remove_child

    def set_editable(self, editable):
        self.editable = editable
        if self.treemodel is not None and self.treemodel_iter is not None:
            self.treemodel[self.treemodel_iter][EDITABLE] = editable

    def set_treemodel(self, treemodel):
        """

        :param Gtk.TreeModel treemodel:
        :return:
        """
        self._treemodel = treemodel

    @property
    def treemodel(self):
        """
        :rtype: Gtk.TreeModel
        :return:
        """
        return self._treemodel

    def get_name(self):
        return self.__class__.__name__

    def set_iter(self, treemodel_iter):
        """

        :param Gtk.TreeIter treemodel_iter:
        :return:
        """
        self.treemodel_iter = treemodel_iter

    def add_child(self, index, treemodel_iter):
        pass

    def rem_child(self, index, treemodel_iter):
        pass

    def on_title_cell_edited(self, path, new_title):
        pass

    def setup_child_item(self, tree_item, value):
        """

        :param TVSourceItemInterface tree_item:
        :param Any value:
        :return:
        """
        tree_item.set_treemodel(self.treemodel)


# noinspection PyUnusedLocal
class TreeViewSource(SourceGui):
    def __init__(self):
        super().__init__()
        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(glade_path(), 'sources', 'source.glade'),
                                      ('treeview_source', 'attributes'))
        old_gui = self._gui
        self._gui = builder.get_object('treeview_source')
        ":type: Gtk.Paned"
        self._gui.add2(old_gui)
        self._attributes_model = builder.get_object('attributes')
        ":type: Gtk.TreeStore"
        self.add_tbtn = builder.get_object('add_tbtn')
        ":type: Gtk.ToolButton"
        self.remove_tbtn = builder.get_object('remove_tbtn')
        ":type: Gtk.ToolButton"
        builder.connect_signals(self)
        self.selection = None
        self.selected_tree_iter = None
        self.selected_tree_item = None
        ":type: TreeViewItemInterface"

    def on_selected_attribute_changed(self, widget, data=None):
        """
        :param Gtk.TreeView widget:
        :param data:
        :return:
        """
        self.selection = widget.get_selection().get_selected_rows()

        self._clear()
        if len(self.selection) != 0:
            selected_path = self.selection[1][0]
            selected_iter = self._attributes_model.get_iter(selected_path)
            tree_item: TVSourceItemInterface = self._attributes_model.get(selected_iter, 1)[0]
            self._set_gui(tree_item)
            self.selected_tree_iter = selected_iter
            self.selected_tree_item = tree_item
            parent_iter = self._attributes_model.iter_parent(selected_iter)
            if parent_iter is None:
                can_remove = False
            else:
                parent_tree_item: TVSourceItemInterface = self._attributes_model.get(parent_iter, 1)[0]
                can_remove = parent_tree_item.get_can_remove_child()
            self.remove_tbtn.set_sensitive(can_remove)
            self.add_tbtn.set_sensitive(tree_item.get_can_create_child())
        else:
            self.remove_tbtn.set_sensitive(False)
            self.add_tbtn.set_sensitive(False)

    def on_add_tbtn_clicked(self, widget, data=None):
        """
        :param Gtk.ToolButton widget:
        :param data:
        :return:
        """
        if self.selected_tree_item is None:
            return
        index = self._attributes_model.iter_n_children(self.selected_tree_iter)
        new_iter = self._attributes_model.append(self.selected_tree_iter, ['', None, False])
        new_item = self.selected_tree_item.add_child(index, new_iter)
        ":type: TVSourceItemInterface|None"
        if new_item is None:
            self._attributes_model.remove(new_iter)
            return
        new_item.set_iter(new_iter)
        self._attributes_model[new_iter] = [new_item.get_name(), new_item, new_item.editable]

    def on_remove_tbtn_clicked(self, widget, data=None):
        """

        :param Gtk.ToolButton widget:
        :param data:
        :return:
        """
        parent_iter = self._attributes_model.iter_parent(self.selected_tree_iter)
        parent_item = self._attributes_model[parent_iter][1]
        ":type: TreeViewItemInterface"
        child_index = self._attributes_model.get_path(self.selected_tree_iter)[-1]
        parent_item.rem_child(child_index, self.selected_tree_iter)
        self._attributes_model.remove(self.selected_tree_iter)

    def on_title_cell_edited(self, cell_renderer, path, new_text):
        c_iter = self._attributes_model.get_iter(path)
        self._attributes_model[c_iter][TVSITEM].on_title_cell_edited(path, new_text)

    def _clear(self):
        self.clear_parameters()
        self._parameters_container.add(self._empty_parameters)
        previews = self._preview_container.get_children()
        for preview in previews:
            self._preview_container.remove(preview)
        preview_setup = self._preview_setup_window.get_child()
        if preview_setup is not None:
            self._preview_setup_window.remove(preview_setup)
        self._preview_setup_window.hide()
        self._active_gui = None

    def _set_gui(self, tree_item):
        self._pack_gui(tree_item)

    def _pack_gui(self, gui):
        """
        :param GuiWithPreview gui:
        :return:
        """
        if gui is not None:
            self._parameters_container.remove(self._empty_parameters)
            self._parameters_container.pack_start(gui.gui(), True, True, 0)
            self._pack_preview(gui)
        self._active_gui = gui

    def _pack_preview(self, gui):
        try:
            preview = gui.get_preview()
        except AttributeError:
            return
        preview_gui = preview.gui()
        if preview_gui is not None:
            self._preview_container.pack_start(preview_gui, True, True, 0)
            preview_setup = preview.setup_gui()
            if preview_setup is not None:
                self._preview_setup_window.add(preview_setup)


class PreviewContext:
    def __init__(self):
        self._handles = 0
        self._previews = set()
        ":type: set(Preview)"

    def register(self, preview):
        """
        :param Preview preview: The preview to update if context changes.
        """
        if preview in self._previews:
            raise ValueError('Preview already registered')
        else:
            self._previews.add(preview)

    def remove(self, preview):
        """
        :param Preview preview: The preview to update if context changes.
        """
        self._previews.remove(preview)

    def signal_changed(self):
        for preview in self._previews:
            preview.on_context_changed()


class Preview(Gui):
    def __init__(self, owner):
        super().__init__()
        self.owner = owner
        self.preview_update_running = False
        self.preview_stop_update_requested = False
        self.preview_restart_update_requested = False
        self.context = None

    def gui(self):
        return None

    def setup_gui(self):
        return None

    def destroy(self):
        super().destroy()
        self.owner = None
        self.set_context(None)

    def set_window(self, window):
        """
        :param Gtk.Window window:
        """
        self.setup_gui().set_transient_for(window)

    def set_context(self, context):
        if self.context is not None:
            self.context.remove(self)
        self.context = context
        if self.context is not None:
            self.context.register(self)

    def on_context_changed(self):
        self.preview_restart_update()

    def preview_start_update(self):
        if self.preview_update_running:
            return
        self.preview_update_started()
        GObject.idle_add(self.preview_update_step)

    def preview_update_started(self):
        pass

    def preview_stop_update(self):
        """Stops a currently running preview update."""
        if self.preview_update_running:
            self.preview_stop_update_requested = True

    def preview_restart_update(self):
        """Restarts a currently running preview update.
        - preview_stop_update is called.
        - preview_start_update is pending until the currently running update has stopped."""
        if self.preview_update_running:
            self.preview_stop_update_requested = True
            self.preview_restart_update_requested = True
        else:
            self.preview_start_update()

    def preview_update_step(self):
        done = self.preview_update_function()
        if done or self.preview_stop_update_requested:
            self.preview_update_running = False
            self.preview_stop_update_requested = False
            self.preview_update_ended()
            if self.preview_restart_update_requested:
                self.preview_start_update()
            self.preview_restart_update_requested = False
            done = True
        return not done

    @abstractmethod
    def preview_update_function(self):
        """ Execute a part of the preview update.
        :rtype: bool
        :return: True if the update is done"""
        pass

    def preview_update_ended(self):
        pass


class GuiWithPreview(Gui):
    def __init__(self):
        super().__init__()
        self.preview = self.build_preview()
        self.preview_update_running = False
        self.preview_stop_update_requested = False
        self.preview_restart_update_requested = False

    def build_preview(self):
        return Preview(self)

    def get_preview(self):
        return self.preview

    def destroy(self):
        super().destroy()
        self.preview.destroy()
