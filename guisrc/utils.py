import os
from gi.repository import Gtk


icon_theme = Gtk.IconTheme.get_default()
":type: Gtk.IconTheme"
icon_theme.append_search_path(os.path.join(os.path.join(os.path.dirname(__file__)), 'data', 'icons'))


def app_path():
    return os.path.dirname(os.path.dirname(__file__))


def data_path():
    return os.path.join(os.path.dirname(__file__), 'data')


def glade_path():
    return os.path.join(os.path.dirname(os.path.dirname(__file__)), 'glade')


def images_path():
    return os.path.join(glade_path(), 'images', 'png')
