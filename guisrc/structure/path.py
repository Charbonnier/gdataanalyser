import os

from gi.repository import Gtk

from guisrc.gui import Gui
from src.structure.path import Path
from structure.mutative import StringSetterGui
from utils import glade_path


class PathGui(Gui):

    def __init__(self):
        super().__init__()
        builder = Gtk.Builder()
        builder.add_from_file(os.path.join(glade_path(), 'structure', 'path.glade'))
        self._gui = builder.get_object('path_gui')
        ":type: Gtk.Box"
        raw_path_container = builder.get_object('raw_path_container')
        ":type: Gtk.Box"
        self.source = None
        self.path: Path = None
        self.raw_path = StringSetterGui()
        raw_path_container.pack_start(self.raw_path.gui(), True, True, 0)
        self.mode_cmb = builder.get_object('path_mode_cmb')
        ":type: Gtk.ComboBox"
        builder.connect_signals(self)

    def gui(self):
        return self._gui

    def set_context(self, context):
        super().set_context(context)
        self.raw_path.set_context(context)

    def set_object(self, source_path):
        """

        :param Source, src.structure.path.Path source_path:
        :return:
        """
        source, path = source_path
        self.source = source
        self.path = path
        self.raw_path.set_object((source, path.mutable_members['raw_path']))
        self.updating = True
        try:
            for i, (_, mode) in enumerate(self.mode_cmb.get_model()):
                if mode == path.modifier:
                    self.mode_cmb.set_active(i)
        finally:
            self.updating = False

    def on_path_mode_cmb_changed(self, widget, data=None):
        """

        :param Gtk.ComboBox widget:
        :param data:
        :return:
        """
        if self.updating:
            return
        self.path.modifier = widget.get_model()[widget.get_active()][1]
