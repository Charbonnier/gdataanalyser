from src.structure.database import DataBase
from src.structure.elements import Elements
from src.structure.source import SourceManager
from src.structure.view import ViewManager


class Context:
    def __init__(self, elements: Elements):
        self.elements: Elements = elements

    def get_dependency_tree(self):
        return self.elements.source_manager.dependency_tree

    def get_source_manager(self) -> SourceManager:
        return self.elements.source_manager

    def get_view_manager(self) -> ViewManager:
        return self.elements.view_manager

    def get_database(self) -> DataBase:
        return self.elements.view_manager.database
