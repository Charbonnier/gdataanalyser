import html
import os
import time

from gi.repository import GdkPixbuf, GObject, Gtk

from guisrc.gui import Gui, SimpleGui, NoGui
from src.structure.elements import Elements
from src.structure.source import Source, SourceManager
from src.structure.view import View
from structure.context import Context
from utils import glade_path

SOURCE_GROUP = 'Sources'
VIEW_GROUP = 'Views'


class ElementsManager(Gui):
    def __init__(self):
        super().__init__()
        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(glade_path(), 'structure', 'elementsmanager.glade'),
                                      ('elements_paned', 'elements_model', 'nothing_selected', 'description_box'))
        self._gui = builder.get_object('elements_paned')
        ":type: Gtk.Paned"
        self.elements_model: Gtk.TreeStore = builder.get_object('elements_model')
        self.remove_element_btn = builder.get_object('remove_element')
        ":type: Gtk.Button"
        self.nothing_selected = SimpleGui(builder.get_object('nothing_selected'))
        self.description_box = SimpleGui(builder.get_object('description_box'))
        self._current_gui = None
        ":type: Gui"
        self._gui_cache = {}
        ":type: dict[()->Gui, Gui]"

        self._sm_obs_ids = set()

        self.current_item_group = None
        self.current_item = None
        self.current_iter = None
        self.element_chooser = ElementChooser()
        self.source_iter = None
        self.view_iter = None

        self._listeners_ids = set()

        self._init_elements_model()
        self.set_current_gui(self.nothing_selected)
        builder.connect_signals(self)

    @property
    def elements(self) -> Elements:
        return self.context.elements

    def gui(self):
        return self._gui

    def clear(self, database):
        # TODO: Clear the context object
        #self.set_object(Elements(database))
        pass

    def set_parent_window(self, window=None):
        super().set_parent_window(window)
        self.element_chooser.set_parent_window(self.parent_window)

    def _init_elements_model(self):
        self.elements_model.clear()
        self.source_iter = self.elements_model.append(None,
                                                      ElementsManager.make_row('Sources', 0, None, '<b>Sources</b>',
                                                                               False, True, SOURCE_GROUP))
        self.view_iter = self.elements_model.append(None,
                                                    ElementsManager.make_row('Views', 0, None, '<b>Views</b>',
                                                                             False, True, VIEW_GROUP))

    def _get_gui_for_object(self, in_object):
        gui_class = Gui.get_gui_class_for_object(in_object)
        try:
            gui = self._gui_cache[gui_class]
        except KeyError:
            gui = gui_class()
            self._gui_cache[gui_class] = gui
        return gui

    def set_current_gui(self, gui):
        """
        :param Gui gui:
        :return:
        """
        if self._current_gui is not None:
            self._gui.remove(self._current_gui.gui())
            if self._current_gui is not self.nothing_selected:
                self._current_gui.destroy()
        self._gui.add2(gui.gui())
        self._current_gui = gui

    def set_current_item(self, item_iter, item, group):
        self.remove_element_btn.set_sensitive(item is not None)
        self.current_iter = item_iter
        self.current_item = item
        self.current_item_group = group

    @staticmethod
    def make_row(name, type_i, icon=None, markup=None, editable=True, is_group=False, group_name=''):
        if markup is None:
            markup = html.escape(name)
        return name, type_i, icon, markup, editable, is_group, group_name

    def set_context(self, context: Context):
        super().set_context(context)
        self.set_object(context)

    def set_object(self, context: Context):
        if self.context is not None:
            manager = self.context.get_source_manager()
            while self._sm_obs_ids:
                manager.remove_event_observer(self._sm_obs_ids.pop())
        self.context = context
        manager = self.context.get_source_manager()
        self._sm_obs_ids.add(manager.add_event_observer(manager.EVENT_SOURCE_ADDED, self.on_manager_source_added))
        self._sm_obs_ids.add(manager.add_event_observer(manager.EVENT_SOURCE_REMOVED, self.on_manager_source_removed))
        self._sm_obs_ids.add(manager.add_event_observer(manager.EVENT_SOURCE_RENAMED, self.on_manager_source_renamed))

        self._init_elements_model()

        for name, source in self.elements.source_manager.sources.items():
            self.elements_model.append(self.source_iter,
                                       ElementsManager.make_row(name, 0))

        for name, view in self.elements.view_manager.views.items():
            self.elements_model.append(self.view_iter,
                                       ElementsManager.make_row(name, 0))

    def on_manager_source_added(self, manager: SourceManager, source: Source):
        self.elements_model.append(self.source_iter, ElementsManager.make_row(source.name, 0))

    def on_manager_source_removed(self, manager: SourceManager, source: Source):
        r_iter = self.elements_model.iter_children(self.source_iter)
        while r_iter is not None:
            if self.elements_model[r_iter][0] == source.name:
                self.elements_model.remove(r_iter)
                break
            r_iter = self.elements_model.iter_next(r_iter)

    def on_manager_source_renamed(self, manager: SourceManager, source: Source, old_name: str):
        r_iter = self.elements_model.iter_children(self.source_iter)
        while r_iter is not None:
            if self.elements_model[r_iter][0] == old_name:
                self._set_iter_name(r_iter, source.name)
                break
            r_iter = self.elements_model.iter_next(r_iter)

    def on_elements_tv_cursor_changed(self, treeview, data=None):
        """
        :param Gtk.TreeView treeview:
        :param data:
        :return:
        """
        cursor = treeview.get_cursor()[0]
        if cursor is None:
            item_iter = None
        else:
            item_iter = self.elements_model.get_iter(cursor)
        self.set_current_item(item_iter, None, None)
        if item_iter is None:
            self.set_current_gui(self.nothing_selected)
            return
        row = self.elements_model[item_iter]
        if row[5]:
            self.set_current_gui(self.description_box)
            return
        parent_iter = self.elements_model.iter_parent(item_iter)
        group = self.elements_model[parent_iter][6]
        if group == SOURCE_GROUP:
            item = self.elements.source_manager.sources[row[0]]
            self.make_item_gui(group, item, item_iter)
        elif group == VIEW_GROUP:
            item = self.elements.view_manager.views[row[0]]
            item_gui = self.make_item_gui(group, item, item_iter)

            start_time_debug = time.time()

            item_gui.evaluate(self.elements.view_manager)

            print('{} : evaluate duration'.format(time.time() - start_time_debug))
        else:
            self.set_current_gui(NoGui())

    def make_item_gui(self, group, item, item_iter):
        item_gui = self._get_gui_for_object(item)
        item_gui.set_context(self.context)
        item_gui.set_object(item)
        item_gui.set_parent_window(self.parent_window)
        self.set_current_item(item_iter, item, group)
        self.set_current_gui(item_gui)
        return item_gui

    def on_add_element_clicked(self, widget, data=None):
        element, group = self.element_chooser.run()
        if element is None:
            return
        if group == SOURCE_GROUP:
            self.register_source(element)
        elif group == VIEW_GROUP:
            self.register_view(element)

    def register_source(self, source: Source):
        self.context.get_source_manager().register_source(source)

    def register_view(self, view):
        """
        :param View view:
        :return:
        """
        self.context.get_view_manager().register_view(view)
        self.elements_model.append(self.view_iter,
                                   ElementsManager.make_row(view.name, 0))

    def on_remove_element_clicked(self, widget, data=None):
        if self.current_item_group == SOURCE_GROUP:
            self.context.get_source_manager().remove_source(self.current_item)

    def on_element_name_edited(self, renderer, path, new_text, data=None):
        if self.current_item_group == SOURCE_GROUP:
            try:
                self.context.get_source_manager().rename_source(self.current_item, new_text)
            except ValueError:
                return

        elif self.current_item_group == VIEW_GROUP:
            try:
                self.context.get_view_manager().rename_view(self.current_item, new_text)
            except KeyError:
                pass
            else:
                self._set_current_iter_name(new_text)

    def _set_current_iter_name(self, name):
        self._set_iter_name(self.current_iter, name)

    def _set_iter_name(self, t_iter, name):
        row = self.elements_model[self.current_iter]
        row[0] = name
        row[3] = name


class ElementChooser(Gui):
    def __init__(self):
        super().__init__()
        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(glade_path(), 'structure', 'elementsmanager.glade'),
                                      ('new_element_dialog', 'nothing_selected', 'description_box'))
        self._gui = builder.get_object('new_element_dialog')
        ":type: Gtk.Dialog"
        self.description_container = builder.get_object('element_type_paned')
        self.element_types_model = Gtk.TreeStore(str, int, GdkPixbuf.Pixbuf, str, bool, str, GObject.TYPE_PYOBJECT)
        ":type: Gtk.TreeStore"
        self.element_types_tv = builder.get_object('element_types_tv')
        ":type: Gtk.TreeView"
        self.nothing_selected = SimpleGui(builder.get_object('nothing_selected'))
        self.description_box = SimpleGui(builder.get_object('description_box'))
        self.element_types_tv.set_model(self.element_types_model)
        self._current_gui = None
        ":type: Gui"
        source_iter = self.element_types_model.append(None, self.make_row('Sources', 0, markup='<b>Sources</b>',
                                                                          is_group=True, group_name=SOURCE_GROUP))
        for name, source in Source.classes[Source.class_path()].items():
            self.element_types_model.append(source_iter, self.make_row(name, 0, cls_object=source))

        view_iter = self.element_types_model.append(None, self.make_row('Views', 0, markup='<b>Views</b>',
                                                                        is_group=True, group_name=VIEW_GROUP))
        for name, view in View.classes.get(View.class_path(), {}).items():
            self.element_types_model.append(view_iter, self.make_row(name, 0, cls_object=view))

        self.set_current_gui(self.nothing_selected)
        self.set_parent_window()
        builder.connect_signals(self)
        self.selection = None

    @staticmethod
    def make_row(name, type_i, icon=None, markup=None, is_group=False, group_name='', cls_object=None):
        if markup is None:
            markup = name
        return name, type_i, icon, markup, is_group, group_name, cls_object

    def set_current_gui(self, gui):
        """
        :param Gui gui:
        :return:
        """
        if self._current_gui is not None:
            self.description_container.remove(self._current_gui.gui())
            self._current_gui.destroy()
        self.description_container.add2(gui.gui())
        self._current_gui = gui

    def set_parent_window(self, window=None):
        super().set_parent_window(window)
        if self.parent_window is not None:
            self._gui.set_transient_for(self.parent_window)

    def gui(self):
        return self._gui

    def set_object(self, object_to_follow):
        pass

    def run(self):
        self._gui.show()
        response = self._gui.run()
        self._gui.hide()
        if response == Gtk.ResponseType.OK:
            return self.selection[0](), self.selection[1]
        else:
            return None, None

    def on_element_types_tv_cursor_changed(self, treeview, data=None):
        """
        :param Gtk.TreeView treeview:
        :param data:
        :return:
        """
        item_iter = self.element_types_model.get_iter(treeview.get_cursor()[0])
        self.selection = None
        if item_iter is None:
            self.set_current_gui(self.nothing_selected)
            return
        row = self.element_types_model[item_iter]
        if row[4]:
            self.set_current_gui(self.description_box)
            return
        else:
            parent = self.element_types_model.iter_parent(item_iter)
            parent_row = self.element_types_model[parent]
            self.selection = row[6], parent_row[5]
            self.set_current_gui(self.description_box)

    def on_new_element_dialog_delete_event(self, widget, data=None):
        self._gui.hide()
