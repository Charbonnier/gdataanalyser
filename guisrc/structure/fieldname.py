import os
from typing import Tuple

from gi.repository import Gtk

from gui import Gui
from src.structure.source import Source, DuplicateFieldName
from utils import glade_path


class FieldNameEntry(Gui):
    def __init__(self):
        super().__init__()
        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(glade_path(), 'structure', 'fieldname.glade'),
                                      ('field_name_entry', ))
        self._field_obs_id = None
        self._gui = builder.get_object('field_name_entry')
        self.entry = builder.get_object('field_name')
        self.info = builder.get_object('field_name_info')
        self.warning_message = builder.get_object('warning_message')
        self.field_name = ''
        self.source = None

        builder.connect_signals(self)

    def gui(self):
        return self._gui

    def set_object(self, source_field: Tuple[Source, str]):
        if self.source is not None:
            self.source.remove_event_observer(self._field_obs_id)
        self.updating = True
        self.source, self.field_name = source_field
        self._field_obs_id = self.source.add_event_observer(Source.EVENT_FIELD_RENAMED, self.on_source_renames_field)
        self.entry.set_text(self.field_name)
        self.updating = False

    def update_field_name(self):
        self.on_field_name_changed(self.entry)

    def on_source_renames_field(self, source: Source, old_name: str, new_name: str):
        self.updating = True
        if old_name == self.field_name:
            self.entry.set_text(new_name)
            self.field_name = new_name
        self.updating = False

    def on_field_name_changed(self, widget: Gtk.Entry, data=None):
        if not self.updating:
            new_name = widget.get_text()
            old_name = self.field_name
            try:
                self.context.get_source_manager().rename_field(old_name, new_name)
            except DuplicateFieldName:
                widget.set_icon_from_stock(0, Gtk.STOCK_DIALOG_WARNING)
                widget.set_icon_tooltip_markup(0, f'Cannot rename <span weight="bold">{old_name}</span> to <span weight="bold">{new_name}</span>\n already in used.')
            else:
                widget.set_icon_from_stock(0, None)
        else:
            widget.set_icon_from_stock(0, None)

