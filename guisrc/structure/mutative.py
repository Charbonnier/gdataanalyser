import os
from typing import Tuple, Union

from gi.repository import Gtk, GObject

from guisrc.gui import Gui
from src.structure.mutative import ValueSource, MutableMemberSetter
from src.structure.source import Source, SourceManager
from utils import glade_path
from widgets.stylecontext import USER_INPUT_STYLE

FIELD_OK = 'ok'
FIELD_MISSING = 'missing'
FIELD_DEPENDENCY_LOOP = 'loop'


class ConstantGui:
    constant_guis = {}
    constant_gui_model = Gtk.ListStore(str, GObject.TYPE_PYOBJECT)

    def __init__(self):
        super().__init__()
        self.value = None
        self.gui = self.build_gui()
        self.updating = False
        self.on_value_changed = None
        ":type: (ConstantGui) -> None"

    @staticmethod
    def register_gui(data_type, gui_class):
        ConstantGui.constant_guis[data_type] = gui_class
        ConstantGui.constant_gui_model.append([data_type.__name__, gui_class])

    @staticmethod
    def gui_factory(value):
        constant_gui = ConstantGui.constant_guis.get(value.__class__, ConstantGui)()
        constant_gui.set_value(value)
        return constant_gui

    def build_gui(self):
        """
        :rtype: Gtk.Widget
        :return:
        """
        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(glade_path(), 'structure', 'mutativemember.glade'),
                                      ('default_container',))

        return builder.get_object('default_container')

    def display_value(self, value):
        self.gui.set_text(str(value))

    def set_value(self, value):
        self.updating = True
        self.display_value(value)
        self.updating = False

    def get_value(self):
        return self.value

    def value_changed(self):
        if self.on_value_changed is not None:
            self.on_value_changed(self)


class BoolGui(ConstantGui):
    def build_gui(self):
        gui = Gtk.Switch()
        gui.connect('notify::active', self.switch_changed)
        gui.show()
        return gui

    def switch_changed(self, widget, data=None):
        if self.updating:
            return
        self.value = self.gui.get_active()
        self.value_changed()

    def display_value(self, value):
        self.gui.set_active(value)


class StringGui(ConstantGui):
    def build_gui(self):
        gui = Gtk.Entry()
        context = gui.get_style_context()
        ":type: Gtk.StyleContext"
        context.add_class(USER_INPUT_STYLE)
        gui.connect('changed', self.entry_changed)
        gui.show()
        return gui

    def entry_changed(self, widget, data=None):
        if self.updating:
            return
        self.value = self.gui.get_text()
        self.value_changed()

    def display_value(self, value):
        self.gui.set_text(value)


class ComboGui(ConstantGui):
    def __init__(self, model: Gtk.ListStore, value_index=1, text_index=0):
        self.gui: Gtk.ComboBox = None
        self.textrenderer: Gtk.CellRendererText = None
        self.value_index = value_index
        self.text_index = text_index
        super().__init__()
        self.gui.set_model(model)

    def build_gui(self):
        gui = Gtk.ComboBox()
        self.textrenderer = Gtk.CellRendererText()
        gui.pack_start(self.textrenderer, True)
        gui.add_attribute(self.textrenderer, 'text', self.text_index)
        gui.connect('changed', self.on_combo_changed)
        gui.show()
        return gui

    def on_combo_changed(self, widget, data=None):
        if self.updating:
            return
        self.value = self.gui.get_model()[self.gui.get_active()][self.value_index]
        self.value_changed()

    def display_value(self, value):
        model = self.gui.get_model()
        self.value = value
        for i, row in enumerate(model):
            if row[self.value_index] == value:
                self.gui.set_active(i)
                break
        else:
            self.gui.set_active(-1)


class IntegerGui(StringGui):
    def display_value(self, value):
        super().display_value(str(value))

    def format_value(self, value_str):
        return int(value_str)

    def entry_changed(self, widget, data=None):
        if self.updating:
            return
        try:
            self.value = self.format_value(self.gui.get_text())
        except ValueError:
            self.gui.set_state_flags(Gtk.StateFlags.INCONSISTENT, False)
        else:
            self.value_changed()
            self.gui.unset_state_flags(Gtk.StateFlags.INCONSISTENT)


class FloatGui(IntegerGui):
    def format_value(self, value_str):
        return float(value_str)


ConstantGui.register_gui(int, IntegerGui)
ConstantGui.register_gui(str, StringGui)
ConstantGui.register_gui(float, FloatGui)


def build_reference_model_for_source(model: Gtk.ListStore, selected_field: str, source: Source):
    selected_index = -1
    dependency_loops = {}
    sm = source.manager
    source_name = source.name
    dependency_tree = sm.dependency_tree
    i = -1
    for i, (field, source_provider) in enumerate(sm.get_fields()):
        error = FIELD_OK
        sensitive = True
        try:
            loop = dependency_loops[source_provider.name]
        except KeyError:
            loop = dependency_tree.check_dependency_loop(source_name, source_provider.name)
        if loop:
            error = FIELD_DEPENDENCY_LOOP
            sensitive = False
        if selected_index < 0 and field == selected_field:
            selected_index = i
        model.append([field, field, sensitive, error, source_provider.name])
    missing_field_offset = i + 1
    for i, missing_field in enumerate(source.manager.get_missing_fields()):
        model.append(make_field_row(missing_field, '', FIELD_MISSING))
        if selected_index < 0 and missing_field == selected_field:
            selected_index = i + missing_field_offset
    return selected_index


def build_reference_model_for_view(sm: SourceManager, model, selected_field):
    selected_index = -1
    i = -1
    for i, (field, source) in enumerate(sm.get_fields()):
        if selected_index < 0 and field == selected_field:
            selected_index = i
        model.append(make_field_row(field, source.name, FIELD_OK))
    missing_field_offset = i + 1
    for i, missing_field in enumerate(sm.get_missing_fields()):
        if selected_index < 0 and selected_field == missing_field:
            selected_index = missing_field_offset + i
        model.append(make_field_row(missing_field, '', FIELD_MISSING))
    return selected_index


def make_field_row(field: str, source_name: str, error: str=FIELD_OK, sensitive: bool=True):
    if error == FIELD_MISSING:
        field_markup = f'<span foreground="red" strikethrough="true">{field}</span>'
    else:
        field_markup = field
    return field, field_markup, sensitive, error, source_name


def remove_field(model: Gtk.ListStore, source: Source, field: str, missing:bool):
    m_iter = model.iter_children()
    while m_iter is not None:
        if model[m_iter][0] == field:
            if missing:
                sensitive = model[m_iter][2]
                model[m_iter] = make_field_row(field, source.name, FIELD_MISSING, sensitive)
            else:
                model.remove(m_iter)
            break
        m_iter = model.iter_next(m_iter)


def rename_field(model: Gtk.ListStore, old_field, new_field):
    m_iter = model.iter_children()
    while m_iter is not None:
        if model[m_iter][0] == old_field:
            model[m_iter][0] = new_field
            break
        model.iter_next(m_iter)


def add_missing_field_to_model(model, selected_field):
    model.append([selected_field,
                  '<span foreground="red" strikethrough="true">{}</span>'.format(selected_field),
                  True, FIELD_MISSING, '-'])
    return len(model) - 1


class MutativeMemberSetterGui(Gui):
    def __init__(self, constant_gui):
        """
        :param ConstantGui|None constant_gui: If gui is None, only the selection of a reference field will be provided.
            Selection of a constant will not be allowed.
        """
        super().__init__()
        self._sm_observer_ids = set()
        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(glade_path(), 'structure', 'mutativemember.glade'),
                                      ('parameter_box', 'parameter_type_menu'))
        self._gui = builder.get_object('parameter_box')
        self.constant_container_box = builder.get_object('constant_container_box')
        """:type: Gtk.Box"""
        self.constant_type_radio = builder.get_object('constant_type')
        """:type: Gtk.RadioMenuItem"""
        self.constant_gui = None
        ":type: ConstantGui"
        self.set_constant_gui(constant_gui)
        self.reference_cmb = builder.get_object('reference_cmb')
        """:type: Gtk.ComboBox"""
        self.reference_model = Gtk.ListStore(str, str, bool, str, str)
        """ field name, field with markup, sensitive, error (none, duplicate, missing)
        :type: Gtk.ListStore"""
        self.reference_cmb.set_model(self.reference_model)
        self.mutable_member: MutableMemberSetter = None
        self.source: Union[Source, None] = None
        self.updating = False
        builder.connect_signals(self)

    def on_sm_field_added(self, sm: SourceManager, source: Source, field: str):
        if self.source is not None:
            loop = sm.dependency_tree.check_dependency_loop(self.source.name, source.name)
        else:
            loop = False
        if loop:
            error = FIELD_DEPENDENCY_LOOP
            sensitive = False
        else:
            error = FIELD_OK
            sensitive = True
        self.reference_model.append(make_field_row(field, source.name, error, sensitive))

    def on_sm_field_removed(self, sm: SourceManager, source: Source, field: str, missing: bool):
        remove_field(self.reference_model, source, field, missing)

    def on_sm_field_renamed(self, sm: SourceManager, source: Source, old_field: str, new_field: str):
        rename_field(self.reference_model, old_field, new_field)

    def set_constant_gui(self, constant_gui):
        """
        :param ConstantGui constant_gui:
        :return:
        """
        if self.constant_gui is not None:
            self.constant_container_box.remove(self.constant_gui.gui)
            self.constant_gui.on_value_changed = None

        self.constant_gui = constant_gui
        if constant_gui is not None:
            self.constant_gui.on_value_changed = self.on_constant_value_changed
            self.constant_container_box.pack_start(self.constant_gui.gui, True, True, 0)

    def gui(self):
        return self._gui

    def set_constant_value(self, value):
        if self.constant_gui is None:
            return
        self.constant_gui.set_value(value)

    def on_constant_value_changed(self, constant_gui: StringGui):
        self.mutable_member.value_source.set_value(constant_gui.get_value())

    def make_reference_model(self):
        if self.mutable_member.value_source.type == ValueSource.REFERENCE:
            current_field = self.mutable_member.value_source.value
        else:
            current_field = None

        if self.source is not None:
            current_field_index = build_reference_model_for_source(self.reference_model, current_field, self.source)
        else:
            current_field_index = build_reference_model_for_view(self.context.get_source_manager(), self.reference_model, current_field)

        if current_field is not None:
            self.reference_cmb.set_active(current_field_index)

    def set_object(self, source_mutable: Tuple[Source, MutableMemberSetter]):
        source, mutable_member = source_mutable
        if self.source is not None and self.source.manager is not None and self.source.manager is not source.manager:
            for obs_id in self._sm_observer_ids:
                self.source.manager.remove_event_observer(obs_id)

        self.source, self.mutable_member = source, mutable_member
        manager = self.source.manager
        self._sm_observer_ids.clear()
        if manager is not None:
            self._sm_observer_ids.add(manager.add_event_observer(SourceManager.EVENT_FIELD_ADDED, self.on_sm_field_added))
            self._sm_observer_ids.add(manager.add_event_observer(SourceManager.EVENT_FIELD_REMOVED, self.on_sm_field_removed))
            self._sm_observer_ids.add(manager.add_event_observer(SourceManager.EVENT_FIELD_RENAMED, self.on_sm_field_renamed))
        self.updating = True
        self.make_reference_model()
        if self.mutable_member.value_source.type == ValueSource.CONSTANT:
            self.constant_type_radio.set_active(True)
            self.show_constant_gui()
            self.set_constant_value(self.mutable_member.value_source.value)
        else:
            self.constant_type_radio.set_active(False)
            self.show_reference_gui()
        self.updating = False

    def show_constant_gui(self):
        self.reference_cmb.hide()
        if self.constant_gui is None:
            return
        self.constant_container_box.show()
        self.updating = True
        try:
            self.constant_gui.set_value(self.mutable_member.value_source.value)
        except ValueError:
            pass
        self.updating = False

    def show_reference_gui(self):
        self.reference_cmb.show()
        self.updating = True
        for i, row in enumerate(self.reference_model):
            if row[0] == self.mutable_member.value_source.value:
                self.reference_cmb.set_active(i)
                break
        self.updating = False
        self.constant_container_box.hide()

    def find_missing_reference(self):
        pass

    def destroy(self):
        super().destroy()
        if self.source is not None and self.source.manager is not None:
            for obs_id in self._sm_observer_ids:
                self.source.manager.remove_event_observer(obs_id)

    def on_constant_type_toggled(self, menu_item, data=None):
        """
        :param Gtk.RadioMenuItem menu_item:
        :param data:
        :return:
        """
        if self.updating:
            return
        if menu_item.get_active():
            self.show_constant_gui()
            self.mutable_member.value_source.set_type(ValueSource.CONSTANT)

    def on_reference_type_toggled(self, menu_item, data=None):
        """
        :param Gtk.RadioMenuItem menu_item:
        :param data:
        :return:
        """
        if self.updating:
            return
        if menu_item.get_active():
            self.show_reference_gui()
            self.mutable_member.value_source.set_type(ValueSource.REFERENCE)
            # TODO: Whats the purpose of this line
            #self.mutable_member.value_source.value = ''

    def on_reference_cmb_changed(self, widget, data=None):
        """
        :param Gtk.ComboBox widget:
        :param data:
        :return:
        """
        if self.updating:
            return
        if self.mutable_member.value_source.type == ValueSource.REFERENCE:
            active_item = widget.get_active()
            if active_item >= 0:
                field = widget.get_model()[active_item][0]
                self.mutable_member.value_source.set_value(field)
            else:
                # The referenced field has been removed.
                self.find_missing_reference()


class VariantSetterGui(MutativeMemberSetterGui):
    @staticmethod
    def __new__(cls, *more):
        instance = super().__new__(cls, *more)

        builder = Gtk.Builder()
        builder.add_objects_from_file(os.path.join(glade_path(), 'structure', 'mutativemember.glade'),
                                      ('variant_toolbar', 'variant_selection_dialog'))
        instance.variant_toolbar = builder.get_object('variant_toolbar')
        instance.selection_dialog = builder.get_object('variant_selection_dialog')
        instance.constant_gui_tv = builder.get_object('types_tv')
        instance.constant_gui_tv.set_model(ConstantGui.constant_gui_model)
        builder.connect_signals(instance)
        return instance

    def __init__(self):
        super().__init__(ConstantGui())
        self.constant_container_box.set_orientation(Gtk.Orientation.HORIZONTAL)
        self.constant_container_box.pack_end(self.variant_toolbar, False, False, 0)

    def show_constant_gui(self):
        constant_gui = ConstantGui.gui_factory(self.mutable_member.value_source.value)
        self.set_constant_gui(constant_gui)
        super().show_constant_gui()

    def on_type_selector_clicked(self, widget, data=None):
        self.selection_dialog.show()
        response = self.selection_dialog.run()
        self.selection_dialog.hide()
        if response == Gtk.ResponseType.OK:
            path, _ = self.constant_gui_tv.get_cursor()
            if path is None:
                return
            constant_iter = ConstantGui.constant_gui_model.get_iter(path)
            constant_gui = ConstantGui.constant_gui_model[constant_iter][1]()
            self.set_constant_gui(constant_gui)

    def on_variant_selection_dialog_delete_event(self, widget, data=None):
        self.selection_dialog.hide()


class StringSetterGui(MutativeMemberSetterGui):
    def __init__(self):
        super().__init__(StringGui())


class IntegerSetterGui(MutativeMemberSetterGui):
    def __init__(self):
        super().__init__(IntegerGui())

