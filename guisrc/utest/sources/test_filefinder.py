from unittest import TestCase
from gi.repository import Gtk

from sources.filefinder import FileFinderSourceGui
from src.sources.filefinder import FileFinderSource
from structure.context import Context
from src.structure.database.simpledatabase import PyDB
from src.structure.elements import Elements
from structure.elementsmanager import ElementsManager


class TestFileFinderSourceGui(TestCase):
    def test_main(self):
        context = Context(Elements(PyDB()))

        file_finder = FileFinderSource()
        context.get_source_manager().register_source(file_finder)

        file_finder_gui = FileFinderSourceGui()
        file_finder_gui.set_context(context)
        file_finder_gui.set_object(file_finder)

        window = Gtk.Window()
        window.show()
        window.add(file_finder_gui.gui())

        Gtk.main()

    def test_set_object(self):
        self.fail()

test_re = 'test (?P<named>.*)'
