import os
import logging
import traceback

import initexternals

import gi

from potable.saveable import Saveable, State
from src.config.jsonwriter import JsonWriter
from src.structure.database.simpledatabase import PyDB
from src.structure.elements import Elements
from guisrc.structure.elementsmanager import ElementsManager
from guisrc.structure.mutative import MutativeMemberSetterGui
from src.structure.source import EvaluationError, SourceManager
from src.utils import main_log_handler
from structure.context import Context
from utils import glade_path, data_path, app_path
from widgets.stylecontext import load_style

START_LINE = 'start_line'

gi.require_version('Gtk', '3.0')

from gi.repository import Gtk, GObject, Gdk, GLib
from guisrc.sources.textfile import GTextFile
from guisrc.sources.filefinder import FileFinderSourceGui
from guisrc.sources.function import FunctionGui
from guisrc.sources.aggregate import AggregateGui
from guisrc.views.plot import PlotGui
import guisrc.views.tableview

#from src.checkanddebug.sources.test_textFile import TestTextFile


class Configuration(Saveable):
    def __init__(self, config_dict=None) -> None:
        super().__init__()
        if config_dict is None:
            config_dict = {}
        self.config_dict = config_dict

    def get_state(self) -> State:
        state = super().get_state()
        state.add_parameter('configuration', self.config_dict)
        return state

    def set_state(self, state: State):
        super().set_state(state)
        self.config_dict = state.get_parameters()['configuration']


Configuration.register_class()


class Main:
    def __init__(self):
        self.logger = logging.Logger('Main', level=logging.ERROR)
        self.logger.addHandler(main_log_handler)
        builder = Gtk.Builder()
        builder.add_from_file(os.path.join(glade_path(), 'main.glade'))
        self.window = builder.get_object('main_window')
        ":type: Gtk.Window"
        self.main_box = builder.get_object('main_box')
        ":type: Gtk.Box"
        self.elements_paned = builder.get_object('elements_paned')
        ":type: Gtk.Paned"
        self.elements_container = builder.get_object('elements_container')
        ":type: Gtk.Box"
        self.console_container = builder.get_object('console_container')
        ":type: Gtk.Box"
        self.tools_ttb = [builder.get_object('evaluation_ttb')]
        ":type: list[Gtk.ToggleToolButton]"
        self.tools_consoles = [builder.get_object('evaluation_console')]
        ":type: list[Gtk.Box]"
        self.evaluation_log_buffer = builder.get_object('evaluation_log_buffer')
        ":type: Gtk.TextBuffer"
        self._run_tb = builder.get_object('run_tb')
        self._stop_tb = builder.get_object('stop_tb')
        self.element_manager = ElementsManager()
        self.element_manager.set_context(Context(Elements(PyDB())))
        self.elements_container.pack_start(self.element_manager.gui(), True, True, 0)
        self.element_manager.set_parent_window(self.window)

        self._current_field = None
        self._evaluation_generator = None
        ":type: list[src.structure.source.EvaluationStep]"
        self._evaluation_in_progress = False
        self._stop_evaluation = False

        self._configuration_path = None

        builder.connect_signals(self)

    @property
    def source_manager(self) -> SourceManager:
        return self.element_manager.elements.source_manager

    def new(self):
        self.element_manager.clear(PyDB())
        self._configuration_path = None

    def open(self):
        dialog = Gtk.FileChooserDialog(title='Open', action=Gtk.FileChooserAction.OPEN)
        dialog.add_buttons(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OPEN, Gtk.ResponseType.OK)
        dialog.set_transient_for(self.window)
        if self._configuration_path is not None:
            dialog.set_filename(self._configuration_path)

        response = dialog.run()
        config_path = dialog.get_filename()
        dialog.destroy()
        if response != Gtk.ResponseType.OK:
            return
        writer = JsonWriter()
        data = writer.read(config_path).config_dict
        sources = data['sources']
        views = data['views']
        elements = Elements(PyDB())
        self.element_manager.set_object(Context(elements))
        elements.source_manager.load_sources(sources.values())
        for view in views.values():
            elements.view_manager.register_view(view)
        elements.source_manager.app_dir = app_path()
        self._configuration_path = config_path
        self.set_config_dir()

    def save(self):
        if self._configuration_path is None:
            self.save_as()
            return

        writer = JsonWriter()
        writer.write(self._configuration_path,
                     Configuration({'sources': self.element_manager.elements.source_manager.sources,
                                    'views': self.element_manager.elements.view_manager.views})
                     )

    def save_as(self):
        dialog = Gtk.FileChooserDialog(title='Save as ...', action=Gtk.FileChooserAction.SAVE,
                                       buttons=(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                                                Gtk.STOCK_SAVE, Gtk.ResponseType.OK))
        dialog.set_transient_for(self.window)
        if self._configuration_path is not None:
            dialog.set_filename(self._configuration_path)
        else:
            dialog.set_filename(self.source_manager.context_source.data_dir)

        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            self._configuration_path = dialog.get_filename()
            self.set_config_dir()
            dialog.destroy()
            self.save()
        else:
            dialog.destroy()

    def set_config_dir(self):
        if self._configuration_path is not None:
            self.source_manager.config_dir = os.path.dirname(self._configuration_path)

    def on_main_window_delete_event(self, window, data=None):
        self.quit()
        return True

    def on_save_tb_clicked(self, widget, data=None):
        self.save()

    def on_save_mit_activate(self, widget, data=None):
        self.save()

    def on_save_as_mit_activate(self, widget, data=None):
        self.save_as()

    def on_open_tb_clicked(self, widget, data=None):
        self.open()

    def on_open_mit_activate(self, widget, data=None):
        self.open()

    def on_new_tb_clicked(self, widget, data=None):
        self.new()

    def on_new_mit_activate(self, widget, data=None):
        self.new()

    def on_quit_mit_activate(self, widget, data=None):
        self.quit()

    def on_data_path_mit_activate(self, widget, data=None):
        dialog = Gtk.FileChooserDialog(title='Select data path ...', action=Gtk.FileChooserAction.SELECT_FOLDER)
        dialog.add_buttons(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OPEN, Gtk.ResponseType.OK)
        dialog.set_transient_for(self.window)
        dialog.set_filename(self.source_manager.context_source.data_dir)
        response = dialog.run()
        path = dialog.get_filename()
        dialog.destroy()
        if response == Gtk.ResponseType.OK:
            self.source_manager.context_source.data_dir = path

    def on_run_tb_clicked(self, widget, data=None):
        self._evaluation_in_progress = True
        self._stop_evaluation = False
        self._stop_tb.set_sensitive(True)
        self._run_tb.set_sensitive(False)
        self.evaluation_log_buffer.set_text('')
        self._current_field = None
        self.evaluation_log_buffer.create_mark(START_LINE, self.evaluation_log_buffer.get_end_iter(), left_gravity=True)
        self._evaluation_generator = self.source_manager.walk_evaluation()
        GLib.idle_add(self._evaluate)

    def on_stop_tb_clicked(self, widget, data=None):
        self._stop_evaluation = True

    def on_evaluation_ttb_toggled(self, widget, data=None):
        """
        :param Gtk.ToggleToolButton widget: 
        :param data: 
        :return: 
        """
        self.set_console(widget)

    def set_console(self, console_ctrl):
        index = self.tools_ttb.index(console_ctrl)
        for i in range(len(self.tools_ttb)):
            if i != index:
                self.tools_ttb[i].set_active(False)
        console = self.tools_consoles[index]
        if console_ctrl.get_active():
            self.console_container.pack_start(console, True, True, 0)
        else:
            self.console_container.remove(console)
        self.console_container.set_visible(len(self.console_container.get_children()) != 0)

    def _evaluation_end(self):
        self._evaluation_in_progress = False
        self._evaluation_generator = None
        self._stop_evaluation = False
        self._stop_tb.set_sensitive(False)
        self._run_tb.set_sensitive(True)

    def _evaluate(self):
        if self._stop_evaluation:
            self._stop_tb.set_sensitive(False)
            self._run_tb.set_sensitive(True)
            self._evaluation_end()
        try:
            evaluation_step = next(self._evaluation_generator)
        except StopIteration:
            self._evaluation_end()
        except EvaluationError as ec:
            self._evaluation_end()
            self.dialog_evaluation_error(ec)
        else:
            start_mark = self.evaluation_log_buffer.get_mark(START_LINE)
            new_node = evaluation_step.source
            index = evaluation_step.item_index
            if new_node != self._current_field and self._current_field is not None:
                self.evaluation_log_buffer.insert(self.evaluation_log_buffer.get_end_iter(), '\n')
                self.evaluation_log_buffer.move_mark(start_mark, self.evaluation_log_buffer.get_end_iter())

            self.evaluation_log_buffer.delete(self.evaluation_log_buffer.get_iter_at_mark(start_mark),
                                              self.evaluation_log_buffer.get_end_iter())
            self.evaluation_log_buffer.insert_with_tags_by_name(self.evaluation_log_buffer.get_end_iter(),
                                                                '{}'.format(new_node), 'field_name')
            self.evaluation_log_buffer.insert(self.evaluation_log_buffer.get_end_iter(), ', {}'.format(index))

            self._current_field = new_node
            GLib.idle_add(self._evaluate)

    def dialog_evaluation_error(self, ec):
        message_title = 'Error during evaluation'
        exception = ec.exception
        message_details = 'Exception: {}\n{}\nAt Node: {}'.format(
            exception.__class__.__name__, str(exception), ec.node)
        self.logger.error(
            '\n\t'.join((message_title + '\n' + message_details + '\n' + traceback.format_exc()).split('\n')))
        dialog = Gtk.MessageDialog(parent=self.window, destroy_with_parent=True,
                                   message_type=Gtk.MessageType.ERROR, buttons=Gtk.ButtonsType.OK,
                                   text=message_title)
        ":type: Gtk.MessageDialog"
        dialog.format_secondary_text(message_details)
        dialog.run()
        dialog.destroy()

    def quit(self):
        Gtk.main_quit()


load_style(os.path.join(data_path(), "style.css"))

main_ui = Main()
main_ui.window.show()
Gtk.main()
